# System Definition File Visualization: sdfvisio

This tool processes an Arm Development Studio System Definition file and provides a visual representation of the JTAG, DAP, trace, CTI, and other topologies. It displays the topology using the following shapes: box, funnel, or replicator. The shape is based on the component type. Each component - component connection is represented by a line.
SDFvisio can export the created diagrams as PNG and SVG files using different color schema. 

## Authors
Authors:
- Lewis Won
- Tyler-Jay Osborne
- Robert Wiesner

## License
`sdfvisio` is licensed under Apache 2.0

# Dependency, build, and installation

## External dependency
This tool is using PySide6 (https://pypi.org/project/PySide6/) as the user interface engine which is licensed under LGPLv3/LGPLv2. The PiSide6 installation package are available under Windows, Mac, and Linux. The Pyside6 packages are automatically installed with the sdfvisio package.
The build mechanism is using the Python "build" package (https://pypi.org/project/build/), ensure that this is installed using: `pip install build`

## Build

How to build:
- Change to the source directory `<repo>/sdfvisio/src`
- Execute:  `python -m build`.
You find the WHL installation package here: `dist/sdfvisio-<version>-py3-none-any.whl`

## Installation
Install this package using:
`pip install dist/sdfvisio-<version>-py3-none-any.whl`

Note: you may need to update the `pip` install package; some users may require the additional option --user:
- `python -m pip install --upgrade pip`
- `python -m pip install --user --upgrade pip`
- Some Linux:
  - install libxcb-cursor0 using `sudo apt install libxcb-cursor0`
  - install venv using `sudo apt install python3-venv`
- Some Linux: create a virtual environment for installation, see https://docs.python.org/3/library/venv.html
  - Create: `python -m venv ~/.venv/sdfvisio`
  - Activate: . ~/.venv/sdfvisio/bin/activate
  - After activating the local environment, you can install and run `sdfvisio`.

# Usage
You can start the tool using the command line:
- `python -m sdfvisio`
- `python -m sdfvisio [options] <sdf files>`

After starting the tool, you can open additional SDF files.

## the command line options
| Option | Description | UI |
| -------- | ------------- | --- |
| -h | show this help message and exit| no |
| --help | show this help message and exit| no |
| --notopology NOTOPOLOGY | If the names are matching exclude these components from the topology view | no |
| --highlight HIGHLIGHT | if name matches, Comp:highlight the box, CompA:CompB: highlight path from compA to CompB | YES |
| --excluded | Add tab if excluded components exist | YES |
| --sinktop | start with trace sinks on top, the default is source on top, sink on the bottom row | YES |
| --addtf | add 'hidden' trace funnels | YES |
| --addtr | add 'hidden' trace replicator | YES |
| --widerect | draw wide rectangle | YES |
| --oldlayout | use the layout with centered boxes | YES |
| --curve | use curved lines | YES |
| --shadow | enable shadow for boxes | YES |

The tool creates a tab for each opened SDF file. Each SDF tab is opening several tabs containing JTAG/SWD, DAP hierarchy, CortexTrace/ATB, CTITrigger and others. Most of the tabs contain a graphical representation, which can be exported as PNG or SVG. You can change the color theme: light, dark, and user based on your preference. Similar with the curved and shadow effect. 

## Settings
The settings and last recently used files are store in a user specific ini file.

## Example N1SDP trace topology:
1. Trace source and sink can be identified by the different border color
1. Trace funnel and replicator a using a unique shape
1. The background color is related to the used access port in the SDF file
1. Unique connections are using the "first" color, while when a source or definitaion point is connect a second time, the "seconds" color is used. 
## Color Overview
| Color | Usage |
| ----- | ----- |
| foreground | default foreground color |
| background | default background color |
| text color | element test color |
| first | first connection between two topology items |
| seconds | if the same source or destination point (component) is used a secondary time, this color is used |
| red | Background color used for device marked as excluded |
| artcstf | background for artificial added Coresight Trace Funnel, not part of the SDF file |
| artrepl | background for artificial added Coresight Trace Funnel, not part of the SDF file |
| tracesink | border color of a trace sink |
| tracesource | border color of a trace source |
| defaultBorder | all other border color colors |
| ERRORtab | tab background if the tab text starts with "ERROR", created if reading the SDF file causes an error |
| INFOtab | tab background if the tab text starts with "INFO ", currently unused |
| highlight | brush color to highlight trace path(s) |
| AP Red/Green/Blue | start, end, and step values for the AP color selection |

![SDV Viso Trace Topology](sdfvisio.jpg)


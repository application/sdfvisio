###
#
# SDF Visualizer, uses PySide6
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###
import re
from PySide6.QtWidgets import QMainWindow, QToolBar
from PySide6.QtWidgets import QLineEdit
from PySide6.QtWidgets import QTextEdit
from PySide6.QtWidgets import QTableWidget, QTableWidgetItem
from PySide6.QtWidgets import QLabel
from PySide6.QtGui     import QColor, QBrush, QCursor, QFontMetrics
from PySide6.QtGui     import QAction
from PySide6.QtWidgets import QScrollArea
from PySide6.QtCore    import Qt
from PySide6.QtWidgets import QMenu
from PySide6.QtWidgets import QFileDialog
from PySide6.QtGui     import QFontDatabase
from PySide6.QtGui     import QKeySequence
from PySide6.QtWidgets import QAbstractItemView

class cTextEdit(QTextEdit):
    def __init__(self, s : str, useFont):
        super().__init__(s)
        self.setReadOnly(True)
        self.setFont(useFont)
        self.setLineWrapMode(QTextEdit.NoWrap)
        self.errorFlag = False
        self.setHighlight(False)

    def getBackgroundColor(self, adjust : int):
        color = self.palette().color(self.backgroundRole())
        r = 255 # color.red()
        g = 255 # color.green()
        b = 255 # color.blue()
        dr = 0
        dg = 0
        db = adjust
        if self.errorFlag:
            dr += 32
            dg += 16
            db += 16

        r = r + dr if r < 128 else r - dr
        g = g + dg if g < 128 else g - dg
        b = b + db if b < 128 else b - db
        ret = 256 * (256 * r + g) + b
        return 0xffffff & ret

    def setErrorFlag(self, errorFlag):
        self.errorFlag = errorFlag
        self.setHighlight(False)

    def getErrorFlag(self):
        return self.errorFlag
    
    def setHighlight(self, flag : bool):
        bgc = self.getBackgroundColor(16 if flag else 0)
        # FIXME
        self.setStyleSheet(f"background-color: #{bgc:06X}; color:#ffffff")

class cTableWidgetItem(QTableWidgetItem):
    def __init__(self, s : str, useFont):
        super().__init__(s)
        self.setFont(useFont)
        self.setFlags(self.flags() & (~Qt.ItemIsEditable))
        self.backGroundColor = QColor(255, 255, 255) # self.background().color()
        self.errorFlag = False
        self.setHighlight(False)
        # FIXME
        self.setBackground(QBrush(self.backGroundColor))
        self.setForeground(QBrush(QColor(0, 0, 0)))


    def getBackgroundColor(self, adjust : int):
        r = self.backGroundColor.red()
        g = self.backGroundColor.green()
        b = self.backGroundColor.blue()
        db = 0
        ds = 0
        if self.errorFlag:
            ds = 16
            db = 32

        r = r + db          if r < 128 else (r - ds - adjust)
        g = g + ds          if g < 128 else (g - db - adjust)
        b = b + ds + adjust if b < 128 else (b - db)

        ret = 256 * (256 * r + g) + b
        return 0xffffff & ret
    
    def setErrorFlag(self, errorFlag):
        self.errorFlag = errorFlag
        self.setHighlight(False)

    def getErrorFlag(self):
        return self.errorFlag
    
    def setHighlight(self, flag: bool):
        bgc = self.getBackgroundColor(16 if flag else 0)
        self.setBackground(QBrush(QColor(bgc)))

class cDapLogTableWidget(QTableWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.setColumnCount(0)
        self.setRowCount(0)
        self.fixedFont = QFontDatabase.systemFont(QFontDatabase.FixedFont)
        self.aHeader = []
        self.defaultRowHeight = QFontMetrics(self.fixedFont).height()
        self.aGroupName  = []
        self.dStatus     = { "Enable All" : True}
        self.aaLineCount = []
        self.aFoundRow   = []
        self.foundIdx    = 0
        self.lastSearch  = ""
        self.clipboard   = parent.clipboard
        
        color = self.palette().color(self.backgroundRole())
        if color.lightness() < 128:
            self.errorBrush      = QBrush(QColor(color.red() + 64, color.green(), color.blue()))
        else:
            self.errorBrush      = QBrush(QColor(color.red(), color.green() - 64, color.blue() - 64))

    def addCheckableAction(self, menu : QMenu, name : str):
        newAction = menu.addAction(name)
        newAction.setCheckable(True)
        newAction.setChecked(self.dStatus.get(name, True))
        return newAction

    def addAction(self, menu : QMenu, name : str):
        newAction = menu.addAction(name)
        return newAction

    def exportAsCSV(self):
        """Export the dap log with filter as a comma separated file, a multi line content creates multiple output lines"""
        fileNames = QFileDialog.getSaveFileName(self, 'Save content as CSV', '.', "CSV File (*.csv)")
        if fileNames is not None and 0 < len(fileNames[0]):
            aCol = []
            aOut = ["Index"]
            for col in range(len(self.aHeader)):
                if self.dStatus[self.aHeader[col]]:
                    aCol.append(col)
                    aOut.append(self.aHeader[col])

            with open(fileNames[0], "w") as fOut:
                print(",".join(aOut), file = fOut)

                for row in range(self.rowCount()):
                    if not self.isRowHidden(row):
                        # create the CSV file but the column header
                        aOut = [self.getItemText(row, col, "").strip() for col in aCol]

                        # add the column header at the beginning
                        aOut.insert(0, self.verticalHeaderItem(row).text())

                        again = True

                        while again:
                            again = False
                            aLineOut = []
                            for idx in range(len(aOut)):
                                aS = aOut[idx].split("\n", 1)
                                aLineOut.append(f'"{aS[0].strip()}"')
                                if 2 == len(aS):
                                    again = True
                                    aOut[idx] = aS[1]
                                else:
                                    aOut[idx] = ""
                            print(",".join(aLineOut), file = fOut)
                            
                fOut.close()

    def contextMenuEvent(self, event):
        aCopyToClip = ["Python Object", "CSV"]
        menu        = QMenu("Select Components")
        byAll       = self.addCheckableAction(menu, "Enable All")

        copyContent = QMenu("Copy to Clipboard")
        menu.addMenu(copyContent)
        for s in aCopyToClip:
            self.addAction(copyContent, s)

        byGroup = QMenu("By Group")
        menu.addMenu(byGroup)
        
        for groupName in self.aGroupName:
            if groupName != self.aHeader[0]:
                self.addCheckableAction(byGroup, groupName)

        menu.addSeparator()
        if len(self.aHeader) < 5:
            for name in self.aHeader:
                self.addCheckableAction(menu, name)
        else:
            self.addCheckableAction(menu, self.aHeader[0])
            for groupName in self.aGroupName:
                if groupName != self.aHeader[0]:
                    group = QMenu(groupName)
                    menu.addMenu(group)
                    for name in self.aHeader:
                        if name.startswith(groupName):
                            self.addCheckableAction(group, name)

        action = menu.exec_(QCursor.pos())

        if action is None:
            pass
        elif action == byAll:
            self.dStatus["Enable All"] = action.isChecked()
            for name in self.dStatus:
                self.dStatus[name] = action.isChecked()
            for idx in range(1, len(self.aHeader)):
                self.changeVisibility(idx, action.isChecked())
        else:
            # check if this is a group if individual action
            name = action.text()
            if name in self.aHeader:
                # change the visibility of these entries
                self.changeVisibility(self.aHeader.index(name), action.isChecked())
            elif name in aCopyToClip:
                # copy selected to clipboard
                if name == "CSV":
                    strSep = '"'
                else:
                    strSep = '"""'

                aLines = []
                aCol   = []
                aRow   = []

                for item in self.selectedItems():
                    row = self.row(item)
                    col = self.column(item)
                    if row not in aRow:
                        aRow.append(row)
                    if col not in aCol:
                        aCol.append(col)
                    aLines.append((row, col, f'{strSep}{item.text()}{strSep}'))

                aCol.sort()
                aRow.sort()

                aaRes = []
                for idx in range(len(aRow)+1):
                    aaRes.append(['""'] * (len(aCol) + 1))

                for idx, row in enumerate(aRow):
                    aaRes[idx + 1][0] = f'"{self.verticalHeaderItem(row).text()}"'

                for content in aLines:
                    aaRes[aRow.index(content[0]) + 1][aCol.index(content[1]) + 1] = content[2]
                
                aColText = []
                aaRes[0][0] = '"Column"'
                
                for idx, col in enumerate(aCol):
                    aaRes[0][idx + 1] = f'"{self.horizontalHeaderItem(col).text()}"'

                if name == "Python Object":
                    sep = ",\n\t\t\t"
                    for aRes in aaRes:
                        aColText.append(f"    {aRes[0]} : [{sep.join(aRes[1:])}]")

                    sep = ",\n\t"
                    lb = "{"
                    rb = "\n}"
                    self.clipboard.setText(f"data = {lb}\n{sep.join(aColText)} {rb}")
                elif name == "CSV":
                    sep = ","
                    for aRes in aaRes:
                        s = f"{sep.join(aRes)}"
                        s = s.replace("\n", "\\n")
                        aColText.append(s)
                    sep = "\n"
                    self.clipboard.setText(f"{sep.join(aColText)}")
            else:
                # group enable/disable
                self.dStatus[name] = action.isChecked()
                for checkName in self.aHeader:
                    if checkName.startswith(name):
                        self.changeVisibility(self.aHeader.index(checkName), action.isChecked())

    def goto(self, idx : int):
        if idx == -1:
            self.foundIdx = len(self.aFoundRow) - 1
            self.setFoundVisible();
        elif 0 <= idx and idx < len(self.aFoundRow):
            self.foundIdx = idx
            self.setFoundVisible()

    def getFoundCount(self):
        return len(self.aFoundRow)
    
    def setFoundVisible(self):
        if 0 < len(self.aFoundRow):
            for col in range(len(self.aHeader)):
                twi = self.verticalHeaderItem(self.aFoundRow[self.foundIdx])
                if not twi is None:
                    self.scrollToItem(twi, QAbstractItemView.PositionAtCenter)
                    self.selectRow(self.aFoundRow[self.foundIdx])
                    self.parent().updateIndexText(self.foundIdx + 1)
                    return
    
    def getCurrentIndex(self, delta : int):
        if delta == -1 and self.foundIdx == 0:
            return len(self.aFoundRow) - 1;
        elif delta == 1 and self.foundIdx == len(self.aFoundRow) - 1:
            return 0
        return self.foundIdx + delta
    
    def updateColors(self, row : int, col : int, highlight : bool):
        twi = self.item(row, col)
        twi = self.cellWidget(row, col) if twi is None else twi
        twi.setHighlight(highlight)

    def highlightItems(self, part : str):
        self.aFoundRow = []
        self.foundIdx = 0

        prog = None if part is None else re.compile(part, re.IGNORECASE)

        for row in range(self.rowCount()):
            if not self.isRowHidden(row):
                for col in range(len(self.aHeader)):
                    text = self.getItemText(row, col)
                    if text is not None:
                        state = self.dStatus[self.aHeader[col]] and not (part is None) and (prog.search(text) is not None)

                        self.updateColors(row, col, state)
                        if state: 
                            self.aFoundRow.append(row)

    def doSearchAndHighlight(self, searchString):
        self.highlightItems(searchString)
        if not searchString is None:
            self.lastSearch = searchString
            self.setFoundVisible()

    def changeVisibility(self, col, isVisible):
        self.dStatus[self.aHeader[col]] = isVisible
        if isVisible:
            self.showColumn(col)
        else:
            self.hideColumn(col)

        if col != 0:
            for row in range(self.rowCount()):
                if not(self.item(row, col) is None and self.cellWidget(row, col) is None):
                    if isVisible:
                        self.showRow(row)
                    else:
                        self.hideRow(row)

        # Adjust the row heights based on column 0 visibility              
        visibleCol0 = self.dStatus[self.aHeader[0]]
        for row in range(self.rowCount()):
            if not self.isRowHidden(row):
                maxCnt = self.aaLineCount[row][1]
                if visibleCol0 and maxCnt < self.aaLineCount[row][0]:
                    if self.aaLineCount[row][0] < 40:
                        maxCnt = max(self.aaLineCount[row][0], maxCnt)
                    else:
                        maxCnt = max(maxCnt, 20);

                self.setRowHeight(row, maxCnt * self.defaultRowHeight)

    def reset(self):
        self.setColumnCount(0)
        self.setRowCount(0)
        self.aHeader = []
        self.aGroupName  = []
        self.dStatus     = { "Enable All" : True}
        self.aaLineCount = []
        self.aFoundRow   = []
        self.foundIdx    = 0
        self.lastSearch  = ""

    def addUniqueColumn(self, name : str, tooltip):
        if not name in self.aHeader:
            self.addColumn(name, tooltip)
            aN = name.split("_")
            self.dStatus[name] = True
            newName = name if len(aN) < 2 else "_".join(aN[:-1])
            if not newName in self.aGroupName:
                self.aGroupName.append(newName)
                self.aGroupName.sort()
                self.dStatus[newName] = True

        return self.aHeader.index(name)

    def addColumn(self, name, tooltip):
        col = self.columnCount();
        self.setColumnCount(col+1)
        twi = cTableWidgetItem(name, self.fixedFont)
        if tooltip is not None:
            twi.setToolTip(tooltip)
        self.setHorizontalHeaderItem(col,twi)
        self.aHeader.append(name);
        return twi

    def addRowHeader(self, row, header):
        if self.rowCount() <= row:
            self.aaLineCount += [ [0, 0] ] *(row + 1 - self.rowCount())
            self.setRowCount(row + 1);
        twi = cTableWidgetItem(header, self.fixedFont)
        twi.setTextAlignment(Qt.AlignRight)
        self.setVerticalHeaderItem(row, twi);

    def getItemText(self, row, col, dflt = None):
        twi = self.item(row, col)
        if twi is None:
            textEdit = self.cellWidget(row, col)
            return dflt if textEdit is None else textEdit.toPlainText()
        
        return twi.text()
        
    def addItem(self, row, col, text, error = False, highlight = False, errorText = None):
        if self.rowCount() <= row:
            self.aaLineCount += [ [0, 0] ] *(row + 1 - self.rowCount())
            self.setRowCount(row + 1);

        text = text.strip()
        text = text.replace("\n\n", "\n")
        aText = text.split("\n")
        lineCount = len(aText) + 1
        
        if 40 < lineCount:
            textEdit = cTextEdit("<br>".join(aText), self.fixedFont)
            textEdit.setErrorFlag(error or errorText is not None)
            self.setCellWidget(row, col, textEdit)
            lineCount = 40
        else:
            twi = cTableWidgetItem(text, self.fixedFont)
            twi.setErrorFlag(error or errorText is not None)
            self.setItem(row, col, twi)

        # print(f"{type(self.cellWidget(row, col)) = }")

        if error or errorText is not None:
            self.horizontalHeaderItem(col).setErrorFlag(True) # setBackground(self.errorBrush)

        if col != 0:
            self.aaLineCount[row][1] = max(self.aaLineCount[row][1], lineCount)
            newHeight = self.defaultRowHeight * self.aaLineCount[row][1]
            self.setRowHeight(row, newHeight)
        else:
            self.aaLineCount[row][0] = lineCount

    def updateToolTip(self, name, row, toolTip):
        col = self.aHeader.index(name)
        if 0 <= col:
            twi = self.item(row, col)
            if twi is None:
                twi = self.cellWidget(row, col)
                if twi is None:
                    return

            if isinstance(toolTip, str):
                tt = toolTip
            else:
                val = toolTip[1]
                if toolTip[2] == 0:
                    aTT = [ f"<tr><td>{v[0]}</td><td>{v[1]}</td><td>{(val >> v[1]) & 1}</td></tr>" if 1 == v[2] else f"<tr><td>{v[0]}</td><td>{v[1]+v[2]-1}:{v[1]}</td><td>0x{(val >> v[1]) & ((1 << v[2]) - 1):X}</td></tr>" for v in toolTip[3:]]
                    tt = toolTip[0] + "<table><tr><th>BitField</th><th>Bits</th><th>Val</th></tr>" + "\n".join(aTT) + "</table>"
                elif toolTip[2] == 1:
                    aTT = [ f"<tr><td>{v[0]}</td><td>{v[1]}</td><td>{(val >> v[1]) & 1}</td><td>{v[3]}</td></tr>" if 1 == v[2] else f"<tr><td>{v[0]}</td><td>{v[1]+v[2]-1}:{v[1]}</td><td>0x{(val >> v[1]) & ((1 << v[2]) - 1):X}</td><td>{v[3]}</td></tr>" for v in toolTip[3:]]
                    tt = toolTip[0] + "<table><tr><th>BitField</th><th>Bits</th><th>Val</th><th>Descr</th></tr>" + "\n".join(aTT) + "</table>"
                else:
                    tt = "Unknown type"

            twi.setToolTip(tt)

    def markColumnErrors(self, name, aRow):
        col = self.aHeader.index(name)
        if 0 <= col:
            for row in aRow:
                twi = self.item(row, col)
                twi = self.cellWidget(row, col) if twi is None else twi
                twi.setErrorFlag(True)
            self.horizontalHeaderItem(col).setErrorFlag(True) #.setBackground(self.errorBrush)

    def changeColorSchema(self, schema):
        print("Update Color Schema")
        self.table.updateSchema(schema)

class cDapLogWidget(QMainWindow):
    """This is the main window for the Dap Log Eater Tab"""
    def createButton(self, toolbar : QToolBar, name : str, callBack, key):
        ret = QAction(name)
        ret.triggered.connect(callBack)
        if not key is None:
            ret.setShortcut(key)
        toolbar.addAction(ret)
        return ret
    
    def createEdit(self, toolbar : QToolBar, text : str, callback):
        ret = QLineEdit(text)
        toolbar.addWidget(ret)
        ret.editingFinished.connect(callback)
        return ret
        
    def createText(self, toolbar : QToolBar, text : str):
        ret = QLabel(text)
        toolbar.addWidget(ret)
        return ret
    
    def actionExportAsCSV(self):
        self.table.exportAsCSV()

    def __init__(self, clipboard):
        super().__init__()
        self.clipboard = clipboard
        self.table     = cDapLogTableWidget(self)
        
        toolbar = QToolBar(self)
        self.exportBtn   = self.createButton(toolbar, "Export", self.actionExportAsCSV, None)
        self.gotoFirst   = self.createButton(toolbar, "First", self.actionGotoFirst, None)
        self.gotoPrev    = self.createButton(toolbar, "Previous", self.actionGotoPrev, QKeySequence("Ctrl+p"))

        self.indexText  = self.createEdit(toolbar, "", self.actionUpdateIndex)
        self.indexText.setMaximumWidth(5*10)
        self.indexLabel = self.createText(toolbar, " of 0")
        
        self.gotoNext  = self.createButton(toolbar, "Next", self.actionGotoNext, QKeySequence("Ctrl+n"))
        self.gotoLast = self.createButton(toolbar, "Last", self.actionGotoLast, None)
        
        toolbar.addSeparator()
        self.searchLabel = self.createText(toolbar, "Search:")
        self.searchText  = self.createEdit(toolbar, "", self.actionUpdateSearch)
        self.addToolBar(toolbar)
        self.setCentralWidget(self.table)
        self.enableButtons(False)

    def enableButtons(self, flag : bool):
        self.gotoFirst.setEnabled(flag)
        self.gotoPrev.setEnabled(flag)
        self.gotoNext.setEnabled(flag)
        self.gotoLast.setEnabled(flag)
        self.indexText.setEnabled(flag)

    def actionUpdateIndex(self):
        if self.indexText.text().isnumeric():
            self.table.goto(int(self.indexText.text()) - 1)
        else:
            self.indexText.setText("")
    
    def updateIndexText(self, index : int):
        if (not self.indexText.text().isnumeric()) or index != int(self.indexText.text()):
            self.indexText.setText(f"{index}")
        
    def actionUpdateSearch(self):
        searchString = self.searchText.text()
        self.table.doSearchAndHighlight(searchString if 0 < len(searchString) else None)
        count = self.table.getFoundCount()
        self.enableButtons(0 < count)
        self.indexLabel.setText(f" of {count}")
        self.indexText.setText(f"{self.table.getCurrentIndex(0)+1}" if 0 < len(searchString) else "")

    def actionGotoFirst(self):
        self.table.goto(0)

    def actionGotoLast(self):
        self.table.goto(-1)

    def actionGotoPrev(self):
        self.table.goto(self.table.getCurrentIndex(-1))
    
    def actionGotoNext(self):
        self.table.goto(self.table.getCurrentIndex(+1))

    def addItem(self, row : int, col : int, text : str, error = False, highlight = False, errorText = None) -> None:
        self.table.addItem(row, col, text, error, highlight, errorText)

    def addRowHeader(self, row : int, header : str):
        self.table.addRowHeader(row, header)
        
    def changeVisibility(self, col : int, isVisible : bool):
        return self.table.changeVisibility(col, isVisible)

    def addUniqueColumn(self, name : str, tooltip):
        return self.table.addUniqueColumn(name, tooltip)
    
    def reset(self):
        self.table.reset()

    def markColumnErrors(self, name, aRow):
        return self.table.markColumnErrors(name, aRow)

    def changeColorSchema(self, schema):
        self.table.updateSchema(schema)

    def setToolTip(self, name, row, toolTip):
        return self.table.updateToolTip(name, row, toolTip)

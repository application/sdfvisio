###
# 
# SDF Visualizer, 
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Contains:
# class cTrackDeviceRegister: trace device specific register values
#

from .cscomponent import *

dDevTypeList = {
        "A-Generic" : cArmv8Debug,
        "CSCTI" : cCSCTI,
}

class cTrackDeviceRegister:
    def __init__(self, device):
        self.dapLogTab = None
        self.component = self.getComponentInstance(device)

    def getComponentInstance(self, device):
        if device.devType in dDevTypeList:
            return dDevTypeList[device.devType](device)
        else:
            return cCSGeneric(device)

    def updateRegister(self, row, regName, aReg, val, dapLogTab):
        """track register value changes and return True, in case the register values is bad"""
        return self.component.updateRegister(row, regName, aReg, val, dapLogTab)

    def getCombineRegisters(self, regList, shiftWidth):
        return self.component.getCombineRegisters(regList, shiftWidth)

    def getPID(self):
        return self.component.getID()

    def getCID(self):
        return self.component.getCID()

    def performComponentTest(self, name):
        self.component.performComponentTest(name)
        
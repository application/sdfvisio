###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
Handle the various color schema
"""
from PySide6.QtWidgets import QLineEdit, QLabel, QColorDialog
from PySide6.QtWidgets import QDialog, QDialogButtonBox, QGridLayout, QPushButton, QVBoxLayout
from PySide6.QtGui     import QPen, QFont, QColor, QBrush
from .qtsettings       import settings

# Entries are a tuple of attribute name, color [, pen width]
defaultColors = [
  ("Light", 
    [("Default", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"    , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0xff), 3),

       ("red"          , QColor(0x60, 0x60, 0xff), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0x00, 0xc0, 0x00), 3),
       ("tracesource"  , QColor(0x00, 0x00, 0xc0), 3),
       ("defaultBorder", QColor(0x00, 0x00, 0x00), 3),

       ("ERRORtab"     , QColor(0xc0, 0x00, 0x00), 3),
       ("INFOtab"      , QColor(0x00, 0x00, 0xc0), 3),

       ("AP"           , [QColor(r, g, b) for r in range(0xff, 0xc0-2, -0x20) for g in range(0xff, 0xc0-2, -0x20) for b in range(0xff, 0xc0-2, -0x20)]),
       ("highlight"    , QColor(0xff, 0xe1, 0x1f), 7))
     ),
     ("Grey", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"   , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0x60), 3),

       ("red"          , QColor(0x60, 0x60, 0x60), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0xc0, 0xc0, 0xc0), 3),
       ("tracesource"  , QColor(0xc0, 0xc0, 0xc0), 3),
       ("defaultBorder", QColor(0xc0, 0xc0, 0xc0), 3),

       ("ERRORtab"     , QColor(0x80, 0x80, 0x80), 3),
       ("INFOtab"      , QColor(0xc0, 0xc0, 0xc0), 3),

       ("AP"           , [QColor(c, c, c) for c in range(0xff, 0xa0-2, -0x18)]),
       ("highlight"    , QColor(0xc0, 0xc0, 0xc0), 7))
     ),
    ("Deuteranomaly", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"    , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0xff), 3),

       ("red"          , QColor(0x60, 0x60, 0xff), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0xc0, 0xc0, 0x00), 3),
       ("tracesource"  , QColor(0x00, 0x00, 0xc0), 3),
       ("defaultBorder", QColor(0x00, 0x00, 0x00), 3),

       ("ERRORtab"     , QColor(0xc0, 0x00, 0x00), 3),
       ("INFOtab"      , QColor(0x00, 0x00, 0xc0), 3),

       ("AP"           , [QColor(0xff, g, b) for g in range(0xff, 0xc0-2, -0x20) for b in range(0xff, 0xc0-2, -0x20)]),
       ("highlight"    , QColor(0xff, 0xe1, 0x1f), 7))
     ),
    ("Tritanomaly", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"    , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0xff), 3),

       ("red"          , QColor(0x60, 0x60, 0xff), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0x00, 0xc0, 0x00), 3),
       ("tracesource"  , QColor(0xc0, 0x00, 0xc0), 3),
       ("defaultBorder", QColor(0x00, 0x00, 0x00), 3),

       ("ERRORtab"     , QColor(0xc0, 0x00, 0x00), 3),
       ("INFOtab"      , QColor(0x00, 0x00, 0xc0), 3),

       ("AP"           , [QColor(r, g, 0xff) for r in range(0xff, 0xc0-2, -0x20) for g in range(0xff, 0xc0-2, -0x20)]),
       ("highlight"    , QColor(0xff, 0xe1, 0x1f), 7))
     ),
    ],
  ),
  ("Dark",
    [("Default", (
       ("foreground"   , QColor(0xff, 0xff, 0xff), 3),
       ("background"   , QColor(0x20, 0x20, 0x20), 3),

       ("textcolor"    , QColor(0xff, 0xff, 0xff), 3),

       ("first"        , QColor(0xc0, 0xc0, 0xc0), 3),
       ("seconds"      , QColor(0x60, 0x60, 0x00), 3),

       ("red"          , QColor(0xa0, 0xa0, 0x00), 3),
       ("artcstf"      , QColor(0x88, 0x88, 0x88), 3),
       ("artrepl"      , QColor(0xa0, 0xa0, 0xa0), 3),

       ("tracesink"    , QColor(0x34, 0x65, 0xa4), 3),
       ("tracesource"  , QColor(0x4a, 0x9e, 0x06), 3),
       ("defaultBorder", QColor(0xff, 0xff, 0xff), 3),

       ("ERRORtab"     , QColor(0x40, 0xff, 0xff), 3),
       ("INFOtab"      , QColor(0xff, 0xff, 0x40), 3),

       ("AP"           , [QColor(r, g, b) for r in range(0x00, 0x40+2, 0x20) for g in range(0x00, 0x40+2, 0x20) for b in range(0x00, 0x40+2, 0x20)]),
       ("highlight"    , QColor(0x00, 0x10, 0xf8), 7)),
     ),
     ("Grey", (
       ("foreground"   , QColor(0xff, 0xff, 0xff), 3),
       ("background"   , QColor(0x00, 0x00, 0x00), 3),

       ("textcolor"    , QColor(0xff, 0xff, 0xff), 3),

       ("first"        , QColor(0xc0, 0xc0, 0xc0), 3),
       ("seconds"      , QColor(0xa0, 0xa0, 0xa0), 3),

       ("red"          , QColor(0xb0, 0xb0, 0xb0), 3),
       ("artcstf"      , QColor(0x88, 0x88, 0x88), 3),
       ("artrepl"      , QColor(0xa0, 0xa0, 0xa0), 3),

       ("tracesink"    , QColor(0xf0, 0xf0, 0xf0), 3),
       ("tracesource"  , QColor(0xe0, 0xe0, 0xe0), 3),
       ("defaultBorder", QColor(0xff, 0xff, 0xff), 3),

       ("ERRORtab"     , QColor(0xc0, 0xc0, 0xc0), 3),
       ("INFOtab"      , QColor(0x80, 0x80, 0x80), 3),

       ("AP"           , [QColor(c, c, c) for c in range(0x00, 0x60+2, 0x18)]),
       ("highlight"    , QColor(0x10, 0x10, 0x10), 7))
     ),
    ],
  ),
  ("User", 
    [("One", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"    , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0xff), 3),

       ("red"          , QColor(0x60, 0x60, 0xff), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0x00, 0xc0, 0x00), 3),
       ("tracesource"  , QColor(0x00, 0x00, 0xc0), 3),
       ("defaultBorder", QColor(0x00, 0x00, 0x00), 3),

       ("ERRORtab"     , QColor(0xc0, 0x00, 0x00), 3),
       ("INFOtab"      , QColor(0x00, 0x00, 0xc0), 3),

       ("AP"           , [QColor(r, g, b) for r in range(0xff, 0xc0-2, -0x20) for g in range(0xff, 0xc0-2, -0x20) for b in range(0xff, 0xc0-2, -0x20)]),
       ("highlight"    , QColor(0xff, 0xe1, 0x1f), 7))
     ),
     ("Two", (
       ("foreground"   , QColor(0x00, 0x00, 0x00), 3),
       ("background"   , QColor(0xff, 0xff, 0xff), 3),

       ("textcolor"   , QColor(0x20, 0x20, 0x20), 3),

       ("first"        , QColor(0x40, 0x40, 0x40), 3),
       ("seconds"      , QColor(0x60, 0x60, 0x60), 3),

       ("red"          , QColor(0x60, 0x60, 0x60), 3),
       ("artcstf"      , QColor(0xa0, 0xa0, 0xa0), 3),
       ("artrepl"      , QColor(0x88, 0x88, 0x88), 3),

       ("tracesink"    , QColor(0x00, 0xc0, 0x00), 3),
       ("tracesource"  , QColor(0x00, 0x00, 0xc0), 3),
       ("defaultBorder", QColor(0x00, 0x00, 0x00), 3),

       ("ERRORtab"     , QColor(0x80, 0x80, 0x80), 3),
       ("INFOtab"      , QColor(0xc0, 0xc0, 0xc0), 3),

       ("AP"           , [QColor(r, g, b) for r in range(0xff, 0xc0-2, -0x20) for g in range(0xff, 0xc0-2, -0x20) for b in range(0xff, 0xc0-2, -0x20)]),
       ("highlight"    , QColor(0xc0, 0xc0, 0xc0), 7))
     ),
    ],
  ),
]

class cSceneColor():
    def __init__(self, group, name, schema):
        self.group = group
        self.name  = name

        self.brush = {}
        self.pen   = {}

        for entry in schema:
            colorName = entry[0]
            color     = entry[1]
            if type(color) is QColor:
                self.brush[colorName] = QBrush(color)
                self.pen[colorName]   = QPen(color)
                if 2 < len(entry) and entry[2]:
                    self.pen[colorName].setWidth(entry[2])
            elif type(color) is type([]):
                self.brush[colorName] = [QBrush(c) for c in color]
            else:
                print("Skip:", self.group, self.name, entry, type(color))
        self.updateColorModeFromSettings()

    def updateColorModeFromSettings(self):
        prefix = self.getColorPath("pen")
        for key in settings.getGroupKeys(prefix):
            # update all pens
            penColor = settings.getStr(prefix + "/" + key)
            if penColor:
                value = [ int(part) for part in penColor.split(",") if part.isnumeric()]
                pen = self.pen.get(key)
                if pen and 4 == len(value):
                    pen.setColor(QColor(value[0], value[1], value[2]))
                    pen.setWidth(value[3])
                    brush = self.brush.get(key)
                    if brush:
                        brush.setColor(QColor(value[0], value[1], value[2]))
            else:
                print("Key not found:", key)

        prefix = self.getColorPath("brush")
        for key in settings.getGroupKeys(prefix):
            print("Brush: ", prefix + "/" + key)


    def getColorPath(self, suffix : str):
        return f"Color_{self.group}_{self.name}/{suffix}"
    
    def getStyleSheet(self):
        return None # """* { font : font-family: "Bahnschrift"; font-size: 11px; font-style : italic } """

    def getAPbrush(self, idx):
        if 0 == len(self.brush["AP"]):
            return self.brush["background"]
        if 1 == len(self.brush["AP"]):
            return self.brush["AP"][0]
        i = idx % (len(self.brush["AP"]) - 1)
        return self.brush["AP"][i+1]

    def getDefaultBrush(self):
        return self.brush["foreground"]

    def getDefaultPen(self):
        return self.pen["foreground"]

    def getPen(self, name, idx = None):
        if idx == None:
            return self.pen.get(name, self.getDefaultPen())
        elif self.pen.get(name) != None and 0x00 <= idx and idx < len(self.pen.get(name)):
            return self.pen.get(name)[idx]
        else:
            return self.getDefaultPen()

    def getBrush(self, name, idx = None):
        if idx == None:
            return self.brush.get(name, self.getDefaultBrush())
        elif self.brush.get(name) != None and type(idx) == int:
            idx = idx % len(self.brush.get(name))

            return self.brush.get(name)[idx]
        
        return self.getDefaultBrush()

    def __str__(self):
        return self.group + ":" + self.name

    def __repr__(self):
        return self.group + ":" + self.name

class cEditColorSchema(QDialog):
    def __init__(self, parent, name):
        super().__init__(parent)
        
        self.ret          = False
        self.colorChanged = False
        self.schema       = None
        for modeList in colorModeList:
            for item in modeList:
                if item.group.lower() == "user" and item.name == name:
                    self.schema = item

        if self.schema is None:
            return

        self.setWindowTitle("Change User Color for " + name)
        self.colorLayout = QGridLayout()
        self.dButton = {}

        row = 0
        
        self.aRed   = []
        self.aGreen = []
        self.aBlue  = []

        for brush in self.schema.brush["AP"]:
            color = brush.color()
            if color.red() not in self.aRed:
                self.aRed.append(color.red())
            if color.green() not in self.aGreen:
                self.aGreen.append(color.green())
            if color.blue() not in self.aBlue:
                self.aBlue.append(color.blue())

        self.uiItems = []
        for name in self.schema.pen:
            pen = self.schema.pen[name]
            button = QPushButton(name)
            lineedit = QLineEdit(f"{pen.width()}")
            self.uiItems.append((self.updatePenBrush, name, pen, self.schema.brush[name], button, lineedit))

            button.clicked.connect(self.changeColor)
            c = pen.color()
            colorStr = "background-color: #{:02X}{:02X}{:02X}; color: #{}; ".format(
                c.red(), c.green(), c.blue(),
                "FFFFFF" if (c.red() + c.green() + c.blue()) < 3*128 else "000000")

            button.setStyleSheet("QPushButton {" + colorStr + "}")
            self.colorLayout.addWidget(QLabel(name), row, 0)
            self.colorLayout.addWidget(button, row, 1, 1, 2)
            self.colorLayout.addWidget(lineedit, row, 3)
            row += 1

        self.colorLayout.addWidget(QLabel("AP"), row, 0)
        self.colorLayout.addWidget(QLabel("Start"), row, 1)
        self.colorLayout.addWidget(QLabel("End"), row, 2)
        self.colorLayout.addWidget(QLabel("Step"), row, 3)
        row += 1

        for name in["Red", "Green", "Blue"]:
            color = getattr(self, "a"+name) 
            if 1 < len(color):
                start = color[0]
                end   = color[-1]
                delta = ((end - start) if end < start else (end - start)) // (len(color) - 1)
            else:
                start = 0
                end   = 0
                delta = 0

            leStart = QLineEdit(f"{start}")
            leEnd   = QLineEdit(f"{end}")
            leDelta = QLineEdit(f"{delta}")
            self.colorLayout.addWidget(QLabel(name), row, 0)
            self.colorLayout.addWidget(leStart, row, 1)
            self.colorLayout.addWidget(leEnd  , row, 2)
            self.colorLayout.addWidget(leDelta, row, 3)

            self.uiItems.append((self.updateAPBrush, name, leStart, leEnd, leDelta))
            row += 1

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.buttonOk)
        buttonBox.rejected.connect(self.buttonCancel)

        layout = QVBoxLayout()
        layout.addLayout(self.colorLayout)
        layout.addWidget(buttonBox)
        self.setLayout(layout)
        return

    def getButtonBackgroundColor(self, button):
        stylesheet = button.styleSheet().split()
        for idx, name in enumerate(stylesheet):
            if 0 <= name.find("background"):
                return QColor(stylesheet[idx + 1][:-1])
        return None

    def updatePenBrush(self, item):
        pen   = item[2]
        brush = item[3]
        color = self.getButtonBackgroundColor(item[4])
        width = self.strToInt(item[5])
        if pen and color:
            pen.setColor(color)
        if pen and width:
            pen.setWidth(width)
        if brush and color:
            brush.setColor(color)

    def inRange_00_ff(self, val):
        return val and 0 <= val and val <= 0xff

    def inStepRange(self, start, end, step):
        return start and end and step and (step < 0 if start > end else step > 0)

    def strToInt(self, lineedit):
        valStr = lineedit.text()
        mul = 1
        if valStr[0] == '-':
            mul = -1
            valStr = valStr[1:]

        if valStr[:2].lower() == "0x":
            return int(valStr[2:], 16) * mul
        elif valStr[0] == "0":
            return int(valStr, 8) * mul
        return (int(valStr) * mul) if valStr.isdecimal() else None

    def updateAPBrush(self, item):
        color = item[1]
        start = self.strToInt(item[2])
        end   = self.strToInt(item[3])
        step  = self.strToInt(item[4])
        if self.inRange_00_ff(start) and self.inRange_00_ff(end) and self.inStepRange(start, end, step):
            self.updateAP[color] = (start, end, step)
        else:
            self.updateAP[color] = ( 0xff, 0xff, 1)

    def changeColor(self):
        sender         = self.sender()
        oldColor       = QColorDialog(self.schema.pen[sender.text()].color())
        colorSelection = QColorDialog(oldColor)
        colorSelection.setWindowTitle("Chandler Color for " + sender.text())
        if colorSelection.exec():
            newColor = colorSelection.selectedColor()
            if newColor != oldColor:
                # change Color
                colorStr = "background-color: #{:02X}{:02X}{:02X}; color: #{}; ".format(
                        newColor.red(), newColor.green(), newColor.blue(),
                        "FFFFFF" if (newColor.red() + newColor.green() + newColor.blue()) < 3*128 else "000000")
                sender.setStyleSheet("QPushButton {" + colorStr + "}")
                self.colorChanged = True

    def buttonCancel(self):
        self.ret = False
        self.close()

    def getStart(self, color):
        item = self.updateAP.get(color)
        return item[0] if item and 3 <= len(item) else 0xff

    def getEnd(self, color):
        item = self.updateAP.get(color)
        return item[1] if item and 3 <= len(item) else 0xff

    def getStep(self, color):
        item = self.updateAP.get(color)
        return item[2] if item and 3 <= len(item) else 0xff

    def buttonOk(self):
        self.ret = True
        if self.colorChanged: # apply changes
            self.updateAP = {}

            for item in self.uiItems:
                item[0](item)

            AP = []
            for r in range(self.getStart("red"), self.getEnd("red"), self.getStep("red")):
                for g in range(self.getStart("green"), self.getEnd("green"), self.getStep("green")):
                    for b in range(self.getStart("blue"), self.getEnd("blue"), self.getStep("blue")):
                        AP.append(QBrush(QColor(r, g, b)))
            if 0 < len(AP):
                self.schema.brush["AP"] = AP

        updateSettingsFromColorMode()
        self.close()
 
    def run(self):
        if self.schema:
            self.show()
            self.exec()
        return self.ret and self.colorChanged


def updateSettingsFromColorMode():
    for colorList in colorModeList:
        for color in colorList:
            if color.group.lower() in ["user"]:
                prefix = "Color_" + color.group + "_" + color.name
                penPrefix = prefix + "/pen/"
                for penName in color.pen:
                    pen = color.pen[penName]
                    rgbw = f"{pen.color().red()},{pen.color().green()},{pen.color().blue()},{pen.width()}"
                    settings.updateStr(penPrefix + penName, rgbw, None)

                for brushName in color.brush:
                    brushList = color.brush[brushName]
                    if type(brushList) is type([]):
                        # remove all AP brushes
                        brushPrefix = prefix + "/brush/" + brushName + "/"
                        settings.remove(brushPrefix[:-1])
                        for idx, brush in enumerate(brushList):
                            rgb = f"{brush.color().red()},{brush.color().green()},{brush.color().blue()}"
                            settings.updateStr(f"{brushPrefix}{idx}", rgb, None)

def generateColorList(defaultColors):
    return [ [cSceneColor(colorList[0], colorInfo[0], colorInfo[1]) for subIdx, colorInfo in enumerate(colorList[1])] 
               for idx, colorList in enumerate(defaultColors) ]


colorModeList = generateColorList(defaultColors)
updateSettingsFromColorMode()

def getColorModeList():
    return [f"{color.group}/{color.name}" for colorList in colorModeList for color in colorList ]

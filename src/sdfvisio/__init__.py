###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This file contains the version, descripion, and revision history
#
###

"""
Declare the version, copyright, and registion information
"""
__version__= "1.0.21"

__copyright__="""
sdfvisio: Arm Ltd, Apache 2.0, https://opensource.org/licenses/Apache-2.0 
Python 3.x: PSF, PSF License, https://docs.python.org/3/license.html
PySide6: Qt, LGPL-3.0/GPL-2.0 https://www.qt.io/licensing, https://opensource.org/licenses/LGPL-3.0 and https://opensource.org/licenses/GPL-2.0
"""
__description__="""
Analyze and display the SDF files and allows export as scalable vector graphic(SVG) or bitmap(PNG)
Each AP bus is assigned a uniq background color which is reused in the topology tabs

The tool is using PySide6 licenced under LGPL-3.0/GPL-2.0, additional information https://pypi.org/project/PySide6/

For each component, a tool tip is added to provide additional information from the XML file such as the base address
In the topology window, the connection lines indiacte (for light color theme):
    - grey lines: the first connection between two components
    - blue lines: either start or end point is used by a different connection
    - green border: trace component without input, should be a trace source
    - blue border: trace component wihout output, should be a final trace sink
    - red background: com,ponent marked as exclused but used in the topology
Note: the color change isn't enirely tested
"""
__revision__="""
1.0.21: fix variable names, use list comprension, improvide robustness when used with dap log eater
1.0.20: improvide source and rename symbols
1.0.19: improve the sources be removing dependency and simplify the topology generation
1.0.18: change minimum python version to 3.11
1.0.17: make the dap log earter reaable, but not dark mode aware
1.0.16: fix the single core, SMP and multi cluster treeview and improvide DapLogEater compatibillity handling
1.0.15: change the type hints to support 3.7 to 3.9 as well
1.0.14: fix and improve the debug activity tree view
1.0.13: remove type hints for Python 3.8
        change the tree view content and add support for the multi colimn using lists
        change QTableWidgetItem/QLable to QTextEdit to support large multi-line entries
        Subclass QTableWidgetItem, QTextEdit, and QTableWidget for the Dap Log Eater output
        Add Copy to clipboard
        Add progress bar to indicate daplog eater progressing, missing: terminate processing
        
1.0.12: New collapsible code sequence (fast memory access)
        Case insensitive search
        add version in "help" and correct SDF argument handling
        improve component register handling
        Add test for the tap log eater to check for issues
1.0.11: move the Dap Log Eater UI to a separate source and add a search tool bar
1.0.10: add DAP log eater search, goto first, prev, next, last and export as CSV
       Add line tool tips to indicate from to path
       Improvide the search to accept regular expressions
       Enabled daplog eater processing with EATER as index and create artificial index
1.0.9: move to Arm GitLab
1.0.8: Update Copyright messages
1.0.7: Enable dap log eater only if package is installed, otherwise disable the button
1.0.6: Fix path highligh, add raw log data as column 0 to the daplogeater tab, 
       limit displayed lines to 48; primarily if Raw Data view is enabled
       add lase recently used to dap log file per SDF file
1.0.5: Replace the sequence analysis with a state machine for future additional functions
1.0.4: Add experimental DAP Log Eater table generation, future to be added to SDFvisio, improve the dap log eater context menu
1.0.3: Add left mouse click & panning for the display
1.0.2: fix issue with Armv6 DAP controller, fix issue with funnel/replicator display, fix issue with key assignment 
1.0.1: Fix issue with missing component base address,
       improve component tool tip
       Change context menue
           Display address to Monospace
           Sort the component names by name numerically by extension number
       Mark a CSTF -> CSTMC with CSTMS already has an input as a "loop" and do not add the connection
1.0.0: Split the revision history from the description in the help menu, change default settings in a new environment and add missing Shadown settings
       Clenaup the sources and add Apache 2.0 copyright messages. Re-enable blink function for searching boxes, fix SDF image export issue 
       Improve Recent file history
0.9.9: added global settings, switchable shadow and cureved interface, save an resore user color and recent open SDF file list
0.9.2.2: add selecting Light / Dark / User mode color and color changes
.9.2: Add context menu to change the highlighted paths in the trace topology
0.9.1: Add option -oldlayout and improve new boxlayout, change names in topology check to device type name
0.9.0: Add AP type to CSMEMAP, add unused trace component to Trace Checks list, fix issues with application abort, use ProbeMode to determine JTAG / SWD
0.8.9: Add cross checks for SDF files to specify loops in in the trace topology and CSTF/Repl/ETMs without inputs or outputs, TMC without inputs
       fix issues with the loop detection
0.8.8: improve box layout if multiple destinations have one source and excluded form DTSL display, fix sinktop display
0.8.7: Set AP index to 0 if not provided by SDF, change tool hop for added CSTF/REPL
0.8.6: improvide identifiaction of CSTF/REPL by using the device type
0.8.5: Fix issue at with scan chain display for SWD only targets
0.8.4: Fix issue at exit
0.8.3: Add a tap for the JTAG scan chain, change the outline for the CFST and CSREPL
0.8.2: increase robustness for the debug configuration tree
0.8.1: fix some UI issues with PySide6 port, (context menue, scroll wheel)
0.8.0: move to PySide6
0.7.7: fix issue with recursive topology
0.7.6: some cleanup
0.7.5: change option names from add_1ton/add_nto1 to addtf/addtr
0.7.4: use the font text width rather an estimate
0.7.3: widen box, Add config type to component box, and add "search by address"
0.7.2: Improvide highlight feture, box layout for replicator, and add zoom feature
0.7.1: Add context menu to find Coresight blocks
0.7.0: Add options
       --add_nto1: create missing trace funnel
       --add_1ton: create missing trace replicator
       --widerect: narrow the rectangles to the requires size based on input and output
       Switch to PySide2 due to LGPLv3 license requirement
0.6.2: Add About dialog
0.6.1: Add SVG output
0.6.0: Add menue and export as PNG files
0.5.x: Synchronize AP collors, add input / output number is needed
0.4.x: add border colors to indicate trasce topology (green: source, blue: sink), 
"""

###
#
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
Class Definition:
  cJTAGTAB: analyze the XML object and fill in the settingsa about IR length, TAB type and SWD enable and supported
"""
class cJTAGTAB:
    """Handle the JTAG and SWD TAP information"""
    def __init__(self, dev, isSWJEn):
        """Initialize the JTAG TAP information to be drawn later"""
        self.tag      = dev.tag
        self.irLen    = int(dev.attrib["irLength"]) if "irLength" in dev.attrib else 0
        self.name     = dev.attrib["name"]     if "name"     in dev.attrib else None
        self.type     = dev.attrib["type"]     if "type"     in dev.attrib else ""
        self.isSWJEn  = isSWJEn
        self.dDevInfo = {}

        self.dDevInfo = { dii.attrib["name"]: dii.text for diis in dev.findall("device_info_items") for dii in diis.findall("device_info_item") }

        if self.irLen == 0 and not isSWJEn:
            # try to determine the IR length
            if self.type[:8] == "UNKNOWN_":
                self.irLen = int(self.type[8:])
            elif self.type in ["ARMCS-DP", "ETB", "ETM"]:
                self.irLen = 4
            elif self.type in ["ARM1176JZF-S"]:
                self.irLen = 5

    def getAttribute(self):
        """Return the attributes in an Array
        LATER: this does not look right"""
        aRet = [f"Name : {self.name}"]

        for name, item in self.dDevInfo.items():
            aRet.append(f"   {name} : {item}")

        if self.isSWJEn:
            aRet.append("SWD: true")
        else:
            aRet.append(f"IR length: {self.irLen}")
            if self.type != "":
                aRet.append(f"Type: {self.type}")
            if self.tag is not None:
                aRet.append(f"Tag: {self.tag}")
        return aRet

    def getToolTip(self) -> str:
        """Return the tool tip string"""
        return "\n".join(self.getAttribute())

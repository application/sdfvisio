###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
type definition:
cSettings: handle the settings for sdfvisio, the section "Global" contains the global settings
    base class: QSettings

Global symbol:
settings: this is the instance for the global and file specific settings.
"""
from PySide6.QtCore import QSettings
from PySide6.QtCore import QCoreApplication

QCoreApplication.setOrganizationName("Arm");
QCoreApplication.setOrganizationDomain("https://developer.arm.com");
QCoreApplication.setApplicationName("SDFvisio");

class cSettings(QSettings):
    def __init__(self):
        super().__init__(QSettings.IniFormat, QSettings.UserScope, "Arm", "sdfvisio")

    def clearGroupKeys(self, group : str, key = None):
        self.beginGroup(group)
        self.remove(key if key else "")
        self.endGroup()

    def getGroupKeys(self, group : str):
        self.beginGroup(group)
        aRet = self.childKeys()
        self.endGroup()
        aRet.sort()
        return aRet

    def updateBool(self, name : str, value, default):
        if value != None:
            self.setValue(name, value)
        elif default != None and self.value(name) == None:
            self.setValue(name, default)

    def updateInt(self, name : str, value, default):
        if value != None:
            self.setValue(name, value)
        elif default and self.value(name) == None:
            self.setValue(name, default)

    def updateStr(self, name : str, value, default):
        if value != None:
            self.setValue(name, value)
        elif default and self.value(name) == None:
            self.setValue(name, default)

    def getBool(self, name : str):
        ret = self.value(name, None)
        if type(ret) == bool:
            return ret
        if type(ret) == str:
            return ret.lower() == "true"
        return ret.toBool() if ret != None else None

    def getInt(self, name : str):
        ret = self.value(name, None)
        if type(ret) == int:
            return ret
        if type(ret) == str:
            return int(ret)
        return ret.toInt() if ret != None else None

    def getStr(self, name : str):
        """Return None if the key does no exist, otherwise the string"""
        ret = self.value(name, None)
        if type(ret) == str:
            return ret
        return ret.toString() if ret != None else None

    def getPrefixedName(self, aPrefix : list, name : str):
        for prefix in aPrefix:
            if not self.value(prefix + '/' + name) is None:
                return prefix + '/' + name
        return None
    
    def update(self):
        self.sync()

settings = cSettings()


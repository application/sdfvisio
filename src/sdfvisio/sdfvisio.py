###
# 
# SDF Visualizer, sdfvisio.py script
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Contains:
#   All required imports
#   Main class to run script
#
###

"""
type definition:
cSSDF: handle a single SDF file
cSDFMain: 
"""
# Import python libraries to handle the XML file and populate internal data structures
import xml.etree.ElementTree as ETree
import traceback

# Import project files
from .devicehierarchy import makeDebugTraceDeviceList
from .devicehierarchy import makeJtagDeviceList
from .link import cLink, cArtEntry
from .layouttopology import cTopo
from typing import Callable, Iterator, Union, Optional

from .qtdisplay  import cDisplaySDFContent, cDisplaySDF
from .qtsettings import settings
from .debugstatemachine import cDebugStateMachine

class cFromToPoint:
    """Create a hashable object for a line start and end point
    The assumption is that the X/Y coordinates are 0 - 0xffffffff
    """
    def __init__(self, x : int, y : int):
        self.x = x & 0xffffffff
        self.y = y & 0xffffffff
        self.hash = (x << 32) + y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __hash__(self):
        return self.hash
    def __str__(self):
        return f"({self.x},{self.y})"

# one class per SDF file to handle all created display tabs
class cSDF:
    """Handle the user interface for a single SDF file create the SDF specific tables and menue"""
    def __init__(self, sdfWindow, fileName : str):
        sdfWindow.createSDFTab(fileName)
        self.sdfWindow     = sdfWindow
        self.fileName      = fileName
        self.APbus         = None
        self.list          = None
        self.dTopoDisp     = {}
        self.dLinkList     = {}
        self.debugActivity = None
        self.dDevByName    = {}
        self.loopTree      = None
        self.dapLogTab     = None
        self.readRegisterName = None
        self.dDebugActivity = None
        self.dleData        = None
        self.dlePrefix      = None
        self.dCluster       = {}
        self.dTraceDev      = {}
        self.aMarkedLines   = []

        self.debugStateMachine = cDebugStateMachine(self.dDevByName, self.addDapLogLine)

    def checkGlobal(self, name : str, noTopoList : list):
        ret = False
        if noTopoList is not None:
            for i in noTopoList:
                if 0 <= name.find(i):
                    ret = True
        return ret

    def doInsert(self, old, new):
        if len(old) < len(new):
            return True
        for o, n in zip(old, new):
            nn = n[0] if isinstance(n, tuple) else n
            oo = o[0] if isinstance(o, tuple) else o

            if len(nn) < len(oo) or (len(nn) == len(oo) and nn < oo):
                return True
        return False

    def insertDebugActivity(self, newEntry):
        """for the entries based on the string length and lexical order"""
        s0 = newEntry[-1][0] if isinstance(newEntry[-1], tuple) else newEntry[-1]

        for idx, entry in enumerate(self.debugActivity):
            s1 = entry[-1][0] if isinstance(entry[-1], tuple) else entry[-1]
            if self.doInsert(entry, newEntry):
                self.debugActivity.insert(idx, newEntry)
                return None
        self.debugActivity.append(newEntry)

    def createDebugActivity(self, root):
        """Create the tree view for the debugger activities based on cores and SMP connection listed in the SDF file"""
        self.dDebugActivity = { }
        for debugActivity in root.findall(".//debug_activity"):
            if "connection_id" in debugActivity.attrib and "type" in debugActivity.attrib:
                name = debugActivity.attrib["connection_id"]
                typ  = debugActivity.attrib["type"]

                if name in self.dCluster:
                    self.dCluster[name]["type"] = typ

                if 0 <= name.find("Multi-Cluster") and typ == "SMP":
                    typ = "Multi-Cluster"

                if typ not in self.dDebugActivity:
                    self.dDebugActivity[typ] = []

                self.dDebugActivity[typ].append(name)
                if name not in self.dDebugActivity:
                    self.dDebugActivity[name] = typ

        self.debugActivity = []

        if "Multi-Cluster" in self.dDebugActivity:
            if 0 < len(self.dDebugActivity["SMP"]):
                coreCount = 0
                for smp in self.dDebugActivity["SMP"]:
                    coreCount += len(self.dCluster[smp]["cores"])

                mcPrefix = (", ".join(self.dDebugActivity["Multi-Cluster"]), 
                            f"Count: {len(self.dDebugActivity['SMP'])}, Core Count: {coreCount}")

                for smp in self.dDebugActivity["SMP"]:
                    self.insertDebugActivity([mcPrefix, (smp, ", ".join(self.dCluster[smp]["cores"])) ])
            else:
                self.insertDebugActivity([(", ".join(self.dDebugActivity["Multi-Cluster"]), "Missing clusters for connection id")])

        if "SMP" in self.dDebugActivity:
            for smp in self.dDebugActivity["SMP"]:
                if smp in self.dCluster and 0 < len(self.dCluster[smp]["cores"]):
                    smpPrefix = (smp, f"Count: {len(self.dCluster[smp]['cores'])}")
                    for core in self.dCluster[smp]["cores"]:
                        self.insertDebugActivity([smpPrefix, core])
                else:
                    self.insertDebugActivity([(smp, "Missing cores for connection id")])

        if "SINGLE_CORE" in self.dDebugActivity:
            singlePrefix = ("Single Core", f"Count: {len(self.dDebugActivity['SINGLE_CORE'])}")
            for core in self.dDebugActivity["SINGLE_CORE"]:
                self.insertDebugActivity([singlePrefix, core])

    def makeDebugActivity(self, root):
        """Collect all debug activity"""
        for cluster in root.findall(".//cluster"):
            nameTag = None
            devTag = None
            for attr in cluster.attrib:
                if attr.lower() == "name":
                    nameTag = attr
                if attr.lower() == "devices":
                    devTag = attr
            if nameTag and devTag:
                self.dCluster[cluster.attrib[nameTag]] = { "cores": list(cluster.attrib[devTag].split(":")), "type" : None }

        # Collect all componets associate with trace data collection
        for dev in root.findall(".//device"):
            if "name" in dev.attrib and "type" in dev.attrib and "irLength" not in dev.attrib:
                typ  = dev.attrib["type"] if "type" in dev.attrib else None
                name = dev.attrib["name"] if "name" in dev.attrib else None
                if name is not None and type is not None and typ in ["CSTFunnel", "CSTPIU", "CSTMC", "CSATBReplicator", "CSETM", "CSITM", "CSSTM", "CSELA600"]:
                    self.dTraceDev[name] = typ
            
        # collect all debugger activities and SMP connections
        self.createDebugActivity(root)

    def makeLinkList(self, root, optList):
        """
        call to find all debug connection (single and SMP)
        Call to find all debug components
        Find links(topology) between components
        Call to layout the boxes
        """
        self.dLinkList = {}

        dDevName = { }
        if self.list is not None:
            for dev in self.list:
                if "JEP_ID" in dev.info:
                    dDevName[dev.name] = False

        self.makeDebugActivity(root)
        # collect the type sepcific topology information
        # merge ATB and CoreTrace information

        # build the topology lists
        for link in root.findall(".//topology_link"):
            typ = link.attrib["type"]
            # merge CoreTrace and ATB into one tab
            typ = typ if typ not in ["ATB", "CoreTrace"] else "CoreTrace/ATB"
            if typ not in self.dLinkList:
                self.dLinkList[typ] = cTopo(typ, settings.getBool("Global/SinkBottom"), settings.getBool("Global/NarrowRect"), optList) # create the new toplogy description

            self.dLinkList[typ].addEntry(link, dDevName, self.dDevByName, typ, self.dTraceDev)


        # create a pseudo topology is needed with components not mentioned in the topology
        typ = "Info missing"
        for i, val in dDevName.items():
            if not val:
                if self.checkGlobal(i, optList["notopo"]):
                    if "global" not in optList:
                        optList["global"] = []
                    optList["global"].append(i)
                else:
                    if typ not in self.dLinkList:
                        self.dLinkList[typ] = cTopo(typ, settings.getBool("Global/SinkBottom"), settings.getBool("Global/NarrowRect"), optList) # create the new toplogy description
                    self.dLinkList[typ].addDummyEntry(i, dDevName, self.dDevByName)

        # for each typ perfrom the topologt layout 

        if "CoreTrace/ATB" in self.dLinkList:
            # check for missing trace componets in the topology
            if settings.getBool("Global/AddTraceFunnel"):
                self.dLinkList["CoreTrace/ATB"].addArtificialCSTF(dDevName, self.dDevByName)

            if settings.getBool("Global/AddTraceReplicator"):
                self.dLinkList["CoreTrace/ATB"].addArtificialReplicator(dDevName, self.dDevByName)

            for name, item in self.dTraceDev.items():
                self.dLinkList["CoreTrace/ATB"].addMissing(item, name)

        for typ, item in self.dLinkList.items():
            item.layoutTopology()

    def addDevice(self, col, row, height, width, dev):
        """Add a device block and add the device by name"""
        self.APbus.hierarchyBlock(col, row, height, width, dev.name, dev.address, arrows=True, device=dev)
        self.dDevByName[dev.name]    = dev

    def updateImages(self):
        """Update and size all created images and diplay them"""
        for name, item in self.dTopoDisp.items():
            item.updateImage()
        self.APbus.updateImage()

    def addErrorTab(self, msg):
        """Create an ERROR tab and display the error message"""
        self.APbus = cDisplaySDFContent("ERROR", self.sdfWindow, None, self, None)
        self.APbus.updateCurveFlag(False)
        self.APbus.updateTopDownFlag(False)
        self.APbus.addBox(20, 20, 400, 100, msg)
        self.updateImages()

    def addProblematicConfig(self):
        """Create table and add questionable information about the topology"""
        self.loopTree = self.sdfWindow.addTreeView("Trace Checks", ["Information", "Description"])

        count = len(self.aMarkedLines)
        for item in self.aMarkedLines:
            self.loopTree.addTreeItem(item)

        for i, val in self.dLinkList.items():
            if i.startswith("CoreTrace"):
                for item in val.getTopologyCheck():
                    count += 1
                    self.loopTree.addTreeItem(item)

        self.sdfWindow.updateTabTitle(f"Trace Checks ({count})")
        self.loopTree.resizeColumnToContents(0)

    def addDebugActivity(self):
        """Add debug activity to the table view"""
        if self.debugActivity is not None:
            self.coreTree = self.sdfWindow.addTreeView("Debugger Activity", ["[SMP/Cores]", "Description"])
            for i in self.debugActivity:
                self.coreTree.addTreeItem(i)
            self.coreTree.resizeColumnToContents(0)

    def updateHighlightLines(self, name : str, topo : cTopo):
        """Update highlights for a topology """
        topoDisp = self.dTopoDisp[name]
        for l in topo.aLines:
            if l.highlight:
                topoDisp.enableLineHighlight(l.sx, l.sy, l.ex, l.ey)

    def updateTabImage(self, name : str, topo : cTopo):
        """Update a table image"""
        topoDisp = self.dTopoDisp[name]

        dCoord = {} # used to check for multiple start and endpoints used and duplicated lines

        # Draw the highlighted lines
        for l in topo.aLines:
            linStart = cFromToPoint(l.sx, l.sy)
            linEnd   = cFromToPoint(l.ex, l.ey)

            fromText = f"{l.inName}" if len(l.startTxt) == 0 else f"{l.inName}[{l.startTxt}]"
            toText   = f"{l.outName}" if len(l.endTxt) == 0 else f"{l.outName}[{l.endTxt}]"

            if linStart in dCoord and dCoord[linStart][0] == linEnd:
                print(f"WARNING: Duplicated line for {name}: {linStart} -> {linEnd}")
            elif linEnd in dCoord and dCoord[linEnd][0] == linStart:
                print(f"WARNING: Duplicated line for {name}: {linStart} -> {linEnd}")

            if linStart in dCoord:
                color = "seconds"
                self.aMarkedLines.append(["Link Duplicates", "Destination", (fromText, f"{toText} and {dCoord[linStart][1]}")])
            elif linEnd in dCoord:
                color = "seconds"
                self.aMarkedLines.append(["Link Duplicates", "Source", (toText, f"{fromText} and {dCoord[linEnd][1]}")])
            else:
                color = "first"

            dCoord[linStart] = (linEnd, toText)
            dCoord[linEnd]   = (linStart, fromText)

            topoDisp.addLineHighlight(l.sx, l.sy, l.ex, l.ey)
            topoDisp.addLine(l.sx, l.sy, l.ex, l.ey, color, f"{fromText} -> {toText}")

        # Draw the boxes on top to override the end portions
        for box, boxText, device, border, highlightVisible in topo.getAllBoxes():
            topoDisp.addBox(box.x, box.y, box.dx, box.dy, boxText, 
                            dev = device, canBeMarkedRed = True, border = border, highlightVisible = highlightVisible)

        # Draw in / out numbers
        for l in topo.aLines:
            topoDisp.addLineText(l.sx, l.sy, l.ex, l.ey, l.startTxt, l.endTxt)

    def createAllTopologyTabs(self):
        """Create all topology tabs with the network connection""" 
        for i, val in self.dLinkList.items():
            text  = f"{i} (Comp: {val.countBoxes()}"
            text += f", Conn: {len(val.aLines)})" if 0 < len(val.aLines) else ")"
            # create the new tab
            topoDisp = cDisplaySDFContent(text, self.sdfWindow, apBus = self.APbus, sdf = self, topo = val)
            topoDisp.updateCurveFlag(False)
            self.dTopoDisp[i] = topoDisp 

            self.updateTabImage(i, val)

    def addDapLogLine(self, row, lastComponent, startNumber, lastNumber):
        if lastComponent == "":
            return row

        if lastComponent in self.dDevByName:
            dev = self.dDevByName[lastComponent]
            col = self.dapLogTab.addUniqueColumn(lastComponent, dev.getToolTip());
        else:
            col = self.dapLogTab.addUniqueColumn(lastComponent, None);

        if startNumber == lastNumber:
            self.dapLogTab.addRowHeader(row, startNumber);
        else:
            self.dapLogTab.addRowHeader(row, f"{startNumber}-{lastNumber}");
        
        self.dapLogTab.addItem(row, 0, self.debugStateMachine.getRawData(lastComponent))

        prefix = self.debugStateMachine.getPrefix(lastComponent)
        data   = self.debugStateMachine.getData(lastComponent)
        self.dapLogTab.addItem(row, col, f"{prefix}{data}", errorText = self.debugStateMachine.getErrorState(lastComponent));

        if lastComponent in self.dDevByName and self.debugStateMachine.inResetState(lastComponent):
            comp = self.dDevByName[lastComponent]
            if 0 < prefix.find(":"):
                comp.updateRegister(self.debugStateMachine.getPrefix(lastComponent), self.debugStateMachine.getData(lastComponent), row, self.dapLogTab)

        return row+1

    def addDapLogSummary(self, summary, updateProcessing):
        """Analyse a dap log file and add a table on a per core bases"""
        if self.dapLogTab is None:
            self.dapLogTab = self.sdfWindow.addDapLogTableView("Dap Log (!!! Experimental!!!)")

        self.dapLogTab.reset()
        
        self.dapLogTab.addUniqueColumn("Raw Data", None);
        self.dapLogTab.changeVisibility(0, False);
        row = 0

        startNumber   = None
        lastNumber    = ""
        lastComponent = ""
        lastLine      = ""
        numberIdx     = None
        self.debugStateMachine.reset();

        self.dleData   = None
        self.dlePrefix = None
        maxCount = summary.getLineCount();
        count    = 0

        for line in summary.readlines():
            count += 1
            updateProcessing(count, maxCount)

            if line[:5] == "EATER":
                numberIdx = 1 if numberIdx is None else (numberIdx + 1)
                number    = f"{numberIdx}"
                access    = line[12:20].strip()
                component = line[20:42].strip()
                offset    = line[42:53].strip()
                regname   = line[53:80].strip()
                value     = line[80:].strip()
            else:
                number    = line[:8].strip()
                access    = line[13:21].strip()
                component = line[21:43].strip()
                offset    = line[43:54].strip()
                regname   = line[54:81].strip()
                value     = line[81:].strip()

            # print(f"{number = } {access =} {component =} {offset =} {regname =} {value =}") 
            row = self.debugStateMachine.processNextInformation(row, number, component, offset, regname, access, value, line)

        row = self.addDapLogLine(row, lastComponent, startNumber, lastNumber)
        
        updateProcessing(None, maxCount)
        # The dap log is added to the table now perfrom some checks
        for name in self.dDevByName:
            comp = self.dDevByName[name]
            comp.performComponentTest(name)

        self.dapLogTab.table.resizeColumnsToContents()
        
    def changeShadowAndCurve(self):
        """Apply the change on the lane and shadow for the current created SDF file"""
        self.sdfWindow.getLastSDF().changeShadow()
        self.sdfWindow.getLastSDF().changeCurve()

class cSDFMain(cDisplaySDF):
    """Main: main operation to read the SDF files and display the topologies
    self.mainWindow: 
    """
    def __init__(self, opt : list, version : str) -> None:
        """start the UI"""
        super().__init__(version, self.addSDFFile, opt)
        self.sdf = { }

    def loop(self) -> None:
        """display loop"""
        super().loop()

    def updateImages(self, fn):
        self.sdf[fn].updateImages()

    def createDeviceHierarchy(self, sdf, root):
        height = 60
        width = 140
        spacing = 1.2 * height
        indent = width + 10
        row = 50
        column = 50

        sdf.list = makeDebugTraceDeviceList(root)
        sdf.jtag = makeJtagDeviceList(root)

        # A similar loop from the hierarchy test can be used to draw the diagram.
        # Display the block hierarchy for the MEMAP, JTAG-AP and alike
        for dapName in list(dict.fromkeys([device.dapName for device in sdf.list])):
            sdf.APbus.hierarchyBlock(column, row, height, width, dapName, device=None)
            row   += spacing # Used for vertical spacing
            maxCol = column  # Used for depth based indenting
            for dev in list(device for device in sdf.list if device.dapName == dapName):
                curCol = column + (indent * dev.depth)  # Current box column from depth
                maxCol = max(curCol, maxCol)  # Finds the maximum depth of the hierarchy
                sdf.addDevice(curCol, row, height, width, dev)
                row += spacing

            row = 50
            column = maxCol + (1.5 * width)  # Starts a new column for each DP

        x = 50
        x += 25
        bits = 0
        totalIR = 0
        for jtag in sdf.jtag:
            totalIR += jtag.irLen

        if 0 < totalIR:
            sdf.JTAG.addText(x, 100,"TDI")
            sdf.JTAG.addLine(x, 120, x + 50, 120)
            x += 50
            for jtag in sdf.jtag:
                sdf.JTAG.addText(x - 20, 100, f"{bits}")
                sdf.JTAG.addText(x - 20, 120, f"{totalIR - bits}")
                sdf.JTAG.addBox(x, 80, 100, 80, jtag.name, toolTip = jtag.getToolTip())
                x += 100
                sdf.JTAG.addLine(x, 120, x + 50, 120)
                x += 50
                bits += jtag.irLen
            sdf.JTAG.addText(x - 50, 100, f"{bits}")
            sdf.JTAG.addText(x - 50, 120, f"{totalIR - bits}")
            sdf.JTAG.addText(x,      100, "TDO")
        else: # should be a SWD only target
            y = 80
            for jtag in sdf.jtag:
                sdf.JTAG.addLine(x - 60, y + 20, x + 50, y + 20)
                sdf.JTAG.addText(x - 60, y, "SWDCLK")
                sdf.JTAG.addLine(x - 60, y + 60, x + 50, y + 60)
                sdf.JTAG.addText(x - 60, y+40, "SWDIO")
                sdf.JTAG.addBox(x, y, 100, 80, jtag.name, toolTip = jtag.getToolTip())

                y += 100
    
    def addSDFFile(self, fn : str) -> None:
        """ analyze a new SDF file"""
        if fn is None:
            return
        sdf = cSDF(self.mainWindow, fn)
        self.sdf[fn] = sdf
        try:
            f = open(fn, "r")
            f.close()
        except IOError:
            sdf.addErrorTab("Unable to open file: " + fn)
            return

        try:
            tree = ETree.parse(fn)
            root = tree.getroot()  # Create XML tree
        except :
            # if no XML file, abort processing
            sdf.addErrorTab("Unable to parse SDF file: " + fn)
            traceback.print_exc()
            return 

        # Create hierarchy display tab
        sdf.JTAG  = cDisplaySDFContent("JTAG/SWD", self.mainWindow, None, sdf, None)
        sdf.JTAG.updateCurveFlag(False)
        sdf.APbus = cDisplaySDFContent("Hierarchy", self.mainWindow, None, sdf, None)
        sdf.APbus.updateCurveFlag(False)
        self.createDeviceHierarchy(sdf, root)

        # -------------------- Topology Parameters -------------------------
        # Create topology tabs for Trace, CTI, and others
        # this happens in makelinklist
        
        sdf.makeLinkList(root,self.opt)
        sdf.createAllTopologyTabs()
        sdf.addDebugActivity()
        sdf.addProblematicConfig()

        self.genAdditionalTab(sdf, "Excluded from DTSL", "excluded_dtsl")
        self.genAdditionalTab(sdf, "Global components", "global")
        # render the generate table 
        self.updateImages(fn)
        
        # update shadow and line rendering
        sdf.changeShadowAndCurve()

    def genAdditionalTab(self, sdf : cSDF, title : str, name : str):
        """create the additional tabs if the named option is enabled"""
        if name in self.opt:
            title += f" ({len(self.opt[name])})"
            topoDisp = cDisplaySDFContent(title, self.mainWindow, sdf.APbus)
            topoDisp.updateCurveFlag(False)
            sdf.dTopoDisp[name] = topoDisp
            x = 50
            y = 50
            for i in self.opt[name]:
                topoDisp.addBox(x, y, 100, 50, i)
                if x < 640:
                    x += 100 + 20
                else:
                    x  = 50
                    y += 60

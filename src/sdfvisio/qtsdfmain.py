###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
type definition:
cSelectSrcDst: create the disalogue to select the source to detination path highlighted
    base class: QDialog

class cGraphicsScene: create a new graphics screne to the JTAG, AP, and topology display
    base class: QGraphicsScene

class cGraphicsView: create a new graphics screne to the JTAG, AP, and topology display
    base class: QGraphicsView

class cSDFTabUI: Create the  main window with the buttons and tab certal widget for a single SDF file.
    base class: QMainWindow
"""

import traceback

from PySide6.QtWidgets import QCheckBox
from PySide6.QtWidgets import QComboBox
from PySide6.QtWidgets import QDialog
from PySide6.QtWidgets import QDialogButtonBox
from PySide6.QtWidgets import QGridLayout
from PySide6.QtWidgets import QVBoxLayout
from PySide6.QtWidgets import QGraphicsDropShadowEffect
from PySide6.QtWidgets import QLabel
from PySide6.QtWidgets import QFileDialog
from PySide6.QtWidgets import QMainWindow
from PySide6.QtWidgets import QMenuBar
from PySide6.QtWidgets import QMenu
from PySide6.QtWidgets import QPushButton
from PySide6.QtWidgets import QTabWidget
from PySide6.QtWidgets import QToolBar
from PySide6.QtWidgets import QLineEdit
from PySide6.QtWidgets import QGraphicsScene
from PySide6.QtWidgets import QGraphicsView
from PySide6.QtCore    import QObject
from PySide6.QtCore    import QEvent
from PySide6.QtCore    import QTimer
from PySide6.QtCore    import Qt
from PySide6.QtGui     import QAction, QPalette
from PySide6.QtGui     import QBrush, QColor

from .qtcolorscheme    import colorModeList, cEditColorSchema
from .qtprimitives     import cGraphicsTextItem, cGraphicsPathItem, cGraphicsPolygonItem
from .qtsettings       import settings

class cAddDapLogEater(QDialog):
    def __init__(self, parent, lastLogFile):
        super().__init__(parent)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.buttonOkClick)
        buttonBox.rejected.connect(self.buttonCancelClick)

        self.setWindowTitle("Add DAPlog output (!!! experimental !!!)")
        self.layout = QGridLayout()
        self.daplogButton    = QPushButton("DAPLOG file")
        self.daplog          = QLineEdit(lastLogFile if type(lastLogFile) == str else "")
        self.daplog.setMinimumWidth(400)
        self.layout.addWidget(self.daplogButton,    0, 0)
        self.layout.addWidget(self.daplog,          0, 1)

        self.daplogButton.clicked.connect(self.openFile)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.addLayout(self.layout)
        self.verticalLayout.addWidget(buttonBox)
        self.setLayout(self.verticalLayout)
        self.ret = None

    def openFile(self):
        logfile = QFileDialog.getOpenFileName(self, "Open DaplogFile", self.daplog.text(), "LOG file (*.log);;All files (*.*)");
        if not logfile is None:
            self.daplog.setText(logfile[0])

    def buttonOkClick(self):
        self.ret = (self.daplog.text(), None)
        self.close()

    def buttonCancelClick(self):
        self.ret = None
        self.close()

    def run(self):
        self.show()
        self.exec_()
        return self.ret


class cSelectSrcDst(QDialog):
    """Create the Dialog to select from / to devices for the trace path selection. If 'All' is selected, the group (corse) name is used"""
    class cList:
        def __init__(self, parent, dBoxes):
            self.dBoxes = dBoxes
            self.corse = QComboBox(parent)
            self.corse.activated.connect(self.selectCorse)

            self.fine  = QComboBox(parent)
            self.fine.activated.connect(self.selectFine)
            aBoxList = [key for key in dBoxes.keys()]
            aBoxList.sort()
            self.corse.addItems(aBoxList)
            self.selectCorse(0)

        def selectFine(self, idx):
            self.fineIdx = idx

        def selectCorse(self, idx):
            self.corseIdx = idx
            self.fine.clear()
            aSubBox = [s[0] for s in self.dBoxes[self.corse.currentText()]]
            aSubBox.sort()
            aSubBox.insert(0, "All")
            self.fine.addItems(aSubBox)
            self.selectFine(0)

        def getItem(self):
            return self.fine.currentText() if self.fineIdx != 0 else self.corse.currentText()

    def __init__(self, parent, dBoxes):
        super().__init__(parent)
        self.setWindowTitle("Select new path")

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.buttonOkClick)
        buttonBox.rejected.connect(self.buttonCancelClick)

        self.layout = QGridLayout()
        self.layout.addWidget(QLabel("Source"), 0, 0)
        self.layout.addWidget(QLabel("Destination"), 0, 1)

        self.src = self.cList(self, dBoxes)
        self.dst = self.cList(self, dBoxes)

        self.layout.addWidget(self.src.corse, 1, 0)
        self.layout.addWidget(self.dst.corse, 1, 1)
        self.layout.addWidget(self.src.fine, 2, 0)
        self.layout.addWidget(self.dst.fine, 2, 1)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.addLayout(self.layout)
        self.verticalLayout.addWidget(buttonBox)
        self.setLayout(self.verticalLayout)
        self.ret = None

    def buttonCancelClick(self):
        self.ret = None
        self.close()

    def buttonOkClick(self):
        self.ret = (self.src.getItem(), self.dst.getItem())
        self.close()

    def run(self):
        self.show()
        self.exec_()
        return self.ret

class cGraphicsScene(QGraphicsScene):
    """Subclass the QGraphicsScene to enable item selection see commente out code line"""
    def __init__(self, sdfContent):
        super().__init__()
        # self.selectionChanged.connect(self.handleSelection)
        self.sdfContent = sdfContent

    def handleSelection(self):
        self.sdfContent.multiSelectHighlight(self.selectedItems())
        self.sdfContent.updateSignal[int].emit(1)

class cGraphicsView(QGraphicsView):
    def __init__(self, parent : cGraphicsScene, name : str, schema):
        super().__init__(parent)
        self.name    = name
        self.image   = parent
        self.lastBox = None
        self.timer   = None
        self.normalBrush = None
        self.dimmBrush   = None
        self.scaleFactor = 1.0
        self.updateSchema(schema)
        self.setDragMode(QGraphicsView.ScrollHandDrag)

    def updateSchema(self, schema):
        self.schema = schema
        self.setStyleSheet(schema.getStyleSheet())

    def paintEvent(self, event):
        """In the Paint event, we set the pen and brush color feoat each object"""
        if event.type() == QEvent.Paint:
            for item in self.items():
                if isinstance(item, cGraphicsPathItem):
                    item.setPen(self.schema.getPen(item.penName))
                elif isinstance(item, cGraphicsPolygonItem):
                    item.setPen(self.schema.getPen(item.penName))
                    if self.dimmBrush and item.isSelected():
                        item.setBrush(self.dimmBrush)
                    else:
                        item.setBrush(self.schema.getBrush(item.brushName, item.brushIdx))
                elif isinstance(item, cGraphicsTextItem):
                    item.setDefaultTextColor(self.schema.getPen("foreground").color())
                else:
                    print("Unhandled item:", item)

        super().paintEvent(event)

    def doBlinkBox(self, flag):
        """Blink the current box"""
        self.lastBox.update(self.lastBox.boundingRect())
        self.lastBox.setSelected(flag)
        
    def doTimer(self):
        if self.lastBox != None and self.countDown != None:
            self.countDown -= 1
            if self.countDown > 0:
                self.doBlinkBox(not self.lastBox.isSelected())
                QTimer.singleShot(500, self.doTimer)
            else:
                self.doBlinkBox(False)
                self.lastBox = None

    def updateBlinkBox(self, box):
        self.normalBrush = box.brush()
        color            = self.normalBrush.color()
        self.dimmBrush   = QBrush(color.lighter(50))

    def startBlinkBox(self, box):
        if (self.lastBox != None):
            self.doBlinkBox(False)
            self.lastBox = None
        self.lastBox = box
        self.countDown = 2*3+1
        self.ensureVisible(box)
        self.updateBlinkBox(box)
        self.doTimer()

    def wheelEvent(self, event):
        if event.modifiers() & Qt.ControlModifier:
            if event.angleDelta().y() > 0:
                self.scaleFactor *= 1.25
                self.scale(1.25, 1.25)
            else:
                self.scaleFactor *= 0.8
                self.scale(0.8, 0.8)
            event.accept()
        else:
            center = self.sceneRect().center()
            numSteps = -event.angleDelta().y() / self.scaleFactor

            r = self.rect()
            m = self.mapToScene(r.width() / 2, r.height() / 2)

            self.centerOn(m.x(), m.y() + numSteps)
            event.accept()

class cSDFTabUI(QMainWindow):
    """Create the main window and tool bar for each SDF file"""
    def __init__(self, fileName, isDarkMode, settings):
        super().__init__()

        self.baseName = fileName.split("/")[-1].split("\\")[-1]
        self.fileName = fileName

        self.aContent    = []
        self.schemaIdx  = 0
        self.createToolBar()

        self.sdfTabs = QTabWidget()
        self.setCentralWidget(self.sdfTabs)

        self.modeList.activated.connect(self.changeMode)
        self.colorList.activated.connect(self.selectColor)
        self.shadowEffect.toggled.connect(self.changeShadow)
        self.curveStyle.toggled.connect(self.changeCurve)

        # check the current default mode light / dark
        lightIdx = None
        darkIdx  = None

        for idx, item in enumerate(colorModeList):
            self.modeList.addItem(item[0].group)
            lightIdx = idx if item[0].group.lower() == "light" else lightIdx
            darkIdx  = idx if item[0].group.lower() == "dark" else darkIdx

        currentPalette = self.palette()
        color          = currentPalette.color(QPalette.Window)

        self.modeIdx = darkIdx if isDarkMode else lightIdx
        self.modeIdx = self.modeIdx if self.modeIdx else 0
        
        self.updateToolBar()

    def contextLocateBox(self, event):
        tabIdx = self.sdfTabs.currentIndex()

        if 0 <= tabIdx and tabIdx < self.sdfTabs.count():
            tab = self.sdfTabs.widget(tabIdx)
            while tab is not None and not isinstance(tab, cSDFTabUI):
                tab = tab.parent()
            if tab is not None:
                currSDF = tab.getCurrentDisplaySDFContent()
                if currSDF is not None:
                    currSDF.contextLocateBox(event)

    def getCurrentDisplaySDFContent(self):
        idx = self.sdfTabs.currentIndex();
        if idx < 0 or len(self.aContent) <= idx:
            print(f"{len(self.aContent)} <= {idx}")
            traceback.print_stack()
            
        return self.aContent[idx] if 0 <= idx and idx < len(self.aContent) else None

    def appendTab(self, name, widget, view):
        self.sdfTabs.addTab(view, name)
        self.aContent.append(widget)

    def getBaseName(self):
        return self.baseName

    def createToolBar(self):
        self.modeList     = QComboBox()
        self.colorList    = QComboBox()
        self.shadowEffect = QCheckBox("Shadow")
        self.curveStyle   = QCheckBox("Curve")
        self.shadowEffect.setChecked(settings.getBool(settings.getPrefixedName([self.baseName, "Global"], "Shadow")))
        self.curveStyle.setChecked(settings.getBool(settings.getPrefixedName([self.baseName, "Global"], "Curve")))

        # add all required widgets
        toolbar = QToolBar();
        toolbar.addWidget(QLabel("Mode:"))
        toolbar.addWidget(self.modeList)
        toolbar.addWidget(QLabel("Color Schema:"))
        toolbar.addWidget(self.colorList)
        toolbar.addWidget(self.shadowEffect)
        toolbar.addWidget(self.curveStyle)

        self.addToolBar(toolbar);

    def changeCurve(self):
        flag = self.curveStyle.isChecked()
        settings.updateBool(self.baseName + "/" + "Curve", flag, True)
        for tab in self.aContent:
            tab.updateCurveFlag(flag)
            for item in tab.view.items():
                if isinstance(item, cGraphicsPathItem):
                    item.updatePathType(flag)

    def changeShadow(self):
        flag = self.shadowEffect.isChecked()
        settings.updateBool(self.baseName + "/" + "Shadow", flag, False)

        for tab in self.aContent:
            for item in tab.view.items():
                if isinstance(item, cGraphicsPolygonItem):
                    item.setGraphicsEffect(QGraphicsDropShadowEffect() if flag else None)

    def updateToolBar(self):
        self.modeList.setCurrentIndex(self.modeIdx)
        colorName = "default" if self.colorList.count() == 0 else self.colorList.currentText().lower()

        self.colorList.clear()
        self.schemaIdx = 0
        for idx, item in enumerate(colorModeList[self.modeIdx]):
            self.colorList.addItem(item.name)
            if item.name.lower() == colorName:
                self.schemaIdx = idx

        self.colorList.setCurrentIndex(self.schemaIdx)

    def updateSchema(self):
        for tab in self.aContent:
            if hasattr(tab, "changeColorSchema"):
                tab.changeColorSchema(self.getCurrentSchema())

    def changeMode(self, idx):
        """Toggle between light and dark modeIdx"""
        self.modeIdx   = idx
        self.schemaIdx = 0
        self.updateToolBar()
        self.updateSchema()

    def selectColor(self, idx):
        """ change table color scheme to the new selection"""
        self.schemaIdx = idx
        self.updateSchema()

    def getCurrentSchema(self):
        return colorModeList[self.modeIdx][self.schemaIdx]

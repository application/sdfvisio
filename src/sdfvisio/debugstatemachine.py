###
#
# SPDX-File: debugstatemachine.py
#
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###
from .statemachine import cState, cStateMachine

class cTrackData:
    """Keep track of the raw data and the lines created """
    def __init__(self):
        self.aRawLine = []
        self.prefix = ""
        self.aData = []

    def reset(self) -> None:
        """Rest the collected data"""
        self.aRawLine = []
        self.prefix = "UNINIT"
        self.aData = []

    def addRawLine(self, line : str) -> None:
        """Add a Dap Log Eater output"""
        self.aRawLine.append(line)

    def setPrefix(self, prefix : str) -> None:
        """set the line prefix"""
        self.prefix = prefix

    def appendPrefix(self, prefix : str) -> None:
        """append the line prefix"""
        self.prefix += prefix

    def addData(self, data):
        """Append a new line to the output"""
        self.aData.append(data)

    def replaceData(self, data):
        """Append string to the last line"""
        self.aData[-1] = data

    def appendData(self, data):
        """Append string to the last line"""
        self.aData[-1] += data

    def getRawData(self) -> str:
        """Return a string with the raw data"""
        return "\n".join(self.aRawLine)
    
    def getPrefix(self) -> str:
        """return the prefix"""
        return self.prefix
    
    def getData(self) -> str:
        """return the combined data"""
        return "\n".join(self.aData)
    
    def getErrorState(self):
        return None
    
class cArmv8DebugStateMachine(cTrackData):
    """Handle the communication for Armv8 class cores"""
    def __init__(self):
        super().__init__()
        self.count = 0
        self.registerNameLen = 12
        self.dRegVal = {}
        self.errorState = None
        self.stateMachine = cStateMachine([
            cState("START->ReadReg.0",             None, "EDITR",        "WRITE", [(0xFFFFFFE0, 0xD5130400)], self.startReadRegisterSequence),  # read register sequence
            cState("START->WriteReg.0",            None, "DBGDTRTX_EL0", "WRITE", None,                       self.startWriteRegisterSequence), # write to register
            cState("START->LDRW1X0.0",             None, "EDITR",        "WRITE", [(0xFFFFFFFF, 0xB8404401)], self.startReadMemorySequence),    # LDR sequence
            cState("START->RdSysReg.0",            None, "EDITR",        "WRITE", [(0xFFF8001F, 0xD5380000)], self.startReadSysRegSequence),
            cState("START->Running.0",             None, "EDSCR",        "READ",  [(0x00000001, 0x00000000)], self.startRunning),
            cState("SPECIAL:IGNORE",               None, "EDSCR",        "READ",  [(0x04000001, 0x04000000)], self.setTXUerror),
            cState("SPECIAL:IGNORE",               None, "EDSCR",        "READ",  [(0x08000001, 0x08000000)], self.setRXOerror),
            cState("SPECIAL:IGNORE",               None, "EDSCR",        "READ",  None,                       None),
            cState("SPECIAL:IGNORE",               None, "EDITR",        "Write", [(0xFFFFFFFF, 0xD5033FDF)], None),

            cState("ReadReg.0->FMemAcc.0",         None, "EDSCR",        "WRITE", [(0x00100001, 0x00100001)], self.startFastMemAccSequence),
            cState("ReadReg.0->ReadReg.2",         None, "DBGDTRRX_EL0", "READ",  None,                       self.setDBGDTRRX_EL0),
            cState("ReadReg.2->ReadReg.3",         None, "DBGDTRTX_EL0", "READ",  None,                       self.setDBGDTRTX_EL0andUpdate),
            cState("ReadReg.3->ReadReg.0",         None, "EDITR",        "WRITE", [(0xFFFFFFE0, 0xD5130400)], self.readRegisterValue),

            cState("WriteReg.0->WriteReg.1",       None, "DBGDTRRX_EL0", "WRITE", None,                        self.setDBGDTRRX_EL0),
            cState("WriteReg.1->WriteReg.3",       None, "EDITR",        "WRITE", [(0xFFFFFFE0, 0xD5330400)],  self.writeRegisterValue),
            cState("WriteReg.3->WriteSysReg.0",    None, "EDITR",        "WRITE", [(0xFFF8001F, 0xD5180000)],  self.startWriteSysRegisterValue),
            cState("WriteReg.3->WriteReg.0",       None, "DBGDTRTX_EL0", "WRITE", None,                        self.setDBGDTRTX_EL0),

            cState("LDRW1X0.0->LDRW1X0.3",         None, "EDITR",        "WRITE", [(0xFFFFFFFF, 0xD5130401)], None),
            cState("LDRW1X0.3->LDRW1X0.4",         None, "DBGDTRTX_EL0", "READ",  None,                       self.readMemory32),
            cState("LDRW1X0.4->LDRW1X0.0",         None, "EDITR",        "WRITE", [(0xFFFFFFFF, 0xB8404401)], None),
            cState("LDRW1X0.4->FINISHED",          None, "EDPRSR",       "READ",  None,                       self.readMemory32finish),

            cState("RdSysReg.0->RdSysReg.3",       None, "EDITR",        "WRITE", [(0xFFFFFFFF, 0xD5130400)], None), # MSR      DBGDTR_EL0,x0
            cState("RdSysReg.3->RdSysReg.4",       None, "DBGDTRRX_EL0", "READ",  None,                       self.setDBGDTRRX_EL0),
            cState("RdSysReg.4->RdSysReg.5",       None, "DBGDTRTX_EL0", "READ",  None,                       self.setDBGDTRTX_EL0andUpdate),
            cState("RdSysReg.5->RdSysReg.0",       None, "EDITR",        "WRITE", [(0xFFF8001F, 0xD5380000)], self.readRegisterValue),

            cState("FMemAcc.0->FMemAcc.2",         None, "DBGDTRTX_EL0", "READ",  None,                       None),
            cState("FMemAcc.2->FMemAcc.2",         None, "DBGDTRTX_EL0", "READ",  None,                       self.readMemory32),
            cState("FMemAcc.2->FMemAcc.5",         None, "EDSCR",        "WRITE", [(0x00100001, 0x00000001)], None),
            cState("FMemAcc.5->FINISHED",          None, "DBGDTRTX_EL0", "READ",  None,                       self.readMemory32finish),
            
            cState("WriteSysReg.0->WriteSysReg.1", None, "DBGDTRTX_EL0", "WRITE", None,                        self.setDBGDTRTX_EL0),
            cState("WriteSysReg.1->WriteSysReg.2", None, "DBGDTRRX_EL0", "WRITE", None,                        self.setDBGDTRRX_EL0),
            cState("WriteSysReg.2->WriteSysReg.3", None, "EDITR",        "WRITE", [(0xFFFFFFE0, 0xD5330400)],  self.writeRegisterValue),
            cState("WriteSysReg.3->WriteSysReg.1", None, "EDITR",        "WRITE", [(0xFFF8001F, 0xD5180000)],  self.writeSysRegisterValue),

            cState("Running.0->Running.0",         None, "EDSCR",        "READ",  [(0x00000001, 0x00000000)],  None),
            cState("Running.0->Running.0",         None, "DBGDTRTX_EL0", "READ",  None,                        self.runningReadData),
        ], "START")

    def reset(self) -> None:
        super().reset()
        self.stateMachine.reset()
        self.errorState = None

    def inResetState(self):
        return self.stateMachine.inResetState()
    
    def selectState(self, component, regname, access, value) -> bool:
        return self.stateMachine.selectState(component, regname, access, value)
    
    def getErrorState(self):
        ret = self.errorState
        self.errorState = None
        return ret
    
    def setTXUerror(self, component, regname : str, access : str, value):
        self.errorState = "EDSCR.TXU[20] set"

    def setRXOerror(self, component, regname : str, access : str, value):
        self.errorState = "EDSCR.RXO[27] set"

    def setDebugRegister(self, regname, value):
        self.dRegVal[regname] = int(value[:10], 16)
        
    def setDBGDTRTX_EL0(self, component, regname : str, access : str, value):
        self.setDebugRegister("DBGDTRTX_EL0", value)

    def setDBGDTRTX_EL0andUpdate(self, component, regname : str, access : str, value):
        self.setDebugRegister("DBGDTRTX_EL0", value)

        val = (self.dRegVal["DBGDTRRX_EL0"] << 32) + self.dRegVal["DBGDTRTX_EL0"]
        self.dRegVal[self.readRegisterName] = val
        
        if self.registerNameLen == 12:
            self.addData(f"{self.readRegisterName:>12}: 0x{val:016X}")
        else:
            self.addData(f"{self.readRegisterName:>20}: 0x{val:016X}")
        self.readRegisterName = None

    def setDBGDTRRX_EL0(self, component, regname : str, access : str, value):
        self.setDebugRegister("DBGDTRRX_EL0", value)


    def readRegisterValue(self, component, regname : str, access : str, value):
        """MSR DBGDTR_EL0, <RegName>"""
        aV = value.split(",")
        self.readRegisterName = aV[-1].upper()

    def writeRegisterValue(self, component, regname : str, access : str, value):
        """MSR <RegName>, DBGDTR_EL0"""
        aV = value.split(",")
        aR = aV[-2].split()
        reg = aR[-1].upper()
        val = (self.dRegVal["DBGDTRTX_EL0"] << 32) + self.dRegVal["DBGDTRRX_EL0"]
        self.dRegVal[reg] = val
        self.addData(f"{reg:3}: 0x{val:016X}")

    def startRunning(self, component, regname : str, access : str, value):
        # print("Start Running State")
        self.setPrefix("Running\n")

    def runningReadData(self, component, regname : str, access : str, value):
        self.addData(f"{value}")

    def startReadRegisterSequence(self, component, regname : str, access : str, value):
        self.setPrefix("Read Register\n")
        self.registerNameLen = 12
        self.readRegisterValue(component, regname, access, value)

    def startReadSysRegSequence(self, component, regname : str, access : str, value):
        self.setPrefix("Read System Register\n")
        self.registerNameLen = 20
        self.readRegisterValue(component, regname, access, value)

    def startWriteRegisterSequence(self, component, regname : str, access : str, value):
        self.setPrefix("Write Register\n")
        self.setDBGDTRTX_EL0(component, regname, access, value)

    def writeSysRegisterValue(self, component, regname : str, access : str, value):
        """Use TRTX/TRRX as value and the current disassembly as Sys  Register name"""
        aV = value.split(",")
        aR = aV[-2].split()
        reg = aR[-1].upper()
        val = (self.dRegVal["DBGDTRTX_EL0"] << 32) + self.dRegVal["DBGDTRRX_EL0"]
        self.dRegVal[reg] = val
        self.addData(f"{reg:3}: 0x{val:016X}")

    def startWriteSysRegisterValue(self, component, regname : str, access : str, value):
        self.setPrefix("Write System Register\n")
        self.writeSysRegisterValue(component, regname, access, value)

    def startFastMemAccSequence(self, component, regname : str, access : str, value):
        self.count = 0
        self.setPrefix(f"Fast Read Memory from 0x{self.dRegVal['X0']:016X}")

    def readMemory32(self, component, regname : str, access : str, value):
        x1 = int(value[:10], 16)
        self.dRegVal["DBGDTRTX_EL0"] = x1
        if self.count & 3:
            self.appendData(f" 0x{x1:08X}")
        else:
            self.addData(f"0x{self.dRegVal['X0']:016X}: 0x{x1:08X}")
        self.dRegVal['X0'] += 4
        self.count += 1

    def readMemory32finish(self, component, regname : str, access : str, value):
        self.readMemory32(component, regname, access, value)
        self.appendPrefix(f", count: {self.count}\n")

    def startReadMemorySequence(self, component, regname : str, access : str, value):
        self.count = 0
        if 'X0' in self.dRegVal:
            self.setPrefix(f"Slow Read Memory from 0x{self.dRegVal['X0']:016X}")
        else:
             self.setPrefix("Read Memory from unknown address")
             self.dRegVal['X0'] = 0


class cDummyStateMachine(cTrackData):
    """handle a generic component"""
    def __init__(self):
        pass

    def inResetState(self) -> bool:
        """it has not state machine and therefore is alwasy 'in reset' """
        return True
    
    def selectState(self, component, regname, access, value) -> bool:
        """We never select a new state"""
        return False
    
class cDebugStateMachine:
    """Stack the dap log eater output on a per compoent basis based on the select type, we choose the state machine class
    to trace the data, cuurrent classes are
    cArmv8DebugStateMachine: Armv8/v8 cortex 
    cDummyStateMachine: any unhandled state machine
    """
    def __init__(self, dDevByName : list, addDapLogLineCB):
        self.dDevByName      = dDevByName
        self.dStateMachine   = {}
        self.lastComponent   = ""
        self.startNumber     = ""
        self.lastNumber      = ""
        self.lastLine        = "                   " # must be longer than 8 char
        self.addDapLogLineCB = addDapLogLineCB

    def getRawData(self, component : str) -> str:
        """Return the raw data string"""
        return self.dStateMachine[component].getRawData() if component in self.dStateMachine else ""

    def getPrefix(self, component : str) -> str:
        """return the set prefix"""
        return f"{self.dStateMachine[component].getPrefix()}" if component in self.dStateMachine else ""

    def getData(self, component: str) -> str:
        """return the compoent data"""
        return f"{self.dStateMachine[component].getData()}" if component in self.dStateMachine else ""

    def getErrorState(self, component : str):
        return self.dStateMachine[component].getErrorState();
    
    def reset(self, component = None) -> None:
        """Reset the device (or all devices) state machine"""
        self.lastComponent = ""
        self.lastNumber    = ""
        self.lastLine      = "                   "
        if component is None:
            for component in self.dStateMachine:
                self.reset(component)
        elif component in self.dStateMachine:
            self.dStateMachine[component].reset()
    
    def createAsNeeded(self, component : str) -> None:
        """Create the necessary state machine entry based on the device type"""
        if component not in self.dStateMachine:
            if component in self.dDevByName:
                devType = self.dDevByName[component].devType
                devType = devType[:-2] if devType[-2:] == "AE" else devType
                if devType.rstrip("0123456789") in ["A-Generic", "Cortex-A", "Neoverse N", "V8-Generic", "V8_X-Generic"]:
                    self.dStateMachine[component] = cArmv8DebugStateMachine()
                else:
                    self.dStateMachine[component] = cDummyStateMachine()
            else:
                self.dStateMachine[component] = cDummyStateMachine()

    def inResetState(self, component : str) -> bool:
        """Return True is this device state machine is 'in reset state' """
        self.createAsNeeded(component)
        return self.dStateMachine[component].inResetState() if component in self.dStateMachine else True
    
    def processNextInformation(self, row: int, lineNumber : str, component : str, offset : str, regname : str, access : str, value : str, line : str):
        """Track the dap log eater output use the device associated state machine to compress the data further"""
        self.createAsNeeded(component)

        doFlush = True;
        if self.lastComponent == component:
            if self.dStateMachine[component].inResetState():
                doFlush = self.lastLine[8:] != line[8:]
            else:
                doFlush = not self.dStateMachine[component].selectState(component, regname, access, value)
                if False and regname in [ "EDSCR",  "DBGDTRTX_EL0"]:
                    print(f"1 {doFlush} {component} {regname} {access} {value}")
        else:
            doFlush = True

        self.lastLine = line

        if doFlush:
            row = self.addDapLogLineCB(row, self.lastComponent, self.startNumber, self.lastNumber)
            # record the next line
            self.startNumber   = lineNumber
            self.lastNumber    = lineNumber
            self.lastComponent = component
            
            self.createAsNeeded(component)
            self.dStateMachine[component].reset();
            doF = self.dStateMachine[component].selectState(component, regname, access, value)
            if False and regname in [ "EDSCR",  "DBGDTRTX_EL0"]:
                print(f"2 {doF} {component} {regname} {access} {value} {self.dStateMachine[component].inResetState()}")
            self.dStateMachine[component].addRawLine(line)

            if self.dStateMachine[component].inResetState():
                if regname == "UNKNOWN":
                    accessName = f"[{offset}]"
                else:
                    accessName = f"{regname}"

                if access.upper() == "READ":
                    self.dStateMachine[component].setPrefix(f"RD:{accessName} = ") 
                elif access.upper() == "WRITE":
                    self.dStateMachine[component].setPrefix(f"WR:{accessName} = ")
                else:
                    self.dStateMachine[component].setPrefix(f"{access}:{accessName} : ")

                self.dStateMachine[component].addData(f"{value}")

                # Update aAccess for tooltip
                #self.dDevByName[component].updateRegister(regname, value, row)

        else:
            self.lastNumber = lineNumber
            self.dStateMachine[component].addRawLine(line)
        return row

###
#
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###

"""
Class Definitions:
cBox: handles the box related information with start location, size and the maximum occupy width; if position is true, the box start location is fixed
cLine: handles line related information between boxes
cInOut: collect the input and output information for a component
cTopoLink: create the links for each topology block, while the SDF file descripes the edges, the layout works better if you describe the boxes
           with the interface connections between them
cTopo: Layout a topologgy, after the layout we have a list of boxes and lines to be drawn

"""
import bisect
from .link import cLink, cArtEntry
from .qtsettings import settings

class cBox:
    """Register a Box and its attributes"""
    def __init__(self, x : int, y : int, dx : int, dy : int):
        self.x   = x
        self.y   = y
        self.dx  = dx    # display width
        self.dy  = dy
        self.ldx = None  # logical based on the sum of all input width
        self.highlight = False
        self.position  = False

    def hasPosition(self) -> bool:
        """Return if the box was possitioned"""
        return self.position

    def setPosition(self, flag : bool) -> None:
        """Change the possition attribute"""
        self.position = flag

    def __str__(self):
        """return a string representation for the data"""
        return f"BOX({self.x},{self.y},{self.dx} / {self.ldx},{self.dy})" if self.position else f"BOX({self.dx} / {self.ldx})"


class cLine:
    """Register a line btween two boxes"""
    def __init__(self, sx : int, sy : int, ex : int, ey : int, si : int, ei : int, inName : str, outName : str):
        self.sx = sx
        self.sy = sy
        self.ex = ex
        self.ey = ey
        self.startTxt = "" if si is None else str(si)
        self.endTxt   = "" if ei is None else str(ei)
        self.inName   = inName
        self.outName  = outName

    def __str__(self):
        """Return a string representation of the lein"""
        return f"({self.inName}[{self.startTxt}]: {self.sx}, {self.sy}) -> ({self.outName}[{self.endTxt}]: {self.ex}, {self.ey})"

class cInOutElement:
    """ Create a single Input / Output Link"""
    def __init__(self, name : str, idx):
        self.name = name
        self.idx  = idx

    def __lt__(self, comp):
        """Return True, if the current item index is less than the compate item"""
        if self.idx is None:
            return comp.idx is not None
        else:
            return False if comp.idx is None else self.idx < comp.idx

    def __repr__(self):
        return f"cInOutElement({self.name}, {self.idx})"

    def __str__(self):
        return f"{self.name}" if self.idx is None else f"{self.name}:{self.idx}"


class cInOut:
    """Record the topology of input or output"""
    def __init__(self, name : str):
        self.name = name
        self.list = []
        self.maxi = 0

    def addItem(self, name : str, idx):
        if idx is not None and self.maxi < idx:
            self.maxi = idx 
        
        item = cInOutElement(name, idx)
        bisect.insort(self.list, item)

    def linksTo(self, name):
        for item in self.list:
            if item.name == name:
                return True
        return False

    def removeLinkTo(self, name):
        for item in self.list:
            if item.name == name:
                self.list.remove(item)
                return

    def __iter__(self):
        for item in self.list:
            yield item

    def getCount(self):
        return len(self.list)

    def __str__(self):
        """Return a string representation of the topology"""
        return f"{self.name}: {self.list}"


class cTopoLink:
    """Build a topology link"""
    def __init__(self, name, dev):
        self.name    = name
        self.input   = cInOut("In") 
        self.output  = cInOut("Out")
        self.printed = False
        self.box     = None
        self.deep    = None
        self.device  = dev
        self.lines   = None
        self.loop    = []
        self.delta   = None

    def __str__(self):
        """Create a string represtation of this data structure"""
        return f"""Name: {self.name}, In/Out: {self.getMaxSi()}/{self.getMaxMi()}, deep: {self.deep}
    {self.input}
    {self.output}
    box: {self.box}
    loop: {self.loop}"""

    def getMaxSi(self) -> int:
        """Return the maximum receiver index or 0"""
        return self.input.maxi

    def getMaxMi(self) -> int:
        """Return the maximum sender index or 0"""
        return self.output.maxi

    def getMaxInOutIdx(self) -> int:
        """Return the maximum index of receiver and sender to determine the max mox size"""
        return max(self.getMaxMi(), self.getMaxSi())

class cTopo:
    """ Layout a topology
    name: name of this topology, use to name the topology tab
    self.aEntry: array containing all cLink information provided by the SDF file for this topology type
    self.dLink: name based information per component to link for each component the input and the ouput to this components
                The entries are
                - "input": all inputs to this component
                - "output": all outputs from this component
                - "printed": internal information
                - "box": layout box information for this component
                - "deep": how many levels deep from the final list
    self.dMissing: componets which should be used by are not part of the topology
    self.dFinal:  list of all elements wiouth any output / provider target and therefore these are the final for this topology
    self.aBoxes: list of boxes by depth
    Inner workings of the algorithm:
    1. Collect all topology informations and create a list of all entries found as provider and recevier
    2. layout the topology:
    2.1: collect all the "input" and "output" components per item
    2.2: for each component check if they have no "output" assigned, if this is the case, add them as "final"
    2.3 for each "final" pferom the following actions:
    2.3.1: setDeep: Assign the "depth" of each component starting with a the final by recusifly following all "input" listed components
    2.3.2: calcBoxSize: calculate the box sizes based on the number of input and outputs
    2.3.3: sortByDepth: create lists of boxes for each assigned depth, 
           the boxes are added to the list by inoput index and therefore are in the right order to be drawn weith minimal line coressing
    2.4: centerBoxes: Start layouting each depth, center the boxes on each line
    2.4.1: calculate the total width of the layouted boxes per line
    2.4.2: start layouting the "x" offset of the boxes with the entire line beinging cernterare at 0
    2.4.3: calculate the max line width for all rows (deep)
    2.5: for all boxes adjust the "x" value by the widest marking
    2.6: createAllLines: generated all lines for this graph

    getBoxByIdx: return the next index or -1 and the boxes and text to be drawn for each component
    """
    SIZE_IN_OUT = 20
    BOX_DX_DIST = 25
    BOX_HEIGHT  = 72
    BOX_WIDTH   = 100
    DEEP_DIST   = 120
    def __init__(self, name : str, sinkLast : bool, narrowRect : bool, opt):
        self.name = name
        self.maxDepth = None
        self.aLines   = None
        self.aBoxes   = None
        self.aEntry   = []
        self.dLink    = {}
        self.dFinal   = {}
        self.dMissing = {}
        self.sinkFirst = not sinkLast
        self.narrowRect = narrowRect
        self.opt = opt

    def createEntryIfNeeded(self, link : cTopoLink, typ : str, dDN, dDBN) -> None:
        name = link.attrib.get(typ)
        self.createEntryByName(name, dDN, dDBN)

    def createEntryByName(self, name : str, dDN : dict, dDBN : dict) -> None:
        if name is not None and name not in self.dLink:
            self.dLink[name] = cTopoLink(name = name, dev = dDBN.get(name))
            dDN[name] = True

    def addDummyEntry(self, name : str, dDN : dict, dDBN : dict) -> str:
        """Create a dummy entry"""
        self.aEntry.append(cLink(None))
        self.aEntry[-1].setMS("", name)
        self.createEntryByName(name, dDN, dDBN)

    def addMissing(self, typ, name : str) -> None:
        if typ not in self.dMissing:
            self.dMissing[typ] = []
        self.dMissing[typ].append(name)

    def addEntry(self, link, dDN : dict, dDBN : dict, typ = None, dTraceDev = None) -> None:
        self.createEntryIfNeeded(link, "master", dDN, dDBN)
        self.createEntryIfNeeded(link, "slave", dDN, dDBN)
        self.aEntry.append(cLink(link.attrib))
        if typ is not None and dTraceDev is not None and typ == "CoreTrace/ATB":
            for t in ["master", "slave"]:
                if t in link.attrib and link.attrib[t] in dTraceDev:
                    del dTraceDev[link.attrib[t]]

    def addArtificialCSTF(self, dDN, dDBN):
        """ Insert Artifical Trace Funnels
        for all trace sinks/targets with multile trace trace sources, add an atrificial trace funnel called artCSTF_<n>
        """
        dInput = {}
        createIdx = 0
        for entry in self.aEntry:
            e = entry.dAttr
            s = e["slave"]
            if s not in dInput:
                dInput[s] = {}
            inputIdx = int(e["slave_interface"]) if "slave_interface" in e else 0
            if inputIdx not in dInput[s]:
                dInput[s][inputIdx] = []
            dInput[s][inputIdx].append(e)
        for s in dInput:
            for idx in dInput[s]:
                if 1 < len(dInput[s][idx]):
                    # replace and add an atrificial connection
                    # s: name to dertination
                    # idx: numerical index of destination
                    # l: touple of the source name / index
                    # self.getLinkByName
                    # print("Add CSTF", s, idx, dInput[s][idx])
                    createIdx += 1
                    name = "artCSTF_" + str(createIdx)
                    si = 0
                    for ne in dInput[s][idx]:
                        ne["slave"] = name
                        ne["slave_interface"] = si
                        si += 1
                    self.addEntry(cArtEntry(name, 0, s, idx), dDN, dDBN)

    def addArtificialReplicator(self, dDN, dDBN):
        """ Insert Artifical Replicator
        for all trace sources with multiple targets, add an artificial trace 
        """
        dOutput = {}
        createIdx = 0
        for entry in self.aEntry:
            e = entry.dAttr
            s = e["master"]
            if s not in dOutput:
                dOutput[s] = {}
            outputIdx = int(e["master_interface"]) if "master_interface" in e else 0
            if outputIdx not in dOutput[s]:
                dOutput[s][outputIdx] = []
            dOutput[s][outputIdx].append(e)
        for s in dOutput:
            for idx in dOutput[s]:
                if 1 < len(dOutput[s][idx]):
                    createIdx += 1
                    name = "artREPL_" + str(createIdx)
                    mi = 0
                    self.addEntry(cArtEntry(s, idx, name, 0), dDN, dDBN)
                    for ne in dOutput[s][idx]:
                        ne["master"] = name
                        ne["master_interface"] = mi
                        mi += 1

    def dumpList(self):
        print(self.name)
        print("============================")
        for  i in self.aEntry:
            print("   " * (1+i.level) + str(i))

        print("----------------------------")
        for i in self.dFinal:
            print("FI    " + i, self.dFinal[i])
            self.doPrintLink(i)
        print("++++++++++++++++++++++++++++")

    def isReachable(self, start, end):
        """return True if there is a link between start and end"""
        if start == end:
            return True
        for output in self.dLink[start].output:
            if self.isReachable(output.name, end):
                return True
            
        return False

    def getAttrValOrNone(self, m, name):
        return int(m.dAttr[name]) if name in m.dAttr else None

    def createLink(self, m):
        """Create a new link between two boxes based on the XML topology
        If both input and output exist, check if a connection from output to input alreay exist
        If this is the case make this as a loop and do not add it to avoid issues with the layout
        and add this connection to the Trace Check table (markAsLoop).
        """
        tr = self.getAttrValOrNone(m, "trigger")
        M = m.dAttr["master"]
        S = m.dAttr["slave"]
        if tr is None:
            si = self.getAttrValOrNone(m, "slave_interface") 
            mi = self.getAttrValOrNone(m, "master_interface")
        else:
            if M[0:5] == "CSCTI":
                M,  S  = S,  M
                mi, si = None, tr
            else:
                mi, si = None, tr

        # check if S can reach M or M can reach S prior to adding the link
        if M != "" and S != "":
            if self.isReachable(S, M):
                # Check if the new addition is from CSTFunnel to CSTMC and a direct link from CSTMC to CSTFunnel exist
                # if this is the case, assume TMC to Funnel is the correct one and remove the existing one
                # and add the current one change CSTFunnel -> CSTMC to CSTMC -> CSTFunnel which vacors the layout
                # for one particular SDF file
                if S[:10] == "CSTFunnel_" and M[:6] == "CSTMC_" and self.dLink[S].output.linksTo(M) and self.dLink[M].input.linksTo(S):
                    # remove the current link and add the new one
                    self.dLink[S].output.removeLinkTo(M)
                    self.dLink[M].input.removeLinkTo(S)
                    self.dLink[M].output.addItem(S, mi)
                    self.dLink[S].input.addItem(M, si)
                    self.markAsLoop(M, S)
                else:
                    self.markAsLoop(S, M)
            else:
                self.dLink[M].output.addItem(S, mi)
                self.dLink[S].input.addItem(M, si)


    def assignLevel(self):
        again = True
        while again:
            again = False
            for s in self.aEntry:
                setIdx = None
                if s.level is None:
                    for m in self.aEntry:
                        if s.dAttr["master"] == m.dAttr["slave"]:
                            setIdx = m.level
                    if setIdx is None: # nobody is connected to me, so I'm top of the list
                        s.level = 0
                        again = True
                    elif 0 <= setIdx:
                        s.level = setIdx + 1
                        again = True

    def moveBoxesUpward(self):
        # Move the final boxes upward as needed
        for nm in self.dFinal:
            minDepth = None
            for inp in self.dLink[nm].input:
                if inp.name in self.dLink:
                    link = self.dLink.get(inp.name)
                    minDepth = link.deep if (minDepth is None) or (minDepth > link.deep) else minDepth
            if minDepth is not None and 2 < minDepth:
                self.dLink[nm].deep = minDepth - 1

    def assignFinalBoxesAndDepthLevel(self):
        # identify all final entries, these have no desintation / ouput
        self.dFinal = { nm : self.dLink[nm] for nm in self.dLink if self.dLink[nm].output.getCount() == 0}
        
        # for all final entries
        for nm in self.dFinal:
            d = self.setDeep(nm)
            self.maxDepth = max(self.maxDepth, d)
        return len(self.dFinal)

    def layoutTopology(self):
        self.aBoxes = []
        self.maxDepth = 0

        # for all entries, create a link
        for m in self.aEntry:
            self.createLink(m)

        finalCount = self.assignFinalBoxesAndDepthLevel();

        # improvide the box layout to reduce the back link 
        self.moveBoxesUpward()

        # if there are too many final boxes move them to maxDepth + 1
        if -1 == self.name.find("CoreTrace/ATB") and 15 < finalCount:
            cnt = 0
            maxCnt = 0
            for nm in self.dFinal:
                maxCnt = cnt // 16
                delta = maxCnt * self.maxDepth
                self.addDeep(nm, delta)
                cnt += 1
            self.maxDepth = self.maxDepth * (maxCnt + 1)

        # calculate the box sized and sort the box sizes by final box
        for nm in self.dFinal:
            self.calcBoxSize(nm)
            self.sortByDepth(nm)

        # set the box location based on the depth and
        self.adjustBoxLocation()

        # create lines between the boxes
        self.aLines = self.createAllLines(nm)

        self.updateHighlightLines(self.opt["highlight"]["path"])

    def getMatchingBoxes(self, name):
        aRet = [b for b in self.dLink if 0 <= b.upper().find(name)]
        return aRet

    def createAllPaths(self, pairs):
        aHighlightLines = []
        for highlight in pairs:
            aStart = self.getMatchingBoxes(highlight[0])
            aEnd   = self.getMatchingBoxes(highlight[1])
            for start in aStart:
                for end in aEnd:
                    hlp = self.highlightPath(start, end, True, [])
                    if hlp is not None:
                        aHighlightLines += hlp
        return aHighlightLines

    def highlightPath(self, start, end, isFirst = True, inList = []):
        if self.dLink[start].output.getCount() == 0:
            return None
        else:
            aRet = None
            inList.append(start)
            for out in self.dLink[start].output:
                if end == out.name:
                    return [(start, out.name)]
                if out.name not in inList:
                    aRet = self.highlightPath(out.name, end, False, inList)
                    if aRet is not None:
                        aRet.append((start, out.name))
                        break
            return aRet

    def placeChildBoxes(self, nm):
        startLink = self.dLink.get(nm)
        startX = startLink.box.x #  - self.dLink[nm].box.ldx // 2
        for i in startLink.input:
            link = self.dLink.get(i.name)
            if link and not link.box.hasPosition():
                link.box.setPosition(True)
                link.box.x        = startX
                startX += link.box.ldx + self.BOX_DX_DIST
                self.placeChildBoxes(i.name)

    def adjustBoxLocation(self):
        # center all boxes by calculating the box sizes with gap
        # calcualte the maximum size for all boxes includeing gaps
        maxDx = 0
        for aBox in self.aBoxes:
            dx = self.centerBoxes(aBox)
            if maxDx < dx:
                maxDx = dx

        if settings.getBool("Global/OldLayout"):
            # shift all boxes by 1/2 of the max line-x size
            adjustX = maxDx // 2 + self.BOX_DX_DIST
            for aBox in self.aBoxes:
                for b in aBox:
                    self.dLink[b].box.x += adjustX
        else:
            adjustX = maxDx // 2 + self.BOX_DX_DIST
            # shift all final boxes by 1/2 of the max line-x size
            for nm in self.dFinal:
                self.dLink[nm].box.x += adjustX

            for nm in self.dFinal:
                self.placeChildBoxes(nm)

        # sort the boxes based on the start x coordinate
        for aBox in self.aBoxes:
            again = True
            while again:
                again = False
                for idx in range(len(aBox) - 1):
                    thisBox  = self.dLink.get(aBox[idx]).box
                    rightBox = self.dLink.get(aBox[idx+1]).box
                    if thisBox.x > rightBox.x:
                        again = True
                        aBox[idx], aBox[idx+1] = aBox[idx+1], aBox[idx]

        # shrinking the boxes back to "normal"
        if self.narrowRect:
            for aBox in self.aBoxes:
                for b in aBox:
                    link   = self.dLink[b]
                    cnt = link.getMaxInOutIdx()
                    newBoxSize   = cnt * self.SIZE_IN_OUT + self.BOX_WIDTH
                    boxSizeDelta = link.box.dx - newBoxSize
                    link.box.dx  -= boxSizeDelta
                    link.box.x   += boxSizeDelta // 2

        # check if boxes overlap and it we can shift them to a better position
        maxXforAllDeep = 0
        minXforAllDeep = 100000000
        forceRearrange = False
        for aBox in self.aBoxes:
            for idx in range(len(aBox) - 1):
                thisBox  = self.dLink.get(aBox[idx]).box
                rightBox = self.dLink.get(aBox[idx+1]).box
                needSpace = thisBox.x + thisBox.dx + self.BOX_DX_DIST - rightBox.x
                if needSpace > 0: # we are overlapping
                    # check if we can move the left box to the left
                    if idx == 0: # left most box so we can shift it left
                        thisBox.x = rightBox.x - thisBox.dx - self.BOX_DX_DIST
                    else: # we have a box to the left, which it to the left - if possible
                        leftBox   = self.dLink.get(aBox[idx-1]).box
                        maxSpace  = thisBox.x - leftBox.x - leftBox.dx
                        if needSpace <= maxSpace: # we can shift it securly to the left
                            thisBox.x = rightBox.x - thisBox.dx - self.BOX_DX_DIST
                        else: # shift is as far left as possible
                            thisBox.x = leftBox.x + leftBox.dx + self.BOX_DX_DIST

            if 1 < len(aBox):
                stillOverlap = False
                needDX = - self.BOX_DX_DIST
                thisBox  = self.dLink.get(aBox[0]).box
                maxX = thisBox.x + thisBox.dx
                minX = thisBox.x
                for idx in range(len(aBox) - 1):
                    thisBox  = self.dLink.get(aBox[idx]).box
                    rightBox = self.dLink.get(aBox[idx+1]).box
                    needDX += self.BOX_DX_DIST + (thisBox.dx if idx == 0 else 0) + rightBox.dx
                    stillOverlap |= (thisBox.x + thisBox.dx) > rightBox.x
                    minX = min(minX, rightBox.x)
                    maxX = max(maxX, rightBox.x + rightBox.dx)

                maxXforAllDeep = max(maxX, maxXforAllDeep)
                minXforAllDeep = min(minX, minXforAllDeep)

                if stillOverlap: # there are overlaps, lets fix this
                    forceRearrange = True
                    # calculate additional spacing between boxes
                    addlTotal = maxXforAllDeep - minXforAllDeep - needDX
                    addlSpace = addlTotal // len(aBox)

                    startX = minX + ((maxX - minX) - needDX - addlTotal) // 2

                    for boxName in aBox:
                        thisBox   = self.dLink.get(boxName).box
                        thisBox.x = startX
                        startX   += thisBox.dx + self.BOX_DX_DIST + addlSpace




    def getUniqIn(self, nm):
        if nm in self.dLink:
            if self.dLink[nm].input.getCount() == 1:
                ret = self.dLink[nm].input[0].name
                return ret if 0 < len(ret) else None
        return None

    def getEndXYandTXT(self, nm, to_nm, dSkip):
        mi = None
        x  = (self.dLink[nm].box.dx - self.SIZE_IN_OUT * self.dLink[nm].getMaxMi()) // 2
        skipCnt = 0
        for output in self.dLink[nm].output:
            if output.name == to_nm:
                if dSkip[nm] == skipCnt:
                    mi = output.idx
                    x  += 0 if mi is None else self.SIZE_IN_OUT * mi
                skipCnt += 1
        x += self.dLink[nm].box.x
        y = self.dLink[nm].box.y + (0 if self.sinkFirst else self.dLink[nm].box.dy)
        return x, y, mi

    def createAllLines(self, nm):
        """General all lines between the boxes itterative and the return the array""" 
        aRet = []
        for nm, src in self.dLink.items():
            src.lines = True
            sx = src.box.x + (src.box.dx - self.SIZE_IN_OUT * src.getMaxSi()) // 2
            sy = src.box.y + (src.box.dy if self.sinkFirst else 0)


            dSkip = {}
            for input in src.input:
                N = input.name
                if N != "":
                    dSkip[N] = dSkip.get(N, 0) + 1
                    dst = self.dLink[N]
                    ex  = (dst.box.dx - self.SIZE_IN_OUT * dst.getMaxMi()) // 2
                    ex += dst.box.x


                    ex, ey, mi = self.getEndXYandTXT(N, nm, dSkip)

                    dx = 0 if input.idx is None else self.SIZE_IN_OUT * input.idx
                    aRet.append(cLine(sx + dx, sy, ex, ey, input.idx, mi, nm, N))
        return aRet

    def updateHighlightLines(self, pairs):
        aHighlightLines = self.createAllPaths(pairs)
        for line in self.aLines:
            doHighlight = False
            for hll in aHighlightLines:
                doHighlight |= (hll[0] == line.outName) and (hll[1] == line.inName)
            line.highlight = doHighlight

    def countBoxes(self):
        ret = 0
        for aBox in self.aBoxes:
            ret += len(aBox)
        return ret

    def lengthCompare(self, loopName, nm):
        """Compare String by Length and then alpha-numerical"""
        if len(loopName) < len(nm):
            return True
        if len(loopName) == len(nm):
            return loopName < nm
        return False

    def getTopologyCheck(self):
        """Create a table for all trace topology loops and missing links for ETM, TMC, CSTF, CSTREP"""
        aRet = []
        aLoopName = []
        """Collect all loop names"""
        for nm in self.dLink:
            link = self.dLink.get(nm)
            if 0 < len(self.dLink[nm].loop):
                idx = 0
                while idx < len(aLoopName) and self.lengthCompare(aLoopName[idx], nm):
                    idx += 1
                if idx < len(aLoopName):
                    aLoopName.insert(idx, nm)
                else:
                    aLoopName.append(nm)
        """Build the Treeview"""
        for nm in aLoopName:
            link = self.dLink.get(nm)
            devType = link.device.devType if link and link.device is not None else None
            loopList = (nm, *self.dLink[nm].loop)

            if devType is None:
                aRet.append(["Topology loop", "->".join(loopList)])
            else:
                aRet.append(["Topology loop", devType, "->".join(loopList)])


        for nm in self.dLink:
            link = self.dLink.get(nm)
            devType = link.device.devType if link and link.device is not None else None

            if devType is None:
                pass
            elif devType in ["CSETM", "CSTFunnel", "CSATBReplicator", "ETE"]:
                if link.input.getCount() == 0:
                    aRet.append(["Trace Check", (devType, "Input required for trace component"), nm])
                if link.output.getCount() == 0:
                    aRet.append(["Trace Check", (devType, "Output required for trace component"), nm])
            elif devType in ["CSELA", "CSSTM", "CSITM", "CSELA500", "CSELA600"]:
                if link.output.getCount() == 0:
                    aRet.append(["Trace Check", (devType, "Output connection missing for trace source"), nm])
            elif devType == "CSTMC" or devType == "CSTPIU":
                if link.input.getCount() == 0:
                    aRet.append(["Trace Check", (devType, "Input required for trace sink"), nm])
            elif devType.split()[0].split("-")[0] in ["Cortex", "Neoverse", "A"]:
                pass # this is a core, we are fine
            else:
                aRet.append(["Trace Check", "Unknown Type", devType, nm])

        for typ in self.dMissing:
            for name in self.dMissing[typ]:
                aRet.append(["Trace Check", (typ, "Trace component without input or output"), name])

        return aRet

    def getAllBoxes(self):
        """get all boxes to be drawn, return one by one"""
        isTrace = self.name == "CoreTrace/ATB"

        for aBox in self.aBoxes:
            for boxText in aBox:
                currBox = self.dLink.get(boxText, None)
                if currBox is not None and boxText != "":
                    if isTrace:
                        outCnt = currBox.output.getCount()
                        inCnt  = currBox.input.getCount()
                        border = "tracesink" if outCnt == 0 else ("tracesource" if inCnt == 0 else None)
                    else:
                        border = None
                    highlight = False
                    boxTxt = boxText.upper()
                    for n in self.opt["highlight"]["box"]:
                        highlight |= 0 <= boxTxt.find(n)
                    yield ( ( currBox.box, boxText, currBox.device, border, highlight ) )

    def centerBoxes(self, aBox):
        dx = -self.BOX_DX_DIST
        for b in aBox:
            dx += self.dLink[b].box.ldx + self.BOX_DX_DIST

        x = - dx // 2
        for b in aBox:
            center = (self.dLink[b].box.ldx - self.dLink[b].box.dx) //2
            self.dLink[b].box.x = x + center
            x += self.dLink[b].box.ldx + self.BOX_DX_DIST
        return dx

    def getLinkByName(self, nm):
        for i in self.aEntry:
            if i.dAttr["master"] == nm:
                return i
        return None

    def addDeep(self, nm, delta):
        if nm != "" and self.dLink[nm].delta is None:
            self.dLink[nm].delta = delta
            self.dLink[nm].deep += delta
            for i in self.dLink[nm].input:
                self.addDeep(i.name, delta)

    def setDeep(self, nm, deep = 0, inPath = []):
        ret = deep
        if nm != "":
            deep += 1
            ret = deep
            if self.dLink[nm].deep is None or deep > self.dLink[nm].deep:
                self.dLink[nm].deep = deep
                for i in self.dLink[nm].input:
                    if i.name in inPath and None != self.dLink[i.name].deep:
                        self.dLink[nm].recursion = True
                    else:
                        inPath.append(nm)
                        r = self.setDeep(i.name, deep, inPath)
                        ret = ret if ret > r else r
        return ret

    def sortByDepth(self, nm):
        if nm != "":
            deep = self.dLink[nm].deep
            while len(self.aBoxes) <= deep:
                self.aBoxes.append([])

            if nm != "" and nm not in self.aBoxes[deep]:
                self.aBoxes[deep].append(nm)
                for idx in range(self.dLink[nm].getMaxSi()+1):
                    for i in self.dLink[nm].input:
                        if idx == i.idx or (idx == 0 and i.idx is None):
                            self.sortByDepth(i.name)

    def getY(self, nm):
        if self.sinkFirst:
            return self.dLink[nm].deep * self.DEEP_DIST - self.BOX_HEIGHT
        else:
            return (self.maxDepth + 1 - self.dLink[nm].deep) * self.DEEP_DIST - self.BOX_HEIGHT

    def markAsLoop(self, src, dst):
        if dst not in self.dLink[src].loop:
            self.dLink[src].loop.append(dst)

    def uniqDestinationCount(self, inOut):
        aRet = []
        for io in inOut:
            if io.name not in aRet:
                aRet.append(io.name)
        return len(aRet)

    def calcBoxSize(self, nm):
        """Recursive function to calculate the required box size based on the input box sizes and its own size"""

        link = self.dLink.get(nm)
        if link is not None and None == link.box:
            # print(self.dLink[nm])
            cnt = link.getMaxInOutIdx()

            link.box = cBox(-1, self.getY(nm), self.SIZE_IN_OUT * cnt + self.BOX_WIDTH, self.BOX_HEIGHT)

            for i in link.input:
                self.calcBoxSize(i.name)

            if link.input.getCount() != 0:
                # calculate the maximum input width
                dx_size = -self.BOX_DX_DIST
                alreadyCounted = []

                for i in link.input:
                    if i.name not in alreadyCounted:
                        alreadyCounted.append(i.name)
                        inLink = self.dLink.get(i.name)
                        adjust = self.uniqDestinationCount(inLink.output)

                        if link.deep < inLink.deep:
                            dx_size += inLink.box.ldx // adjust + self.BOX_DX_DIST

                        if inLink.box.ldx is None:
                            self.markAsLoop(nm, i.name)

                dx_size = max(dx_size, self.SIZE_IN_OUT * link.getMaxInOutIdx() + self.BOX_WIDTH)

                link.box.dx  = dx_size
            link.box.ldx = link.box.dx

    def doPrintLink(self, nm):
        li = self.getLinkByName(nm)
        if li is None:
            print("   " * (self.dLink[nm].deep) + nm)
        else:
            print("   " * (self.dLink[nm].deep) + str(li))
        if not self.dLink[nm].printed:
            self.dLink[nm].printed = True
            for i in self.dLink[nm].input:
                self.doPrintLink(i.name)

###
#
# SPDX-File: cscomponent.py
#
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###
"""
Define Coresight register classes
cTrackRegister: Track register changes for the DAP log
cCSGeneric:     Generic Coresight registers (especially offset 0xFxx)
cArmv8Debug:    The Armv8 debug core registers
"""

from .qtdaplog import cDapLogTableWidget

class cTrackRegister:
    """
    class cTrackRegister: Track changes of a single register in which Dap Log Table row a register is changed and its new value
    Contains an array to tack the changes containing the dap log row, access type and new value
    """
    def __init__(self, name):
        """Initalize a new array"""
        self.name    = name
        self.aAccess = []

    def updateRegister(self, row, acc, val) -> None:
        """Update a single register change"""
        #print(f"updateRegister: {row}, {acc}, {val}") if self.name[:4] == "CIDR" else None
        self.aAccess.append((row, acc, val))

    def getAllValues(self) ->list:
        """Return an array with all of the values for the entire file"""
        return [l[2] for l in self.aAccess]

    def getAllRows(self) -> list:
        """Return an array with all rows changin the file"""
        return [l[0] for l in self.aAccess]

    def getFirstValue(self):
        """Get the first register value"""
        return self.aAccess[0][2] if 0 < len(self.aAccess) else None

    def getLastValue(self):
        """Get the last register value or None"""
        #print(f"{self.aAccess[-1]}")
        return self.aAccess[-1][2] if 0 < len(self.aAccess) else None

class cCSGeneric:
    """Handle the generic (unknown) CS type and the shared registers"""
    aPIDR = ["PIDR0", "PIDR1", "PIDR2", "PIDR3", "PIDR4", "PIDR5", "PIDR6", "PIDR7"]
    aCIDR = ["CIDR0", "CIDR1", "CIDR2", "CIDR3"]
    aEDPIDR = ["EDPIDR0", "EDPIDR1", "EDPIDR2", "EDPIDR3", "EDPIDR4", "EDPIDR5", "EDPIDR6", "EDPIDR7"]
    aEDCIDR = ["EDCIDR0", "EDCIDR1", "EDCIDR2", "EDCIDR3"]
    def __init__(self, device):
        """Setup a generic CS device, this will be subclassed for all sepcific clases"""
        self.device = device
        self.dRegValue   = {}
        self.dapLogTab   = None
        self.usePID      = None
        self.useCID      = None

    def getRegValue8str(self, regName, default = "XX"):
        """Return a 8bit value string or default if the value is not set"""
        return f"{self.dRegValue[regName].getLastValue():02X}" if regName in self.dRegValue else default

    def getRegValue32str(self, regName, default = "XXXXXXXX"):
        """Return a 32bit value string or default if the value is not set"""
        return f"{self.dRegValue[regName].getLastValue():08X}" if regName in self.dRegValue else default

    def getLastValue(self, regName : str):
        """return the last value change for the provided register name or None"""
        return self.dRegValue[regName].getLastValue() if regName in self.dRegValue else None

    def getRegisterToolTip(self, reg : str):
        """Return a tool tip string or None"""
        if self.usePID is not None and reg in self.usePID:
            aS = [self.getRegValue8str(rn) for rn in self.usePID]
            return " ".join(aS)

        if self.useCID is not None and reg in self.useCID:
            aS = [self.getRegValue8str(rn) for rn in self.useCID]
            return " ".join(aS)

        return None

    def updateRegister(self, row : int, regName : str, aReg : list, val : int, dapLogTab : cDapLogTableWidget) -> bool:
        """track register value changes and return True, in case the register values is bad"""
        self.dapLogTab = dapLogTab
        self.dRegValue.setdefault(regName, cTrackRegister(regName))

        #print(f"{self.device.name}:{regName} {aReg} {val}")
        self.dRegValue[regName].updateRegister(row, aReg[0], val)

        if self.useCID is None:
            if regName.startswith("CIDR"):
                self.useCID = self.aCIDR
            elif regName.startswith("EDCIDR"):
                self.useCID = self.aEDCIDR
                
        if self.usePID is None:
            if regName.startswith("PIDR"):
                self.usePID = self.aPIDR
            elif regName.startswith("EDPIDR"):
                self.usePID = self.aEDPIDR

        ret  = (self.usePID is not None) and (regName in self.usePID) and ((val & 0xffffff00) != 0)
        ret |= (self.useCID is not None) and (regName in self.useCID) and ((val & 0xffffff00) != 0)

        if ret:
            self.dapLogTab.markColumnErrors(self.device.name, [row])

        toolTip = self.getRegisterToolTip(regName)

        if toolTip is not None:
            self.dapLogTab.setToolTip(self.device.name, row, toolTip)

        return False

    def getCombineRegisters(self, regList : list | None, shiftWidth : int) -> int:
        """combine multiple registers to a single value"""
        if regList is None:
            return None, []

        values = [(self.dRegValue[name].getLastValue() << (i * shiftWidth)) for i, name in enumerate(regList) if name in self.dRegValue]
        return sum(values) if 0 < len(values) else None, [self.dRegValue[name].getAllRows() for name in regList if name in self.dRegValue]

    def getPID(self):
        """Combine the last PID register to a current value or XX"""
        return self.getCombineRegisters(self.usePID, 8)

    def getCID(self):
        """Combine the last CID register to a current value or XX"""
        return self.getCombineRegisters(self.useCID, 8)

    def performComponentTest(self, name):
        """Default component register test"""
        if self.dapLogTab is None:
            return
        val, aRow = self.getPID()
        if val is not None and val == 0:
            self.dapLogTab.markColumnErrors(name, aRow)

        val, aRow = self.getCID()
        if val is not None and val == 0:
            self.dapLogTab.markColumnErrors(name, aRow)

class cArmv8Debug(cCSGeneric):
    """Generic Armv8/9 register definition"""
    #def __init__(self, device):
    #    """initialize the device"""
    #    super().__init__(device)

    def getRegisterToolTip(self, reg):
        """Return the Armv8/9 specifc register tool tip"""
        if reg in ["[0x314]", "EDPRSR"]:
            return ["EDPRSR", self.getLastValue(reg), 0,
                    ("EPMADE", 16,  1),
                    ("ETADE",  15,  1),
                    ("EDADE",  14,  1),
                    ("STAD",   13,  1),
                    ("ETAD",   12,  1),
                    ("SDR",    11,  1),
                    ("SPMAD",  10,  1),
                    ("EPMAD",   9,  1),
                    ("SDPD",    8,  1),
                    ("EDPD",    7,  1),
                    ("DLK",     6,  1),
                    ("OSLK",    5,  1),
                    ("HALTED",  4,  1),
                    ("SR",      3,  1),
                    ("R",       2,  1),
                    ("SPD",     1,  1),
                    ("PU",      0,  1),
                   ]
        if reg == "DEVTYPE":
            return [reg, self.getLastValue(reg), 1,
                    ("MINOR", 4,  4, "CORE"),
                    ("MAJOR", 0,  4, "DEBUG"),
                    ]

        return super().getRegisterToolTip(reg)

class cCSCTI(cCSGeneric):
    """Handle the Cross Trigger Interface sepcifc registers"""

###
#
# SDF Visualizer, uses PySide6
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
Type definitions:
 cDisplaySDFContent: handles a single graphical TAB and its content
    base class: QObject
 cMenuEntry: helper function to create the main menu
 cMainWindow: creates the main window for the entire tool
   base class: QMainWindow
 cTreeWidget: handles a single Tree Tab Widget
   base class: QTreeWidget
 cDisplaySDF: Container for the SDF created TABs, contains cDisplaySDFContent and cTreeWidget
   base class: QApplication

 Class Hierarchy
  cMainWindow(1)
    -> cDisplaySDF(*)
        -> cDisplaySDFContent(*)
        -> cTreeWidget(*)

Helper Function
 DisplayMsg: display system dialog box

"""
# Imports
import os
import sys
import subprocess
import traceback

from pathlib import Path
from .__init__ import __version__, __revision__, __description__, __copyright__
from PySide6.QtWidgets import QApplication, QMainWindow, QTabWidget
from PySide6.QtWidgets import QStyleFactory
from PySide6.QtWidgets import QTreeWidget, QTreeWidgetItem, QComboBox
from PySide6.QtGui     import QFont, QColor, QBrush, QPixmap, QPainter, QCursor
from PySide6.QtGui     import QAction
from PySide6.QtWidgets import QWidgetAction
from PySide6.QtCore    import QSize, QRect, Qt, Signal, Slot, QObject
from PySide6.QtSvg     import QSvgGenerator
from PySide6.QtWidgets import QMenu
from PySide6.QtWidgets import QFileDialog, QMessageBox
from PySide6.QtGui     import QFontDatabase
from .qtcolorscheme    import colorModeList, cEditColorSchema, getColorModeList
from .qtsdfmain        import cSDFTabUI
from .qtsettings       import settings
from .qtsdfmain        import cSelectSrcDst
from .qtsdfmain        import cAddDapLogEater
from .qtsdfmain        import cGraphicsView
from .qtsdfmain        import cGraphicsScene
from .qtprimitives     import cGraphicsTextItem, cGraphicsPathItem, cGraphicsPolygonItem
from .qtdaplog         import cDapLogWidget
from PySide6.QtWidgets import QProgressBar

clipboard = None

import traceback
try:
    from dap_log_eater.eater import DapLogEater
    dapLogEaterClass = DapLogEater
except Exception as err: 
    dapLogEaterClass = None
dapLogEater = None

class cDLEReceiveFile:
    def __init__(self, store):
        self.store = store
        self.aCollect = []

    def close(self):
        self.aCollect = []

    def write(self, item : str) -> None:
        if self.store:
            self.aCollect.append(item)

    def readlines(self) -> list:
        return self.aCollect

    def getLineCount(self) -> int:
        return len(self.aCollect)
    
    def __repr__(self):
        return f"cDLEReceiveFile({self.store})\n" + "\n".join(self.aCollect)


class cContextObject:
    def __init__(self, addr, item , sx, sy, dx, dy, name, apIdx):
        self.addr = addr
        self.item = item
        self.sx   = sx
        self.sy   = sy
        self.dx   = dx
        self.dy   = dy
        self.name = name
        self.apIdx = apIdx

class cContext:
    def __init__(self):
        self.dAddr = {}
        self.dName = {}
        self.aObj  = []
        self.apIdxMax = 0;

    def addItem(self, addr, item, sx, sy, dx, dy, name, apIdx):
        idx = len(self.aObj)
        self.aObj.append(cContextObject(addr, item, sx, sy, dx, dy, name, apIdx))

        self.apIdxMax = self.apIdxMax if apIdx is None else max(self.apIdxMax, apIdx)

        if addr in self.dAddr:
            # must be on different AP or JTAG
            self.dAddr[addr].append(idx)
        else:
            self.dAddr[addr] = [idx]

        aName = name.split("_")
        if aName[-1].isnumeric():
            keyName = "_".join(aName[:-1]) + "_" + ("00000" + aName[-1])[-5:]
        else:
            keyName = name
            
        if keyName in self.dName:
            # This is illegal
            self.dName[keyName].append(idx)
        else:
            self.dName[keyName] = [idx]

    def printAll(self):
        for prefix, key, val in self.byAddr():
            print(prefix, key, val);
        for prefix, key, val in self.byName():
            print(prefix, key, val);
    
    def __str__(self):
        return "\n".join([f"{prefix} {key} {val}"] for prefix, key, val in self.byName())
    
    def byAddr(self):
        keyList = list(self.dAddr.keys())
        keyList.sort()
        dAddrCnt20 = {}
        dAddrCnt24 = {}
        # determine the address prefix list
        for key in keyList:
            idx = int(key, 16) // 0x100000
            dAddrCnt20[idx] = dAddrCnt20.get(idx, 0) + len(self.dAddr[key])
            idx = idx // 16
            dAddrCnt24[idx] = dAddrCnt24.get(idx, 0) + len(self.dAddr[key])

        for key in keyList:
            idx = int(key, 16) // 0x100000
            if dAddrCnt24[idx // 16] <= 32:
                prefixStr = f"0x{idx // 16:02X}xx_xxxx"
            elif dAddrCnt20[idx] <= 32:
                prefixStr = f"0x{idx:03X}x_xxxx"
            else:
                idx = int(key, 16) // 0x10000
                prefixStr = f"0x{idx:04X}_xxxx"

            for value in self.dAddr[key]:
                yield prefixStr, key, self.aObj[value]

    def byName(self):
        keyList = list(self.dName.keys())
        keyList.sort()
        for key in keyList:
            aKey = key.split("_")
            for value in self.dName[key]:
                yield aKey[0], key, self.aObj[value]

class cDisplaySDFContent(QObject):
    """Create the Graphics view for the graphical representation"""
    updateSignal = Signal( (int,) )

    def __init__(self, name : str, dispSDF, apBus, sdf, topo):
        global dapLogEaterClass
        global dapLogEater
        super().__init__(None)

        if dapLogEaterClass is not None and dapLogEater is None:
            # print(f"{type(dispSDF) = }, {type(apBus) = } {type(sdf) = }, {type(topo) = }")
            try:
                dapLogEater = dapLogEaterClass(False, dispSDF.progressCallBack)
            except Exception as err: 
                # Add status message if Dap Log Eater cannont be instantiated and disable it
                dispSDF.statusBar().showMessage("Incompatible Dap Log Eater version installed", 5000)
                dapLogEaterClass = None
                dapLogEater      = None
        
        self.updateHighlighList = None
        self.minX    = 10000000
        self.minY    = 10000000
        self.maxX    = 0
        self.maxY    = 0
        self.startHierBox = None
        self.image   = cGraphicsScene(self)
        self.view    = cGraphicsView(self.image, name, dispSDF.getCurrentSchema())
        self.dispSDF = dispSDF
        self.topo    = topo
        self.sdf     = sdf
        tabs    = dispSDF.getLastTabs()
        tabs.parent().appendTab(name, self, self.view)
        self.updateSignal[int].connect(self.performMultiSelectHighlight)

        # Passed arguments
        self.name = name

        self.dColorByAP = { "UNKNOWN_UNKNOWN" : 0}
        self.dBoxes  = {}
        self.context = cContext()
        self.aLines  = []
        self.aTexts  = []
        self.dLineHighlight = {}
        self.dBoxesHighlight  = {}
        self.font = self.image.font()
        self.font.setPointSize(8);

        color = QColor(0x000000)
        color.setAlphaF(0.0)
        self.brushBackground = QBrush(color)

        self.image.setFont(self.font)
        self.colorIdx    = 1
        self.lastHierBox = 0
        self.changeColorSchema(dispSDF.getCurrentSchema())

        self.topDownFlag = settings.getBool("Global/SinkBottom")
        self.useCurve    = None
        # check if ERROR
        if name == "ERROR":
            dispSDF.setLastTabColor(self.schema.getPen("ERRORtab").color())
        elif name[:5] == "Info ":
            dispSDF.setLastTabColor(self.schema.getPen("INFOtab").color())
 
    def updateCurveFlag(self, flag : bool):
        self.useCurve = self.useCurve if flag is None else flag
        
    def updateTopDownFlag(self, flag : bool):
        self.topDownFlag = self.topDownFlag if flag is None else flag


    def contextLocateBox(self, event):
        """locate a box in the image"""
        menu = QMenu("Select Components")

        byAddr = QMenu("By Address")
        menu.addMenu(byAddr)
        menu.addSeparator()
        menu.setToolTipsVisible(True)
        if self.topo is not None:
            highlightAction = menu.addAction("Highlight Path")
            highlightAction.setToolTip("Highlight a path between components")
        else:
            highlightAction = None

        if not dapLogEater is None:
            if hasattr(dapLogEater, "version"):
                addDapLog = menu.addAction(f"Add DapLog ({dapLogEater.version})")
            else:
                addDapLog = menu.addAction("Add DapLog")
            addDapLog.setToolTip("EXPERIMENTAL Dap Log Eater integration")
        else:
            addDapLog = None

        menu.addSeparator()

        lastBase = ""
        for newBase, name, obj in self.context.byName():
            if newBase != lastBase:
                lastBase = newBase
                subMenu = QMenu(newBase)
                menu.addMenu(subMenu)
            act = subMenu.addAction(f"{obj.name} at AP[{obj.apIdx}]:{obj.addr}")
            act.setData(obj)

        subMenu  = None
        lastBase = ""
        fixedFont = QFontDatabase.systemFont(QFontDatabase.FixedFont)

        for newBase, addr, obj in self.context.byAddr():
            if newBase != lastBase:
                lastBase = newBase
                subMenu = QMenu(newBase)
                act = byAddr.addMenu(subMenu)
                act.setFont(fixedFont)
            if 100 <= self.context.apIdxMax:
                act = subMenu.addAction(f"AP[{obj.apIdx:>3d}]:{addr} {obj.name}")
            elif 10 <= self.context.apIdxMax:
                act = subMenu.addAction(f"AP[{obj.apIdx:>2d}]:{addr} {obj.name}")
            else:
                act = subMenu.addAction(f"AP[{obj.apIdx:d}]:{addr} {obj.name}")
            
            act.setFont(fixedFont)
            act.setData(obj)

        action = menu.exec_(QCursor.pos())

        if action is None: # abort menue with no action selected
            pass
        elif action == highlightAction:
            if not (self is None or self.dBoxes is None):
                dialog = cSelectSrcDst(self.view, self.dBoxes)
                pair = dialog.run()
                if pair is not None:
                    self.performUpdateHighlighLines([ [pair[0].upper(), pair[1].upper()] ] )
        elif action == addDapLog:
            baseName = os.path.basename(self.sdf.fileName)
            dialog = cAddDapLogEater(self.view, settings.getStr(f"{baseName}/DapLogFile"))
            value = dialog.run()
            if not value is None:
                # invoke the dap log eater
                outputText = cDLEReceiveFile(False)
                summaryText = cDLEReceiveFile(True)
                settings.updateStr(f"{baseName}/DapLogFile", value[0], None)
                sdfInFile = open(str(Path(self.sdf.fileName).absolute()), "rt" )
                try:
                    with open(value[0], "rt") as logInFile:
                        
                        self.dispSDF.setProgressPrefix("1 of 2: Process Daplog")
                        dapLogEater.parse(sdfInFile, logInFile, outputText, summaryText, None)
                        # add the new tab with the DAP log
                        self.dispSDF.setProgressPrefix("2 of 2: Generate table")
                        self.sdf.addDapLogSummary(summaryText, self.dispSDF.progressLineCallBack)
                except IOError as err:
                    pass
                except Exception as err:
                    traceback.print_exc()
                    pass
                self.dispSDF.progressLineCallBack(None, None)
        elif action is not None:
            obj = action.data()
            self.view.startBlinkBox(obj.item)

    def performUpdateHighlighLines(self, pairs):
        for line in self.dLineHighlight:
            self.dLineHighlight[line].setVisible(False)
        self.topo.updateHighlightLines(pairs)
        self.sdf.updateHighlightLines(self.topo.name, self.topo)

    def updateImage(self):
        self.image.setSceneRect(self.minX, self.minY, self.maxX+20, self.maxY+20);
        self.view.setSceneRect(self.minX, self.minY, self.maxX+20, self.maxY+20);
        self.view.show()

    # Make block for hierarchy

    def enableLineHighlight(self, sx, sy, ex, ey):
        line = self.dLineHighlight.get((sx, sy, ex, ey))
        if line:
            line.setVisible(True)

    def addLineHighlight(self, sx, sy, ex, ey):
        line = cGraphicsPathItem(sx, sy, ex, ey, self.useCurve, "highlight", None)
        self.image.addItem(line)
        self.dLineHighlight[(sx, sy, ex, ey)] = line
        self.aLines.append((line, "highlight"))

    def addLine(self, sx, sy, ex, ey, color = None, toolTip = None):
        color = color if color else "foreground"
        line = cGraphicsPathItem(sx, sy, ex, ey, self.useCurve, color, toolTip)
        self.image.addItem(line)
        self.aLines.append((line, color))

    def addLineText(self, sx, sy, ex, ey, st, et):
        factor = 1 if self.topDownFlag else -1
        self.addText(sx, sy, st, -1, +factor)
        self.addText(ex, ey, et, -1, -factor)

    def addText(self, x : int, y : int, text : str, centerDX = None, adjustY = None):
        """Add a text graphics item"""
        if 0 < len(text):
            item = cGraphicsTextItem(x, y, text, centerDX, adjustY)
            self.image.addItem(item)
            self.aTexts.append(item)
            return item
        return None

    def getBrushColor(self, dev, dfltName):
        if dev is not None and dev.dapName is not None and dev.index is not None:
            key = f"{dev.dapName}_{dev.index}"

            if key not in self.dColorByAP:
                self.dColorByAP[key] = self.colorIdx
                self.colorIdx = self.colorIdx + 1 if self.colorIdx + 1 < len(self.schema.brush["AP"]) else 1

            return "AP", self.dColorByAP[key]
        else:
            return dfltName, None

    def getToolTipText(self, name, dev, markRed):
        ret = None
        if dev is None:
            if markRed:
                if name[:8] == "artCSTF_":
                    ret = "Hidden CS Trace Funnel"
                elif name[:8] == "artREPL_":
                    ret = "Hidden CS Trace Replicator"
                else:
                    ret = " Device used but excluded from DTSL"
        else:
            ret = dev.getToolTip();
        return ret;

    @Slot(int)
    def performMultiSelectHighlight(self, ignore):
        if self.updateHighlighList is not None:
            pairs = self.updateHighlighList
            self.updateHighlighList = None
            self.performUpdateHighlighLines(pairs)

    def multiSelectHighlight(self, selectionList):
        if 1 < len(selectionList):
            aBoxNames = []
            for item in selectionList:
                for boxClass in self.dBoxes:
                    for boxInfo in self.dBoxes[boxClass]:
                        if boxInfo[1] == item:
                            aBoxNames.append(boxInfo[0].upper())
            aPairs = []
            for a in aBoxNames:
                for b in aBoxNames:
                    if a != b:
                        aPairs.append( [a, b] )
            self.updateHighlighList = aPairs
        else:
            self.updateHighlighList = None

    def addBox(self, sx : int, sy : int, dx : int, dy : int, name : str,
                     dev = None, canBeMarkedRed = False, border = None, toolTipPrefix = "" ,
                     toolTip = None, highlightVisible = False):
        """ sx, sy, dx, sy: create a box starting at (sx, sy) with the size (dx, dy)
        name: this is the instribe names of the box
        dev: options device description of the box
        canBeMarkedRed: if true, this box can be marked red in case of excluded device or none device
        border: default border color
        toolTipPrefix / toolTip: create a tool tip and add it to the box as needed
        """
        border = border if border else "foreground"
        distOff = 10
        self.minX = min(self.minX, sx - distOff)
        self.minY = min(self.minY, sy - distOff)
        self.maxX = max(self.maxX, sx+dx + distOff)
        self.maxY = max(self.maxY, sy+dy + distOff)

        noIdxName = "_".join(name.split("_")[:-1])
        noIdxName = noIdxName if 0 < len(noIdxName) else "Unique"

        if noIdxName not in self.dBoxes:
            self.dBoxes[noIdxName] = []
        if noIdxName not in self.dBoxesHighlight:
            self.dBoxesHighlight[noIdxName] = []

        toolTip = self.getToolTipText(name, dev, canBeMarkedRed) if toolTip is None else toolTip
        devType = "unknown"
        if toolTip is not None:
            for val in toolTip.split("\n"):
                aV = val.split(":")
                if 1 < len(aV) and aV[0].strip() == "Device Type":
                    devType = aV[-1].strip()
        if devType == "CSTFunnel" or name[:7] == "artCSTF":
            boxType = "funnel" if self.topDownFlag else "replicator"
            brushName, brushIdx = self.getBrushColor(dev, "artcstf")
        elif devType == "CSATBReplicator" or name[:7] == "artREPL":
            boxType = "replicator" if self.topDownFlag else "funnel"
            brushName, brushIdx = self.getBrushColor(dev, "artrepl")
        else:
            boxType = "default"
            if canBeMarkedRed and (dev is None or dev.isExcluded):
                brushName, brushIdx = self.getBrushColor(None, "red")
            else:
                brushName, brushIdx = self.getBrushColor(dev, "background")

        item = cGraphicsPolygonItem(boxType, sx-3, sy-3, dx+6, dy+6, "highlight", "background", None, None)

        if (highlightVisible):
            item.setVisible(True)

        self.dBoxesHighlight[noIdxName].append((name, item))
        self.image.addItem(item)

        item = cGraphicsPolygonItem(boxType, sx, sy, dx, dy, border, brushName, brushIdx, toolTipPrefix + toolTip if toolTip else None )
        self.image.addItem(item)
        self.dBoxes[noIdxName].append((name, item))

        if toolTip is not None and dev is not None:
            apIdx = dev.index
            if dev.address is not None:
                addr = dev.address
                high = addr[:-4]
                low  = addr[-4:]

                highIdx = None
                self.context.addItem(addr, item, sx, sy, dx, dy, name, apIdx)

        if boxType == "funnel":
            dy -= dy // 5
        elif boxType == "replicator":
            sy += dy // 5
            dy -= dy // 5

        self.addText(sx + dx // 2, sy + dy // 2, name, dx, dy)

    def hierarchyBlock(self, column, row, h, w, name, addr="", arrows=False, device = None):
        """Dr the AP blocks"""
        self.addBox(column, row, w, h, name, device)

        if arrows and self.startHierBox is not None:
            if self.startHierBox[-1]["column"] == column: # continue one the same hierarchie
                self.startHierBox[-1]["y"] = row + h
            elif column < self.startHierBox[-1]["column"]: # pop some column
                while column < self.startHierBox[-1]["column"]: # remove last entry
                    del self.startHierBox[-1]
                self.startHierBox[-1]["y"] = row + h
            else: # add a new level
                self.startHierBox.append({"x" : column + w // 2, "y" : row + h, "column" : column})

            startX = self.startHierBox[-2]["x"]
            startY = self.startHierBox[-2]["y"]
            endX   = column
            endY   = row + h // 2
            self.startHierBox[-2]["y"] = endY

            self.image.addItem(cGraphicsPathItem(startX, startY, startX, endY, False, "foreground", None))
            self.image.addItem(cGraphicsPathItem(startX, endY  , endX  , endY, False, "foreground", None))
        else:
            self.startHierBox = [{"x" : column + w // 2, "y" : row + h, "column" : column}]

    def changeColorSchema(self, schema):
        self.schema = schema
        self.image.setBackgroundBrush(self.schema.getBrush("background"))
        self.view.updateSchema(schema)

class cMenuEntry:
    def __init__(self, name, action, isChecked = None, toolTip = None, comboList = None):
        self.name      = name
        self.action    = action
        self.isChecked = isChecked
        self.toolTip   = toolTip
        self.comboList = comboList
        self.comboBox  = None

    def isSubMenu(self):
        return isinstance(self.action, list)
    
    def isComboList(self):
        return self.comboList is not None
    
    def isSeperator(self):
        return self.name == "-"
    
    def createComboList(self, window):
        self.comboBox = QComboBox(window)
        for s in self.comboList:
            self.comboBox.addItem(s)
        self.comboBox.setCurrentIndex(0)

        doit = QWidgetAction(window)
        doit.setDefaultWidget(self.comboBox)

        return doit

    def createMenuEntry(self, window):
        doit = QAction(self.name, window)
        if not self.isChecked is None:
            doit.setCheckable(True)
            doit.setChecked(self.isChecked)
        doit.triggered.connect(self.action)
        if not self.toolTip is None:
            doit.setToolTip(self.toolTip)
        return doit

class cMainTabWidget(QTabWidget):
    def __init__(self, closeSDF):
        super().__init__()
        self.setTabsClosable(False) # later True to enable close of SDF files
        self.tabCloseRequested.connect(closeSDF)


class cMainWindow(QMainWindow):
    """this is the to lebel main window"""
    def __init__(self, sdfApp, title : str, isDarkMode : bool):
        super().__init__()

        self.isDarkMode = isDarkMode
        self.sdfFileTab = cMainTabWidget(self.closeSDF)

        self.setWindowTitle(title)
        self.setCentralWidget(self.sdfFileTab)

        self.sdfApp    = sdfApp
        self.statusProgress = QProgressBar()
        self.statusProgress.setFixedWidth(100)
        self.statusProgress.hide()
        self.progressHidden = True
        self.statusBar().addPermanentWidget(self.statusProgress)

        self.lastCurrentVal = -1

        self.createMenu(QStyleFactory.keys())
        self.progressPrefix = ""

    def closeSDF(self, index):
        print(f"Close Index Request {index}")

    def setProgressPrefix(self, progressPrefix):
        self.progressPrefix = progressPrefix

    def enableProgressWidget(self, _max):
        if self.progressHidden:
            self.statusProgress.show()
            self.progressHidden = False
            self.statusProgress.setRange(0, _max)

    def resetProgressWidget(self):
        if not self.progressHidden:
            self.statusProgress.hide()
            self.progressHidden = True
        self.lastCurrentVal = -1 

    def updateStatusText(self, text, cur = None):
        if cur is not None:
            self.lastCurrentVal = cur;
            self.statusProgress.setValue(cur)
            
        self.statusBar().showMessage(text, 5000)
        self.sdfApp.processEvents()

    def progressLineCallBack(self, _cur, _max):
        if _max is None:
            self.resetProgressWidget()
        elif _cur is None:
            self.updateStatusText(f"{self.progressPrefix} {_max} of {_max} lines processed", _max)
        else:
            # print(f"{_cur} of {_max = }")
            _cur //= 2048
            if self.lastCurrentVal != _cur:
                self.enableProgressWidget((_max + 2048-1) // 2048)
                self.updateStatusText(f"{self.progressPrefix} {2048 * _cur} of {_max} lines processed", _cur)

    def progressCallBack(self, _cur, _max):
        if _cur is None or _max is None:
            self.resetProgressWidget()
        else:
            _max //= 1024
            _cur //= 1024

            if _max < 10000:
                isMB = False
            else:
                isMB = True
                _max = (2 * _max) // 1024
                _cur = (2 * _cur) // 1024

            if self.lastCurrentVal != _cur:
                self.enableProgressWidget(_max)
                if isMB:
                    self.updateStatusText(f"{self.progressPrefix} {_cur / 2} MB of {_max / 2} MB processed", _cur)
                else:
                    self.updateStatusText(f"{self.progressPrefix} {_cur} kB of {_max} kB processed", _cur)

    def getCurrentTab(self):
        sdfIdx = self.sdfFileTab.currentIndex()
        return self.sdfFileTab.widget(sdfIdx);

    def getLastTab(self):
        sdfIdx = self.sdfFileTab.count() - 1
        return self.sdfFileTab.widget(sdfIdx);

    def contextMenuEvent(self, event):
        currentSdfTab = self.getCurrentTab()

        if currentSdfTab is not None:
            currentSdfTab.contextLocateBox(event)

    def addSDF(self):
        fileNames = QFileDialog.getOpenFileNames(self, 'Open SDF file', '.',"SDF files (*.sdf)")
        if fileNames is not None:
            for fn in fileNames[0]:
                self.sdfApp.addSDFFileCB(fn)

    def generateFullName(self, dirName, name, ext, prefix = ""):
        if prefix[-4:].lower() == ".sdf":
            prefix = prefix[:-4]

        if 0 < len(prefix):
            prefix += "-"

        name = name.split(" (")[0]
        name = name.replace("/", "_")
        name = name.replace(" ", "_")

        return dirName + "/" + prefix + name + "." + ext

    def exportImage(self, fileName, content):
        self.updateStatusText("Create Image: " + fileName)
        dx = int(content.maxX - content.minX)
        dy = int(content.maxY - content.minY)
        svg = None
        image = None
        if fileName[-4:] == ".svg":
            svg = QSvgGenerator()
            svg.setFileName(fileName)
            svg.setSize(QSize(dx, dy))
            svg.setViewBox(QRect(0, 0, dx, dy))
            svg.setTitle(fileName)
            svg.setDescription("SVG created by Arm sdfvisio, version: " + __version__)
            painter = QPainter(svg)
        else:
            image = QPixmap(dx, dy)
            painter = QPainter(image)

        painter.setFont(content.font)
        painter.setBrush(content.brushBackground)
        painter.drawRect(0, 0, dx, dy)
        content.image.render(painter)
        painter.end()

        if image is not None:
            image.save(fileName)

    def exportSDFImages(self, aContent, dirName, ext, prefix=""):
        ret = 0
        for c in aContent:
            ret += 1
            self.exportImage(self.generateFullName(dirName, c.name, ext, prefix), c)
        return ret

    def exportAll(self, ext):
        dirName = QFileDialog.getExistingDirectory(self, 'Directory for all SDF images', '.')
        cnt = 0
        for idx in range(self.sdfFileTab.count()):
            t = self.sdfFileTab.widget(idx);
            cnt += self.exportSDFImages(t.aContent, dirName, ext, t.baseName)

        self.updateStatusText(f"{cnt}  images created in {dirName}")

    def exportAllpng(self):
        self.exportAll("png")

    def exportAllsvg(self):
        self.exportAll("svg")

    def exportCurrentSDF(self, ext):
        curTab = self.sdfTab.currentWidget()
        aContent = None
        titleName = ""
        for idx in range(self.sdfFileTab.count()):
            t = self.sdfFileTab.widget(idx);
            if t.sdfTabs == curTab.sdfTabs:
                curView = curTab.sdfTabs.currentWidget()
                aContent = t.aContent
                titleName = t.baseName

        cnt = 0
        if aContent is not None:
            title = 'Directory for ' + titleName + ' images'
            dirName = QFileDialog.getExistingDirectory(self, title, '.')
            if dirName is not None and 0 < len(dirName):
                cnt += self.exportSDFImages(aContent, dirName, ext)
        self.updateStatusText(f"{cnt} images created {dirName}")

    def exportCurrentSDFpng(self):
        self.exportCurrentSDF("png")

    def exportCurrentSDFsvg(self):
        self.exportCurrentSDF("svg")

    def exportCurrentImage(self):
        curTab = self.sdfFileTab.currentWidget()
        content = None

        for idx in range(self.sdfFileTab.count()):
            t = self.sdfFileTab.widget(idx);
            if t.sdfTabs == curTab.sdfTabs:
                curView = curTab.sdfTabs.currentWidget()
                for c in t.aContent:
                    if c.view == curView:
                        content = c

        if content is not None:
            fileNames = QFileDialog.getSaveFileName(self, 'Save current image tab', '.', "Image files (*.png *.svg)")
            if fileNames is not None and 0 < len(fileNames[0]):
                self.exportImage(fileNames[0], content)
        self.updateStatusText("Image created")

    def doExit(self):
        self.close()

    def doAbout(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(f"""sdfvision: read Arm DS SDF files and visualize the Coresight components and topology
(c) 2024 Arm Ltd, no implicit or explicit warranty of any fitness
License: Apache 2.0
PySide6: (L)GPL 3.0
Settings File: {settings.fileName()}
{__description__}""")
        msg.setWindowTitle("About sdfvisio " + __version__)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec()

    def doCopyright(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(f"""Copyright, Legal, and 3rd party licenses

{__copyright__}""")
        msg.setWindowTitle("About sdfvisio " + __version__)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec()
        
    def doRevision(self):
        """display the revision histor and limit it to the last 25 lines"""
        aRev = __revision__.split("\n")
        revision = "\n".join(aRev[:25]) + "\n..."
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(f"""sdfvision: read Arm DS SDF files and visualize the Coresight components and topology
(c) 2024 Arm Ltd, no implicit or explicit warranty of any fitness
License: Apache 2.0
Settings File: {settings.fileName()}
{revision}""")
        msg.setWindowTitle("Revision sdfvisio " + __version__)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec()

    def editColor(self, text):
        editColor = cEditColorSchema(self, text)
        if editColor.run():
            print("ColorTheme changed")
        else:
            print("ColorTheme Abort")

    def editColorOne(self):
        self.editColor("One")

    def editColorTwo(self):
        self.editColor("Two")

    def createMenu(self, styleList):
        self.menuBar = self.menuBar()
        dMenu = { "&File" : [cMenuEntry("Add SDF File", self.addSDF, toolTip="Add new SDF file"),
                               cMenuEntry("-", None),
                               cMenuEntry("Export All Images as PNG",          self.exportAllpng, toolTip = 
                                          "Export all open SDF file images as Portable Network Graphic(PNG) into the selected directory.\nThe filenames are <DIR>/<SDF Base Name>-<Tab Name>.png"),
                               cMenuEntry("Export All Images as SVG",          self.exportAllsvg, toolTip = 
                                          "Export all open SDF file images as Scalable Vector Graphic(SVG) into the selected directory.\nThe filenames are <DIR>/<SDF Base Name>-<Tab Name>.svg"),
                               cMenuEntry("Export current SDF as PNG",         self.exportCurrentSDFpng, toolTip = 
                                          "Export the current SDF file images as Portable Network Graphic(PNG) into the selected directory.\nThe filenames are <DIR>/<Tab Name>.png"),
                               cMenuEntry("Export current SDF as SVG",         self.exportCurrentSDFsvg, toolTip = 
                                          "Export the current SDF file images as Scalable Vector Graphic(SVG) into a selected directory.\nThe filenames are <DIR>/<Tab Name>.svg"),
                               cMenuEntry("Export Current Image (PNG or SVG)", self.exportCurrentImage, toolTip = 
                                          "Export the current image as Portable Network Graphic(PNG) or Scalable Vector Graphic(SVG) based on the file extension."),
                               cMenuEntry("-", None),
                               cMenuEntry("Exit", self.doExit, toolTip= "Exit Tool") ],

                  "&Edit User Color" : [ ],
                  "&Defaults" : [ ],
                  "&Help" : [cMenuEntry("About", self.doAbout, toolTip = "Version: " + __version__),
                             cMenuEntry("Copyright", self.doCopyright, toolTip = "Copyright and 3rd party licenses"),
                             cMenuEntry("Revison", self.doRevision)]}

        # update the color Modes
        for modeList in colorModeList:
            for item in modeList:
                if item.group.lower() == "user":
                    dMenu["&Edit User Color"].append( cMenuEntry(item.name, getattr(self, "editColor" + item.name) ))

        # update the default global settings
        colorSchema = ("Dark" if self.isDarkMode else "Light") + "/Default"
        for key in settings.getGroupKeys("Global"):
            if key == "color":
                colorSchema = settings.getString("Global/" + key)
            else:
                dMenu["&Defaults"].append( cMenuEntry(key, self.changeGlobal, isChecked=settings.getBool("Global/" + key)) )

        # Add the default color schema
        dMenu["&Defaults"].append( cMenuEntry(colorSchema, self.changeGlobal, comboList = getColorModeList()) )

        # add recent files
        aRecent = []
        for key in settings.getGroupKeys("Recent"):
            aRecent.append( cMenuEntry(key, self.addRecentSDF, toolTip = settings.getStr("Recent/" + key)) )

        if 0 < len(aRecent):
            dMenu["&File"].insert(1, cMenuEntry("&Recent SDF Files", aRecent))

        for m in dMenu:
            menu = QMenu(m, self)
            menu.setToolTipsVisible(True)
            self.menuBar.addMenu(menu)
            self.createRecursiveMenu(menu, dMenu[m])

    def createRecursiveMenu(self, menu, menuList):
        for act in menuList:
            if act.isSubMenu():
                subMenu = menu.addMenu(act.name)
                subMenu.setToolTipsVisible(True)
                self.createRecursiveMenu(subMenu, act.action)
            elif act.isSeperator():
                menu.addSeparator()
            elif act.isComboList():
                menu.addAction(act.createComboList(self))
            else:
                menu.addAction(act.createMenuEntry(self))

    def addRecentSDF(self):
        action = self.sender()
        if action is not None:
            fileName = settings.getStr("Recent/" + action.text())
            self.sdfApp.addSDFFileCB(fileName)

    def changeGlobal(self):
        action = self.sender()
        # print(f"{type(action)}")
        if action is not None:
            # change the global default settings
            settings.updateBool("Global/" + action.text(), action.isChecked(), None)

    def createSDFTab(self, fn):
        """ Create a new tab widget for a new SDF file"""
        subMain = cSDFTabUI(fn, self.isDarkMode or settings.getBool("Global/DarkMode"), settings)

        idx = self.sdfFileTab.addTab(subMain, subMain.baseName)
        self.sdfFileTab.setTabToolTip(idx, subMain.fileName)
        self.sdfFileTab.setCurrentIndex(idx)

        self.updateStatusText("Processing: " + fn)
        return subMain.sdfTabs

    def getCurrentSchema(self):
        return self.getLastTab().getCurrentSchema()

    def activateTab(self, idx):
        if 0 <= idx and idx < self.sdfFileTab.count():
            self.sdfFileTab.setCurrentIndex(idx)

    def getLastSDF(self):
        return self.getLastTab()

    def getLastTabs(self):
        return self.getLastTab().sdfTabs

    def addTreeView(self, title, column):
        treeWidget = cTreeWidget(column)
        self.getLastTabs().addTab(treeWidget, title)
        return treeWidget

    def addDapLogTableView(self, title):
        dapLogWidget = cDapLogWidget(clipboard)
        dapLogTap = self.getLastTabs().addTab(dapLogWidget, title)
        return dapLogWidget

    def setLastTabColor(self, color):
        tabBar = self.sdfFileTab.tabBar()
        tabBar.setTabTextColor(self.sdfFileTab.count() - 1, color)

    def updateTabTitle(self, newTitle):
        self.getLastTabs().setTabText(self.getLastTabs().count() - 1, newTitle)

class cTreeWidget(QTreeWidget):
    """Wrapper for a Tree Widget, create a Tree with n columns
       To fill the table use addTreeItem, the argument is a list in entries. each entire contains a number of elements.
       if the element is a string fill the first column entiry, if a list, it contains the the column 1 - n content
    """
    def __init__(self, column):
        super().__init__()
        self.setColumnCount(len(column))
        self.setHeaderLabels(column)

    def setTreeItemText(self, item, entry):
        if isinstance(entry, str):
            item.setText(0, entry)
        else:
            maxCnt = min(len(entry), self.columnCount())
            for idx in range(maxCnt):
                item.setText(idx, entry[idx])

    def isChildText(self, child, entry):
        if isinstance(entry, str):
            return child.text(0) == entry
        else:
            return child.text(0) == entry[0]

    def recursiveAddTree(self, parent, aList):
        if 0 < len(aList) and aList[0]:
            levelItem = None
            # search for the location to add the new tree item
            for childIdx in range(parent.childCount()):
                child = parent.child(childIdx)
                if self.isChildText(child, aList[0]):
                    levelItem = child

            if levelItem is None:
                # if not found add the new item
                levelItem = QTreeWidgetItem()
                self.setTreeItemText(levelItem, aList[0])
                parent.addChild(levelItem)

            self.recursiveAddTree(levelItem, aList[1:])

    def addTreeItem(self, aList):
        topLevelItem = None
        for idx in range(self.topLevelItemCount()):
            tli = self.topLevelItem(idx)
            if self.isChildText(tli, aList[0]):
                topLevelItem = tli
        if topLevelItem is None:
            topLevelItem = QTreeWidgetItem()
            self.setTreeItemText(topLevelItem, aList[0])
            self.addTopLevelItem(topLevelItem)

        self.recursiveAddTree(topLevelItem, aList[1:])

    def changeColorSchema(self, schema):
        print("change color scheme needs implementating")

class cDisplaySDF(QApplication):
    """QApplication wrapper"""
    def __init__(self, version, addSDFFileCB, opt):
        global clipboard
        super().__init__(sys.argv)
        self.addSDFFileCB  = addSDFFileCB

        # change the default font and set the style to Fusion to enable Dark mode
        self.setStyle(QStyleFactory.create("Fusion"))
        
        cGraphicsTextItem.defaultFont = QFont("Bahnschrift", 11)

        self.opt        = opt 
        self.isDarkMode = self.styleHints().colorScheme() == Qt.ColorScheme.Dark
        self.mainWindow = cMainWindow(self, "SDF Visualizer (Version: " + version + ")", self.isDarkMode)
        clipboard = self.clipboard();


    def loop(self):
        self.mainWindow.sdfFileTab.setCurrentIndex(0)
        self.mainWindow.resize(800,600)
        self.mainWindow.showMaximized()
        ret = self.exec()
        aSDFRecentList = [self.mainWindow.sdfFileTab.widget(idx - 1) for idx in range(self.mainWindow.sdfFileTab.count(), 0, -1)]
        aSDFRecentList = aSDFRecentList[:10]
        
        # add up to 10 files to the recent SDF file list
        aOldRecent = []
        for key in settings.getGroupKeys("Recent"):
            newKey = ":".join(key.split(":")[1:])
            aOldRecent.append( (newKey, settings.getStr("Recent/" + key)) )
            
        # build the new recent file list
        settings.clearGroupKeys("Recent")
        aAdded = []
        recentIdx = 1
        for sdf in aSDFRecentList:
            if recentIdx <= 10:
                absFileName = str(Path(sdf.fileName).absolute())
                if sdf.baseName not in aAdded:
                    aAdded.append(sdf.baseName)
                    settings.updateStr(f"Recent/{recentIdx:2d}:{sdf.baseName}", absFileName, None)
                    recentIdx += 1

        for sdf in aOldRecent:
            if recentIdx <= 10:
                if sdf[0] not in aAdded:
                    aAdded.append(sdf[0])
                    settings.updateStr(f"Recent/{recentIdx:2d}:{sdf[0]}", sdf[1], None)
                    recentIdx += 1

        settings.update()
        sys.exit(ret)

def DisplayMsg(name, msg):
    msg = QMessageBox()
    msg.setWinodwTitle(name)
    msg.setText(msg)
    msg.exec()

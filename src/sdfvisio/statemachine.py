###
# 
# SDF Visualizer, 
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Contains:
# class cState: Declare a state for the state machine use to analyze a sequence of DAPLOW eater summary ouptut
# class cStateMachine: handling the state machine
#
###

class cState:
    """Declare a new state"""
    def __init__(self, state : str, component, regname : str, access : str, valueList, callback):
        """Initialize a state, the state can be either numerical or name based but need to use the same type for the state machine
        state: "currentState->nextState" if the current state of the stateMachine does match this state we check this state
                "SPECIAL:IGNORE" this state is applicaple to all states and is just ignored
                "SPECIAL:RESET"  this state is applicaple to all states and return to the start 
        component:    (optional)required component
        regname:      (required) register name
        access:       (required) Access type (like "R" or "W")
        valueList:    (optional) pairs of mask and result [(m0, r0), (m1, r1), ...] (value * Mn) == Rn
        callback:     (optional) callback function if the check matches with all arguments
        """
        self.component = component
        self.regname   = regname
        self.access    = access.upper()
        self.valueList = valueList
        self.callback  = callback
        aState = state.split("->")
        self.currentState = aState[0].strip()
        self.nextState    = aState[1].strip() if 1 < len(aState) else aState[0].strip()
        self.currentIndex = None
        self.nextIndex    = None
        self.isIgnore     = self.currentState == "SPECIAL:IGNORE"
        self.isReset      = self.currentState == "SPECIAL:RESET"
        self.isSpecial    = self.isIgnore or self.isReset
        
    def __repr__(self):
        ret = f"{self.currentState} -> {self.nextState} if "
        if not self.component is None:
            ret += f"Component: {self.component}, "
        ret += f"RegName: {self.regname}, Access: {self.access}"
        if self.valueList:
            ret += ", ValueList: ["
            for pair in self.valueList:
                ret += f"(0x{pair[0]:08X}, 0x{pair[1]:08X})"
            ret += "]"
        return ret
    
    def check(self, currentIndex: str | int, component, regname : str, access : str, value) -> bool:
        """if the state matches, call the state callback function
           otherwise return False
        Return false if one of the folliwing condition is True:
         - incorrect currentIndex
         - incorrect component (optional)
         - incorrect register name
         - incorrect access
         - unmatched value (optional)
        """
        # print(f"Check: {self.currentState} -> {self.nextState} if {self.component} {self.regname} {self.access} {self.valueList}")
        if self.currentIndex != currentIndex and not self.isSpecial:
            return False
        if self.component is not None and self.component != component:
            return False
        if self.regname != regname:
            return False
        if self.access != access.upper():
            return False
        if not self.valueList is None:
            flag = False
            val = int(value[:10], 16)
            for pair in self.valueList:
                flag |= (val & pair[0]) == pair[1]
            if not flag:
                return False
            
        if self.callback:
            self.callback(component, regname, access, value)

        return True

class cStateMachine:
    """The Dap Log Eater output state machine provide additional functionallity to combine sequences of componens accesses into a combined readable output
    The state machine handles a list of states which are currently "hand written".
    Each state has current number and a following number. if the current state maches, the next number is used
    and True is returned. if no state match, False is returned
    Each state may provide additinal callback function which is call if the current start maches.
    self.currentList: list of all posible states
    self.currentState: current state index
    self.startState:   inital start state, use to reset the state

    For all states in the stateList, we check if the state check function returns true, we use this as the maching state
    and progress to the next state ID and return True. if no state maches we return False to indicate that we don't match
    """
    def __init__(self, stateList: list, startState : str | int):
        # separate list into two lists
        self.stateList   = [state for state in stateList if not state.isSpecial]
        self.specialList = [state for state in stateList if state.isSpecial]

        self.nameIndex = []
        self.startState = self.name2index(f"{startState}") if type(startState) == int else self.name2index(startState)

        self.currentState = self.startState

        for state in self.stateList:
            self.assignStateIndex(state)

    def printAll(self):
        for state in self.stateList:
            if (state.currentIndex == self.startState):
                print(f"*{state}")
            else:
                print(f" {state}")

    def name2index(self, name : str) -> int:
        if name in self.nameIndex:
            ret = self.nameIndex.index(name)
        else:
            ret = len(self.nameIndex)
            self.nameIndex.append(name)
        return ret

    def assignStateIndex(self, state):
        state.currentIndex = self.name2index(state.currentState)
        state.nextIndex    = self.name2index(state.nextState)

    def addState(self, newState : cState) -> None:
        self.stateList.append(newState)
        self.assignStateIndex(newState)

    def reset(self):
        self.currentState = self.startState

    def inResetState(self):
        return self.currentState == self.startState
    
    def selectState(self, component, regname : str, access : str, value):
        if any(state.check(self.currentState, component, regname, access, value) and
               (setattr(self, 'currentState', state.nextIndex) or True) for state in self.stateList):
                return True
            
        if not self.inResetState():
            """check all of the special state to handle ignore and reset"""
            matched_state = next((state for state in self.specialList if state.check(self.currentState, component, regname, access, value)), None)
            if matched_state:
                    if matched_state.isIgnore: # ignore this state and check the next line
                        return True
                    if matched_state.isReset: # force state back to start state
                        self.currentState = self.startState;
                        return False
                    
        return False

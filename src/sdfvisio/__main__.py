###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
main module to process arguments and start the UI
It launches and opend any given SDF files. 
It sets up the options and updates the global settings.
The default settings are stored in a user specific initialization file
"""

import argparse

from .sdfvisio import cSDFMain
from .__init__ import __version__
from .qtsettings import settings

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog = "sdfvision", description = f"Visualize Arm Development Studio SDF files; version {__version__}")
    parser.add_argument('--notopology', type=str, action='append', help = "Components without topology requirement")
    parser.add_argument('--highlight',  type=str, action='append', help = "if name matches, Comp:highlight the box, CompA:CompB: highlight patrh from compA to CompB")
    parser.add_argument('--excluded',   default = None, action='store_true',  help = "Add tab if excluded components exist")
    parser.add_argument('--sinktop',    default = None, action='store_false', help = "start with trace sinks on top")
    parser.add_argument('--addtf',      default = None, action='store_true',  help = "add 'hidden' trace funnels")
    parser.add_argument('--addtr',      default = None, action='store_true',  help = "add 'hidden' trace replicator")
    parser.add_argument('--widerect',   default = None, action='store_true',  help = "draw wide rectange")
    parser.add_argument('--oldlayout',  default = None, action='store_true',  help = "use the layout with centered boxes")
    parser.add_argument('--curve',      default = None, action='store_true',  help = "use curved lines")
    parser.add_argument('--shadow',     default = None, action='store_true',  help = "enable shadow for boxes")
    parser.add_argument('sdf', type=str, nargs='*', help = "SDF files")

    # parse the arguments and change to a dictionaly for the Main class
    args = parser.parse_args()

    # update the global setting is option is use, otherwise ise old value or default
    settings.updateBool("Global/Excluded", args.excluded, False)
    settings.updateBool("Global/SinkBottom", args.sinktop, True)
    settings.updateBool("Global/NarrowRect", args.widerect, True)
    settings.updateBool("Global/AddTraceFunnel", args.addtf, False)
    settings.updateBool("Global/AddTraceReplicator", args.addtr, False)
    settings.updateBool("Global/Curve", args.curve, True)
    settings.updateBool("Global/Shadow", args.curve, False)
    settings.updateBool("Global/OldLayout", args.oldlayout, False)

    opt = { "notopo":          args.notopology,
            "highlight":       { "box" : [], "path": []}}

    if args.highlight is not None:
        for s in args.highlight:
            aS = s.split(":")
            if len(aS) == 1:
                opt["highlight"]["box"].append(aS[0].upper())
            elif len(aS) == 2:
                opt["highlight"]["path"].append((aS[0].upper(), aS[1].upper()))

    # open the SDF main view
    displaySDF = cSDFMain(opt, __version__)

    # add all SDF files to the display
    for fn in args.sdf:
        displaySDF.addSDFFile(fn)

    # start the UI loop
    displaySDF.loop()

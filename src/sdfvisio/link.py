###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Link Class
# 
###

"""
Class definition:
cLink: helper function to start the initial layout based. All references are base on the component name
cArtEntry: create an artificial entry for CS Funnels or Replicators
"""
class cLink:
    def __init__(self, attrib):
        self.dAttr = {k: v for k, v in attrib.items() } if attrib else {}
        self.level = None # level not yet set

    def setMS(self, m, s):
        self.dAttr["master"] = m
        self.dAttr["slave"]  = s

    def __repr__(self):
        return self.__str__()

    def getStrPart(self, prefix, name):
        nameVal = self.dAttr.get(name, None)
        return '' if nameVal is None else f' {prefix}[{nameVal}]'

    def __str__(self):
        misi = self.getStrPart("MI", "master_interface") + self.getStrPart("SI", "slave_interface") + self.getStrPart("TR", "trigger")
        return f'{self.dAttr["master"]}:{self.level}  -> {self.dAttr["slave"]}{misi}'

class cArtEntry(cLink):
    """Create an artificial funnel or replicator and simulate a XML ElementTree.Element of the SDF XML file"""
    def __init__(self, m, mi, s, si):
        super().__init__({ "master" : m, "master_interface": str(mi), "slave": s, "slave_interface" : str(si) } )


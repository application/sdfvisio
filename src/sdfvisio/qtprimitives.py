###
# 
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###

"""
Type definitions:
cGraphicsPathItem:create the lines between two boxes based on the path settings and add a tool tip as needed
    base class: QGraphicsPathItem

cGraphicsPolygonItem: Create the box type for funnels, replicators, and normal itesm and set the fill color according to the setting
    base class: QGraphicsPolygonItem

cGraphicsTextItem: create a text item to the graphics scene; based on the widthDX/DY settgins, the start coordinate will be changed
    base class: QGraphicsTextItem
"""
from PySide6.QtCore    import QPointF
from PySide6.QtGui     import QPolygonF
from PySide6.QtGui     import QPainterPath
from PySide6.QtGui     import QFontMetrics
from PySide6.QtWidgets import QGraphicsPathItem
from PySide6.QtWidgets import QGraphicsPolygonItem
from PySide6.QtWidgets import QGraphicsTextItem
from PySide6.QtWidgets import QGraphicsItem
from PySide6.QtWidgets import QLineEdit, QLabel
from PySide6.QtWidgets import QDialog, QDialogButtonBox
from PySide6.QtWidgets import QHBoxLayout, QVBoxLayout

class cSearchDialog(QDialog):
    def __init__(self, parent, initLine):
        super().__init__(parent)
        self.setWindowTitle("Search String")
        self.ret      = False
        self.lineEdit = QLineEdit(initLine)

        self.hbox = QHBoxLayout()
        self.hbox.addWidget(QLabel("String:"))
        self.hbox.addWidget(self.lineEdit, 3)
        self.vbox = QVBoxLayout()
        self.vbox.addLayout(self.hbox)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.buttonOk)
        buttonBox.rejected.connect(self.buttonCancel)
        self.vbox.addWidget(buttonBox)
        self.setLayout(self.vbox)

    def buttonCancel(self):
        self.ret = False
        self.close()

    def buttonOk(self):
        self.ret = True
        self.line = self.lineEdit.text()
        self.close()

    def run(self):
        self.show()
        self.exec()
        return self.line if self.ret else None

class cGraphicsPathItem(QGraphicsPathItem):
    """Create
      - if sx == sy or sy == ey: a simple line
      - otherwise: start and end stub and a connect line or path between these lines
    """
    def genPath(self, useCurve):
        sx = self.sx
        sy = self.sy
        ex = self.ex
        ey = self.ey

        path = QPainterPath(QPointF(sx, sy))
        if (sx != ex and sy != ey):
            hy = (ey + sy) // 2
            dy = -4 if sy > ey else 4
            path.lineTo(sx, sy + dy)
            sy += dy
            hx = (ex + sx) // 2
            dx = 2 * ((ex - sx) // 50)
            if useCurve:
                path.cubicTo(sx + dx, hy     , ex - dx, hy     , ex, ey - dy)
            else:
                path.lineTo(ex, ey - dy)
        path.lineTo(ex, ey)
        self.setPath(path)

    def updatePathType(self, useCurve):
        self.genPath(useCurve)

    def __init__(self, sx : int, sy : int, ex : int, ey : int, useCurve : bool, penName : str, toolTip):
        super().__init__()
        self.sx = sx
        self.sy = sy
        self.ex = ex
        self.ey = ey

        self.penName = penName
        self.genPath(useCurve)
        self.setVisible(penName != "highlight")

        if not toolTip is None:
            self.setToolTip(toolTip)

class cGraphicsPolygonItem(QGraphicsPolygonItem):
    def __init__(self, typ : str, sx : int, sy : int, dx : int, dy : int, penName : str, brushName : str, brushIdx : int, tooltip : str):
        self.penName   = penName
        self.brushName = brushName
        self.brushIdx  = brushIdx 
        poly = QPolygonF()
        if typ == "funnel":
            dy6 = dy // 6
            poly.append(QPointF(sx,               sy + 0*dy6))
            poly.append(QPointF(sx + dx,          sy + 0*dy6))
            poly.append(QPointF(sx + dx,          sy + 4*dy6))
            poly.append(QPointF(sx + (2*dx) // 3, sy + 5*dy6))
            poly.append(QPointF(sx + (2*dx) // 3, sy + 6*dy6))
            poly.append(QPointF(sx + (1*dx) // 3, sy + 6*dy6))
            poly.append(QPointF(sx + (1*dx) // 3, sy + 5*dy6))
            poly.append(QPointF(sx,               sy + 4*dy6))
            poly.append(QPointF(sx,               sy + 0*dy6))
        elif typ == "replicator":
            dy6 = dy // 6
            poly.append(QPointF(sx,               sy + 6*dy6))
            poly.append(QPointF(sx + dx,          sy + 6*dy6))
            poly.append(QPointF(sx + dx,          sy + 2*dy6))
            poly.append(QPointF(sx + (2*dx) // 3, sy + 1*dy6))
            poly.append(QPointF(sx + (2*dx) // 3, sy + 0*dy6))
            poly.append(QPointF(sx + (1*dx) // 3, sy + 0*dy6))
            poly.append(QPointF(sx + (1*dx) // 3, sy + 1*dy6))
            poly.append(QPointF(sx,               sy + 2*dy6))
            poly.append(QPointF(sx,               sy + 6*dy6))
        else:
            poly.append(QPointF(sx     , sy))
            poly.append(QPointF(sx     , sy + dy))
            poly.append(QPointF(sx + dx, sy + dy))
            poly.append(QPointF(sx + dx, sy))
        super().__init__(poly)
        self.setFlags(QGraphicsItem.GraphicsItemFlags.ItemIsSelectable)
        self.setToolTip(tooltip)
        self.setVisible(penName != "highlight")

class cGraphicsTextItem(QGraphicsTextItem):
    """create a graphics text item and center it, if necessary"""
    defaultFont = None
    def __init__(self, mx  : int, my : int, text : str, widthDX = None, widthDY = None):
        """mx, my: top left corner or mid coordinate coordinate
           text: add this text
           widthDX: if == None: use mx as is
                    elif -1 correct mx by text width // 2
                    elif >0 and <textwidth() center text
                    else create mult line text
           widthDY: if == None  use my as is
                    elif == -1: adjust up
                    elif ==  1: adjust down
                    else use as DY for the mid point
        """
        super().__init__(text)
        
        self.setFont(cGraphicsTextItem.defaultFont)

        fm = QFontMetrics(cGraphicsTextItem.defaultFont)
        rect = fm.tightBoundingRect(text)

        dyMul = 1
        if widthDX != None:
            if widthDX == -1:
                mx -= rect.width() // 2
                mx -= 5
            elif rect.width() + 8 < widthDX:
                mx -= rect.width() // 2
                mx -= 4
            else:
                self.setTextWidth(widthDX)
                mx   -= widthDX // 2
                dyMul = 2

        if widthDY != None:
            height = rect.height()
            height += (height & 1)
            if widthDY == -1:
                my -= (3 * height) // 2
                my -= 1
            elif widthDY == +1:
                my -= height // 2
                my += 3
            else:
                my -= 2
                my -= (dyMul * height) // 2

        self.setPos(mx, my)

###
#
# SDF Visualizer, DeviceHierarchy library
#
# SPDX-FileCopyrightText: Copyright 2022-2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Contains:
#   Viable AP names as a dictionary
#   Device class with attributes
#   Procedure to create device list from an SDF hierarchy
#
###

"""
Class Definitions:
  cDevice: SDF specific device information

Functions:
makeDebugTraceDeviceList: Read the SDF XML file and returns a list to all cDevice
makeJtagDeviceList: build a list of all cJTAGTAB
"""

from xml.etree.ElementTree import Element

from .qtsettings    import settings
from .jtagtab       import cJTAGTAB
from .trackregister import cTrackDeviceRegister
from .qtdaplog      import cDapLogTableWidget

# AP names found in SDF files, list is expandable

viableaps = {
    "CSMEMAP",
    "JTAG-AP",
}

def getInt(v : str) -> int:
    """convert a hex or decimal string to integer"""
    if 2 < len(v) and v[0] == '0' and (v[1] == 'x' or v[1] == 'X'):
        return int(v, 16)
    return int(v)

# Device class
class cDevice:
    """
    handle SDF related device information
    """
    def __init__(self, dapName : str, name : str, devType : str, dev):
        """Setup variables on initialize, initialized not set values with None"""
        self.dapName    = dapName
        self.name       = name
        self.devType    = devType
        self.parent     = None
        self.index      = None
        self.depth      = None
        self.address    = None
        self.isExcluded = False
        self.info       = {}
        self.deviceRegister = cTrackDeviceRegister(self)
        self.toolTip    = None

        for item in dev.findall(".//config_item"):
            name = item.attrib.get("name", "")
            if name == "CORESIGHT_BASE_ADDRESS":  # Should be in each device
                self.address = str(item.text)
            # Find AP_INDEX for CSMEMAP categorisation
            elif name == "CORESIGHT_AP_INDEX":  # Should be in each device and AP
                self.index = getInt(item.text)
            # Check for "PARENT_AP_INDEX" for nested CSMEMAPs
            elif name == "PARENT_AP_INDEX":  # Only for nested APs
                self.parent = getInt(item.text)
            else:
                self.info[name] = str(item.text)

        for item in dev.findall(".//device_info_item"):
            name = item.attrib.get("name", None)
            if name is not None:
                if name == "EXCLUDE_FROM_DTSL":  # Only for nested APs
                    self.isExcluded = str(item.text).lower() != "false"
                else:
                    self.info[name] = str(item.text)

        if self.index is None:
            self.index = 0     # use the default AP index
            self.info["++IMPLICIT_AP_INDEX++"] = "True"

    def updateRegister(self, reg : str, value : str, row : int, dapLogTab : cDapLogTableWidget) -> None:
        """Track device specific register values"""
        val = int(value.split()[0], 16)
        aReg = reg.split(":")
        regName = aReg[-1].split()[0]

        self.deviceRegister.updateRegister(row, regName, aReg, val, dapLogTab)

    def performComponentTest(self, name : str) -> None:
        """After processing the dap log file, anayze the value correctness"""
        self.deviceRegister.performComponentTest(name)

    def __str__(self) -> str:
        """Return a string representation"""
        return f"{self.dapName}:{self.devType}:{self.name} "

    def setDepth(self, depth : int) -> None:
        """The the level in the topology structure"""
        self.depth = depth

    def addToolTipPart(self, aRet, prefix, value) -> None:
        """Helper function to add a tool tip is value is set"""
        if value is not None:
            aRet.append(f"{prefix}: {value}")

    def getToolTip(self) -> str:
        """Create a device specific tool tip used for the boxes and dap log eater table"""
        if self.toolTip is None:
            aRet = []
            self.addToolTipPart(aRet, "DAP", self.dapName)
            self.addToolTipPart(aRet, "   AP_Index", self.index)
            self.addToolTipPart(aRet, "      Address    ", self.address)
            self.addToolTipPart(aRet, "      Device type", self.devType)
            prefix = "Info:\n  "
            for name, item in self.info.items():
                self.addToolTipPart(aRet, prefix + name, item)
                prefix = "  "
            self.toolTip = "\n".join(aRet)
        return self.toolTip

# Device Hierarchy based on Arm DS and Developer examples
# Section 14.1.4 of User Guide
# https://developer.arm.com/documentation/101470/2010/Platform-Configuration/Platform-Configuration-and-the-Platform-Configuration-Editor--PCE-/Device-hierarchy-in-the-PCE-view

def makeJtagDeviceList(root : Element) -> list:
    """Build the JTAG/SWD device list and mask if SWD is enabled"""
    aJtagList = []

    isSWD   = False
    # check if forces SWD mode
    for debugTraceConfig in root.findall(".//debug_and_trace_config"):
        for ci in debugTraceConfig.findall(".//config_item"):
            if ci.attrib.get("name", "") == "ProbeMode":
                isSWD = ci.text == "2"

    for jtag in root.findall(".//scanchain"):
        for dev in jtag:
            aJtagList.append(cJTAGTAB(dev, isSWD))
    return aJtagList

def _updateDevDepth(aDeviceList : list, currentDev : cDevice) -> None:
    """Calculate depth based on index, accounting for nested aps
    LATER: move this into cDevice
    """
    if currentDev.devType in viableaps:  # Check if AP
        if currentDev.parent is None:  # Checks if this AP is nested
            currentDev.setDepth(1)
        else:
            for parentAP in aDeviceList:
                if (parentAP.devType in viableaps) and (parentAP.index == currentDev.parent):  # find Parent
                    currentDev.setDepth(parentAP.depth + 1)  # child depth = parent depth + 1
    else:
        for memAP in aDeviceList:  # non-APs are not nested
            if (memAP.devType in viableaps) and (memAP.index == currentDev.index):  # find parent
                currentDev.setDepth(memAP.depth + 1)  # child depth = parent depth + 1

def _appendDevice(aDeviceList : list, dev : cDevice, excludedFromDTSL : list) -> None:
    """Append this device if device index and depth is updated"""
    if (dev.index is None) or (dev.depth is None):
        print("ERROR: Device " + dev.name + " has no AP index or AP depth")
    else:
        aDeviceList.append(dev)  # Must have index and depth for this display
        if dev.isExcluded:
            if settings.getBool("excluded"):
                excludedFromDTSL.append(dev.name)
            else:
                print(f"Device {dev.name} is excluded from DTSL")

def makeDebugTraceDeviceList(root : Element) -> list:
    """Read all CSMEMAP and CS components"""
    aDeviceList = []

    excludedFromDTSL = []

    for dap in root.findall(".//dap"):  # Searches each DP, for multiple DP systems
        dapName = dap.attrib["name"]

        # Get device information under selected DAP
        for dev in dap.iter('device'):  # nested for loops to get details of each device
            currentDev = cDevice(dapName, dev.attrib["name"], dev.attrib["type"], dev)  # New device object
            _updateDevDepth(aDeviceList, currentDev)
            _appendDevice(aDeviceList, currentDev, excludedFromDTSL)
    return aDeviceList
